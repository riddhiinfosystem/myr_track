package Fragment;


import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.riddhi.myr_track.R;
import com.riddhi.myr_track.VehicleNearByActivity;
import com.riddhi.myr_track.VehicleNearByPlaceActivity;
import com.riddhi.myr_track.VehicleStatusActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class UtilitiesFragment extends Fragment {
    private ListView listViewReportList;
    private String[] listString = {"Vehicle Status", "Near By Vehicle", "Near By Place"};
    private int[] listImages = {R.drawable.vehicle_status, R.drawable.nearby_vehicle_icon, R.drawable.nearby_place};
    private TextView textView3;

    public UtilitiesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_utilities, container, false);
        listViewReportList = rootView.findViewById(R.id.lstReport);

        String customFont = "fonts/arlrdbd.ttf";
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), customFont);
        textView3 = rootView.findViewById(R.id.textView3);
        textView3.setTypeface(typeface);
        List<HashMap<String, String>> aList = new ArrayList<HashMap<String, String>>();

        for (int i = 0; i < listString.length; i++) {
            HashMap<String, String> hm = new HashMap<String, String>();
            hm.put("txt", listString[i]);
            hm.put("img", Integer.toString(listImages[i]));
            aList.add(hm);

            String[] from = {"txt", "img"};

            int[] to = {R.id.tvList, R.id.ivList};

            SimpleAdapter adapter = new SimpleAdapter(getActivity(), aList, R.layout.list, from, to);
            listViewReportList.setAdapter(adapter);

            listViewReportList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    switch (position) {
                        case 0:
                            startActivity(new Intent(getActivity(), VehicleStatusActivity.class));
                            getActivity().overridePendingTransition(R.anim.right_in, R.anim.left_out);
                            break;
                        case 1:
                            startActivity(new Intent(getActivity(), VehicleNearByActivity.class));
                            getActivity().overridePendingTransition(R.anim.right_in, R.anim.left_out);
                            break;
                        case 2:
                            startActivity(new Intent(getActivity(), VehicleNearByPlaceActivity.class));
                            getActivity().overridePendingTransition(R.anim.right_in, R.anim.left_out);
                            break;

                    }
                }
            });
        }

        return rootView;
    }

}

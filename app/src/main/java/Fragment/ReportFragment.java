package Fragment;


import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.riddhi.myr_track.DailyAnalysisRootBase;
import com.riddhi.myr_track.DailyAnlysisTimeBaseActivity;
import com.riddhi.myr_track.DailyVehAnaPara;
import com.riddhi.myr_track.FuelReport;
import com.riddhi.myr_track.MarkLocationNew;
import com.riddhi.myr_track.R;
import com.riddhi.myr_track.StatusReport;
import com.riddhi.myr_track.TempratureReport;
import com.riddhi.myr_track.TrackAllActivity;
import com.riddhi.myr_track.TrackandTraceReport;
import com.riddhi.myr_track.VehicleDetails;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ReportFragment extends Fragment {
    private ListView listViewReportList;
    private String[] listString = {"Daily Analysis", "Daily Analysis(Time Base)", "Daily Analysis(Root Report)", "Mark Location", "Fuel", "Temperature", "Track and Trace", "Track All", "Status Graph", "Vehicle Details"};
    private int[] listImages = {R.drawable.reporttrans, R.drawable.reporttrans, R.drawable.reporttrans, R.drawable.maklocation, R.drawable.fuelpump, R.drawable.fuelreport, R.drawable.trackandtrace, R.drawable.trackall_icon, R.drawable.status_icon_for_rptlist, R.drawable.vehicledetails};
    private TextView textView3;

    public ReportFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_reports, container, false);

        listViewReportList = rootView.findViewById(R.id.lstReport);

        String customFont = "fonts/arlrdbd.ttf";
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), customFont);
        textView3 = rootView.findViewById(R.id.textView3);
        textView3.setTypeface(typeface);

        List<HashMap<String, String>> aList = new ArrayList<HashMap<String, String>>();

        for (int i = 0; i < listString.length; i++) {
            HashMap<String, String> hm = new HashMap<String, String>();
            hm.put("txt", listString[i]);
            hm.put("img", Integer.toString(listImages[i]));
            aList.add(hm);

            String[] from = {"txt", "img"};

            int[] to = {R.id.tvList, R.id.ivList};

            SimpleAdapter adapter = new SimpleAdapter(getActivity(), aList, R.layout.list, from, to);
            listViewReportList.setAdapter(adapter);

            listViewReportList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    switch (position) {
                        case 0:
                            Intent i = new Intent(getActivity(), DailyVehAnaPara.class);
                            startActivity(i);
                            getActivity().overridePendingTransition(R.anim.right_in, R.anim.left_out);
                            break;
                        case 1:
                            Intent timebase = new Intent(getActivity(), DailyAnlysisTimeBaseActivity.class);
                            startActivity(timebase);
                            getActivity().overridePendingTransition(R.anim.right_in, R.anim.left_out);
                            break;
                        case 2:
                            Intent rootbase = new Intent(getActivity(), DailyAnalysisRootBase.class);
                            startActivity(rootbase);
                            getActivity().overridePendingTransition(R.anim.right_in, R.anim.left_out);
                            break;
                        case 3:
                            Intent iGpsLoc = new Intent(getActivity(), MarkLocationNew.class);
                            startActivity(iGpsLoc);
                            getActivity().overridePendingTransition(R.anim.right_in, R.anim.left_out);
                            break;
                        case 4:
                            Intent imap = new Intent(getActivity(), FuelReport.class);
                            startActivity(imap);
                            getActivity().overridePendingTransition(R.anim.right_in, R.anim.left_out);
                            break;
                        case 5:
                            Intent itemp = new Intent(getActivity(), TempratureReport.class);
                            startActivity(itemp);
                            getActivity().overridePendingTransition(R.anim.right_in, R.anim.left_out);
                            break;
                        case 6:
                            Intent itrack = new Intent(getActivity(), TrackandTraceReport.class);
                            startActivity(itrack);
                            getActivity().overridePendingTransition(R.anim.right_in, R.anim.left_out);
                            break;
                        case 7:
                            Intent itrackall = new Intent(getActivity(), TrackAllActivity.class);
                            startActivity(itrackall);
                            getActivity().overridePendingTransition(R.anim.right_in, R.anim.left_out);
                            break;
                        case 8:
                            startActivity(new Intent(getActivity(), StatusReport.class));
                            getActivity().overridePendingTransition(R.anim.right_in, R.anim.left_out);
                            break;
                        case 9:
                            startActivity(new Intent(getActivity(), VehicleDetails.class));
                            getActivity().overridePendingTransition(R.anim.right_in, R.anim.left_out);
                            break;

                    }
                }
            });
        }

        return rootView;
    }

}

package Fragment;


import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.riddhi.myr_track.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class AboutFragment extends Fragment {
    int[] imgid = {R.drawable.aboutusone, R.drawable.aboutustwo};
    AnimationDrawable rocketAnimation;

    public AboutFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_about, container, false);

        ImageView rocketImage = rootView.findViewById(R.id.ivAboutUsNew);
        rocketImage.setBackgroundResource(R.drawable.rocket_thrust);
        rocketAnimation = (AnimationDrawable) rocketImage.getBackground();
        rocketAnimation.start();
        return rootView;
    }

    @Override
    public void onPause() {
        rocketAnimation.stop();
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        rocketAnimation.start();
    }


}

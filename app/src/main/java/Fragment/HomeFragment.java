package Fragment;

import android.animation.ValueAnimator;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.riddhi.myr_track.ControlRoomActivity;
import com.riddhi.myr_track.Login;
import com.riddhi.myr_track.MainActivity;
import com.riddhi.myr_track.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;

import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import services.AsyncDashboardDetail;

/**
 * Created by Vinayak on 8/17/2018.
 */

public class HomeFragment extends Fragment {
    public static String stat_total_vehicle = "";
    public static String stat_ok_vehicle = "";
    public static String stat_in_garage = "";
    public static String stat_power_disconnect = "";
    public static String stat_device_problem = "";
    RelativeLayout center_relative;
    private TextView textView3, total_vehicle_value, ok_vehicle_value, in_garage_value, power_disconnect_value, device_problem_value, tv_company_name;
    private Button Btn_Logout,Btn_control_room;
    private SQLiteDatabase sqLiteDatabase = null;
    private String TABLE_NAME = "Login";
    private String DB_NAME = "DEMO_DB";
    CardView card_ok_vehicle;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        String customFont = "fonts/arlrdbd.ttf";
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), customFont);
        textView3 = rootView.findViewById(R.id.textView3);
        textView3.setTypeface(typeface);
        center_relative = rootView.findViewById(R.id.center_relative);
        Btn_Logout = rootView.findViewById(R.id.Btn_Logout);
        Btn_control_room = rootView.findViewById(R.id.Btn_control_room);
        card_ok_vehicle = rootView.findViewById(R.id.card_ok_vehicle);

        card_ok_vehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.viewPager.setCurrentItem(1);

            }
        });
        total_vehicle_value = rootView.findViewById(R.id.total_vehicle_value);
        ok_vehicle_value = rootView.findViewById(R.id.ok_vehicle_value);
        in_garage_value = rootView.findViewById(R.id.in_garage_value);
        power_disconnect_value = rootView.findViewById(R.id.power_disconnect_value);
        device_problem_value = rootView.findViewById(R.id.device_problem_value);
        tv_company_name = rootView.findViewById(R.id.tv_company_name);

        sqLiteDatabase = getActivity().openOrCreateDatabase(DB_NAME, getActivity().MODE_PRIVATE, null);

        Btn_Logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                // set title
                alertDialogBuilder.setTitle("LogOut");
                // set dialog message
                alertDialogBuilder
                        .setMessage("Are you Sure?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                sqLiteDatabase.execSQL("delete from " + TABLE_NAME);
                                Login.server = 72;
                                Login.tempHomeFrag = 1;
                                Login.tempVehicleFrag = 1;
                                Intent i = new Intent(getActivity(), Login.class);
                                startActivity(i);
                                getActivity().finish();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

        if(!stat_total_vehicle.equalsIgnoreCase(""))
        {
            center_relative.setVisibility(View.GONE);
            total_vehicle_value.setText("" + stat_total_vehicle);
            ok_vehicle_value.setText("" + stat_ok_vehicle);
            in_garage_value.setText("" + stat_in_garage);
            power_disconnect_value.setText("" + stat_power_disconnect);
            device_problem_value.setText("" + stat_device_problem);
            tv_company_name.setText("" + Login.companyname);
            //startCountAnimation();
        }

        Btn_control_room.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ControlRoomActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.right_in, R.anim.left_out);
            }
        });


        // Inflate the layout for this fragment
        return rootView;

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (Login.tempHomeFrag == 1) {
                Log.e("VehicleFragment", "---Visible()-->");
                populateDashboardData();
                //MainActivity.tempVal = 2;
            } else {
                center_relative.setVisibility(View.GONE);
                total_vehicle_value.setText("" + stat_total_vehicle);
                ok_vehicle_value.setText("" + stat_ok_vehicle);
                in_garage_value.setText("" + stat_in_garage);
                power_disconnect_value.setText("" + stat_power_disconnect);
                device_problem_value.setText("" + stat_device_problem);
                tv_company_name.setText("" + Login.companyname);
                //startCountAnimation();
            }
        } else {
            Log.e("VehicleFragment", "---Invisible()-->");
        }
    }

    private void startCountAnimation() {
        int temp = Integer.parseInt(stat_total_vehicle);
        ValueAnimator animator = ValueAnimator.ofInt(0, temp);
        animator.setDuration(2000);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                total_vehicle_value.setText(animation.getAnimatedValue().toString());
            }
        });
        animator.start();
    }

    private void populateDashboardData() {
        AsyncDashboardDetail lvAsysnc = new AsyncDashboardDetail();
        lvAsysnc.execute(Login.companyid, Login.userid, center_relative, HomeFragment.this);
    }

    public void setDashboardDataOnView(String total_vehicle, String ok_vehicle, String in_garage, String power_disconnect, String device_problem) {
        center_relative.setVisibility(View.GONE);

        stat_total_vehicle = "" + total_vehicle;
        stat_ok_vehicle = ok_vehicle;
        stat_in_garage = in_garage;
        stat_power_disconnect = power_disconnect;
        stat_device_problem = device_problem;
        total_vehicle_value.setText("" + stat_total_vehicle);
        ok_vehicle_value.setText("" + stat_ok_vehicle);
        in_garage_value.setText("" + stat_in_garage);
        power_disconnect_value.setText("" + stat_power_disconnect);
        device_problem_value.setText("" + stat_device_problem);
        tv_company_name.setText("" + Login.companyname);
        Login.tempHomeFrag = 2;
        //startCountAnimation();
    }
}

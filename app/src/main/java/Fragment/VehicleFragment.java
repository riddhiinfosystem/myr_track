package Fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.riddhi.myr_track.LiveTrackTraceSingleVehicleActivity;
import com.riddhi.myr_track.Login;
import com.riddhi.myr_track.MainActivity;
import com.riddhi.myr_track.R;
import com.riddhi.myr_track.TrackAllActivity;
import com.riddhi.myr_track.VehicleNearByPlaceActivity;
import com.riddhi.myr_track.trackvehicle;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import model.vehiclelist;
import services.ListVehicleAsyncNew;
import support.CustomVehicleAdapter;
import support.NearByVehicleAdapter;

/**
 * Created by Vinayak on 8/17/2018.
 */

public class VehicleFragment extends Fragment {
    private CustomVehicleAdapter adapter;
    private ListView recyclerView;
    public List<vehiclelist> myvehiclelist = new ArrayList<vehiclelist>();
    public List<vehiclelist> myvehiclelistsearch = new ArrayList<vehiclelist>();
    public AlphaAnimation alphaAnimation;
    Typeface fontRegular;
    EditText searchbox;
    CustomVehicleAdapter vehicleAdapter;
    Context context;
    RelativeLayout center_relative;
    private String searchmode;
    private RecyclerView.LayoutManager layoutManager;
    private TextView textView3;

    public VehicleFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.e("VehicleFragment", "--onCreateView()-->");
        View rootView = inflater.inflate(R.layout.fragment_vehicle, container, false);

        context = getActivity();


        String customFont = "fonts/arlrdbd.ttf";
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), customFont);
        textView3 = rootView.findViewById(R.id.textView3);
        textView3.setTypeface(typeface);

        searchmode = "normal";
        center_relative = rootView.findViewById(R.id.center_relative);
        searchbox = rootView.findViewById(R.id.txtsearchvehicle);

        recyclerView = rootView.findViewById(R.id.vehicle_recycler_view);
        recyclerView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView vehicleno1 = view.findViewById(R.id.tvvehiclenonew);
                TextView unitno = view.findViewById(R.id.tvvehiclereportedUnitNo);
                TextView reporteddatetime = view.findViewById(R.id.tvvehiclereporteddatetime);
                TextView lat = view.findViewById(R.id.tvlatnew);
                TextView lon = view.findViewById(R.id.tvlonnew);
                TextView location = view.findViewById(R.id.tvlocationnew);
                TextView speed = view.findViewById(R.id.tvvehiclespeed);
                vehiclelist vehiclelist;
                for(int v = 0; v < myvehiclelist.size();v++)
                {
                    vehiclelist = myvehiclelist.get(v);
                    if(vehiclelist.getVehicleno().toString().equalsIgnoreCase((vehicleno1.getText().toString()).replace(" ","")))
                    {
                        Intent ii = new Intent(context, LiveTrackTraceSingleVehicleActivity.class);
                        Bundle bb = new Bundle();
                        bb.putString("lat", ""+vehiclelist.getLat());
                        bb.putString("lon", ""+vehiclelist.getLon());
                        bb.putString("vehicleno", ""+vehiclelist.getVehicleno());
                        bb.putString("reporteddatetime", ""+vehiclelist.getLastreported());
                        bb.putString("unitNo", ""+vehiclelist.getUnitNo());
                        bb.putString("location", ""+vehiclelist.getLocation());
                        bb.putString("speed", ""+vehiclelist.getSpeed());
                        bb.putString("digital3", ""+vehiclelist.getDigital3());
                        bb.putString("ignition", ""+vehiclelist.getIgnition());
                        ii.putExtras(bb);
                        (context).startActivity(ii);
                    }
                }
            }
        });

        searchbox.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {

                if (cs != null && cs.toString().length() > 0) {
                    searchmode = "search";
                    myfilter(cs);
                } else {
                    searchmode = "normal";
                    listShow();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }
        });
        fontRegular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/futura.ttf");

        return rootView;
    }

    private void myfilter(CharSequence cs) {
        cs = cs.toString().toLowerCase();
        myvehiclelistsearch = new ArrayList<vehiclelist>();
        vehiclelist vl;
        String vehicleno, vehiclecity, combinestring, run;

        if (cs != null && cs.toString().length() > 0) {
            for (int i = 0; myvehiclelist.size() - 1 >= i; i++) {
                vl = myvehiclelist.get(i);
                vehicleno = vl.getVehicleno();
                vehiclecity = vl.getLocation();
                combinestring = vehicleno + vehiclecity;

                if (combinestring.toLowerCase().contains(cs)) {
                    myvehiclelistsearch.add(vl);
                }
            }
        }
        listShow();
    }

    private void listShow() {

        if (searchmode == "search") {
            vehicleAdapter = new CustomVehicleAdapter((ArrayList<vehiclelist>) myvehiclelistsearch, context);

        } else {
            vehicleAdapter = new CustomVehicleAdapter((ArrayList<vehiclelist>) myvehiclelist, context);
        }

        recyclerView.setAdapter(vehicleAdapter);

    }

    public void populatevehiclelistview(List<vehiclelist> VList)
    {
        if(VList.size()>0)
        {
            if(searchbox.getText().toString().equalsIgnoreCase(""))
            {
                adapter = new CustomVehicleAdapter((ArrayList<vehiclelist>) VList, context);
                // Assign adapter to ListView
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();

                /*recyclerView.getRecycledViewPool().clear();
                adapter = new CustomVehicleAdapter((ArrayList<vehiclelist>) VList, context);
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();*/

                searchbox.setVisibility(View.VISIBLE);
                Login.tempVehicleFrag = 2;
                center_relative.setVisibility(View.GONE);
            }
        }


    }

    private void populatevehiclelist()
    {
        Log.e("myvehiclelist size","==>"+myvehiclelist.size());
        ListVehicleAsyncNew lvAsysnc = new ListVehicleAsyncNew();
        lvAsysnc.execute(Login.companyid, myvehiclelist, center_relative, VehicleFragment.this, Login.userid);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (Login.tempVehicleFrag == 1)
            {
                Log.e("VehicleFragment", "---Visible()-->");
                LoadList();
            } else {
                center_relative.setVisibility(View.GONE);
            }
        } else {
            Log.e("VehicleFragment", "---Invisible()-->");
        }
    }

    public void LoadList() {
        Timer t = new Timer();
        t.scheduleAtFixedRate(
                new TimerTask() {
                    public void run()
                    {
                        Log.e("Timer--", "Called");
                        populatevehiclelist();

                    }
                }, 0, 1000 * 60 * 1
        );
    }
}

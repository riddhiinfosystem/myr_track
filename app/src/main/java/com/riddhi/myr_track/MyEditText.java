package com.riddhi.myr_track; /**
 * Created by ab on 19-May-16.
 */

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;


public class MyEditText extends EditText {

    public MyEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        //init();
    }

    public MyEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        // init();
    }

    public MyEditText(Context context) {
        super(context);
        //init();
    }


}
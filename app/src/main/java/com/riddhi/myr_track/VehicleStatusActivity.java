package com.riddhi.myr_track;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import model.VehicleStatus;
import services.AsyncVehicleStatus;
import services.AsyncVehicleStatusDetailSave;
import services.SearchPlaceAsync;
import support.InternetConnectionDetector;
import support.MyCustomAdapter;
import support.MyVehicleStatusBaseAdapter;

public class VehicleStatusActivity extends Activity {
    List<VehicleStatus> _myvehiclesList = new ArrayList<VehicleStatus>();
    List<VehicleStatus> _myvehiclesStatus = new ArrayList<VehicleStatus>();
    List<VehicleStatus> _myvehiclesControllingBranch = new ArrayList<VehicleStatus>();
    List<VehicleStatus> _myvehiclesTempList = new ArrayList<VehicleStatus>();
    List<VehicleStatus> _myvehiclesSearchList = new ArrayList<VehicleStatus>();
    ProgressDialog barProgressDialog;
    MyCustomAdapter dataAdapter = null;
    String _status = "", _status_Id, _vehicleId, _controllingBranch = "";
    private Spinner spVehicleControllingBranch;
    private Spinner spVehicleStatus;
    private ListView listView1;
    private TextView textView3;
    private InternetConnectionDetector _clsInternetConDetect;
    private Boolean _isInternetPresent = false;
    private Button btn_save;
    private EditText etDailyVehParaSelVeh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_vehicle_status);

        String customFont = "fonts/arlrdbd.ttf";
        Typeface typeface = Typeface.createFromAsset(getAssets(), customFont);
        textView3 = findViewById(R.id.textView3);
        textView3.setTypeface(typeface);

        barProgressDialog = ProgressDialog.show(VehicleStatusActivity.this, "Please wait ...", "Downloading...", true);
        barProgressDialog.setCancelable(false);

        spVehicleControllingBranch = findViewById(R.id.spVehicleControllingBranch);
        spVehicleStatus = findViewById(R.id.spVehicleStatus);

        btn_save = findViewById(R.id.btn_save);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (_isInternetPresent) {
                    if (_controllingBranch.equalsIgnoreCase("")) {
                        Toast.makeText(VehicleStatusActivity.this, "Please Select Vehicle Controlling Branch", Toast.LENGTH_SHORT).show();
                    } else if (!_status.equalsIgnoreCase("")) {
                        _vehicleId = "";
                        VehicleStatus vehicleStatus = null;
                        for (int i = 0; i < _myvehiclesList.size(); i++) {
                            vehicleStatus = _myvehiclesList.get(i);
                            if (vehicleStatus.isSelected() == true) {
                                if (_vehicleId.equalsIgnoreCase("")) {
                                    _vehicleId = "" + vehicleStatus.getVehicle_No();
                                } else {
                                    _vehicleId = "" + _vehicleId + "," + vehicleStatus.getVehicle_No();
                                }
                            }
                        }

                        if (_vehicleId.equalsIgnoreCase("")) {
                            Toast.makeText(VehicleStatusActivity.this, "Please Select Vehicle", Toast.LENGTH_SHORT).show();
                        } else {
                            barProgressDialog = ProgressDialog.show(VehicleStatusActivity.this, "Please wait ...", "Saving...", true);
                            barProgressDialog.setCancelable(false);

                            AsyncVehicleStatusDetailSave async = new AsyncVehicleStatusDetailSave();
                            async.execute(_status, _status_Id, Login.companyid, Login.userid, _vehicleId, VehicleStatusActivity.this);
                        }
                    } else {
                        Toast.makeText(VehicleStatusActivity.this, "Please Select Vehicle Status", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                            VehicleStatusActivity.this);
                    alertDialog.setTitle("Error");
                    alertDialog.setMessage("Internet Connection Problem");
                    alertDialog.setIcon(R.drawable.failtwo);
                    alertDialog.setCancelable(false);
                    alertDialog.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    finish();
                                    overridePendingTransition(R.anim.left_in, R.anim.right_out);
                                }
                            });
                    alertDialog.show();
                }
            }
        });

        _clsInternetConDetect = new InternetConnectionDetector(getApplicationContext());
        checkGpsAndInternetConnection();

        if (_isInternetPresent) {
            AsyncVehicleStatus async = new AsyncVehicleStatus();
            async.execute(Login.companyid, Login.userid, VehicleStatusActivity.this);
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                    VehicleStatusActivity.this);
            alertDialog.setTitle("Error");
            alertDialog.setMessage("Internet Connection Problem");
            alertDialog.setIcon(R.drawable.failtwo);
            alertDialog.setCancelable(false);
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog,
                                            int which) {
                            finish();
                            overridePendingTransition(R.anim.left_in, R.anim.right_out);
                        }
                    });
            alertDialog.show();
        }

        etDailyVehParaSelVeh = (EditText)findViewById(R.id.etDailyVehParaSelVeh);
        etDailyVehParaSelVeh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    etDailyVehParaSelVeh.setError(null);
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Exp - " + e.toString());
                }
            }
        });

        etDailyVehParaSelVeh.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                etDailyVehParaSelVeh.setError(null);

                if (cs != null && cs.toString().length() >= 0)
                {
                    if(cs.toString().length() <= 1 )
                    {
                        dataAdapter = new MyCustomAdapter(VehicleStatusActivity.this, _myvehiclesTempList);
                        listView1.setAdapter(dataAdapter);
                        dataAdapter.notifyDataSetChanged();
                    }
                    else
                    {
                        bindTempVehicleList(cs.toString());
                    }

                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });

        etDailyVehParaSelVeh.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (etDailyVehParaSelVeh.getRight() - etDailyVehParaSelVeh.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
                        etDailyVehParaSelVeh.setText("");

                        return true;
                    }
                }
                return false;
            }
        });

        listView1 = findViewById(R.id.listView1);

        spVehicleControllingBranch.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (spVehicleControllingBranch.getSelectedItemPosition() != 0) {

                    VehicleStatus vehicleStatus = _myvehiclesControllingBranch.get(position);
                    String tempControllingBranchID;
                    _controllingBranch = vehicleStatus.getVehicle_No();
                    tempControllingBranchID = vehicleStatus.getControlling_Branch_ID();
                    bindList(tempControllingBranchID);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spVehicleStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (spVehicleStatus.getSelectedItemPosition() != 0) {
                    VehicleStatus vehicleStatus = _myvehiclesStatus.get(position);
                    _status = vehicleStatus.getVehicle_No();
                    _status_Id = vehicleStatus.getControlling_Branch_ID();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void checkGpsAndInternetConnection() {
        _isInternetPresent = _clsInternetConDetect.isConnectingToInternet();
    }

    public void bindglobalarray(List<model.VehicleStatus> _myvehiclesControllingBranch, List<model.VehicleStatus> _myvehiclesList, List<model.VehicleStatus> _myvehiclesStatus) {
        barProgressDialog.dismiss();
        spVehicleControllingBranch.setAdapter(new MyVehicleStatusBaseAdapter(this, _myvehiclesControllingBranch));
        spVehicleStatus.setAdapter(new MyVehicleStatusBaseAdapter(this, _myvehiclesStatus));
        this._myvehiclesControllingBranch = _myvehiclesControllingBranch;
        this._myvehiclesList.clear();
        this._myvehiclesList = _myvehiclesList;
        this._myvehiclesStatus = _myvehiclesStatus;

        listView1.setAdapter(new MyCustomAdapter(this, _myvehiclesList));
        listView1.deferNotifyDataSetChanged();

        //spVehicleControllingBranch.
    }

    public void bindTempVehicleList(String tempControllingBranchID) {
        _myvehiclesSearchList.clear();
        VehicleStatus vehicleStatus = null;
        for (int i = 0; i < _myvehiclesTempList.size(); i++) {
            vehicleStatus = _myvehiclesTempList.get(i);
            if (vehicleStatus.getVehicle_No().toLowerCase().contains("" + tempControllingBranchID)) {
                _myvehiclesSearchList.add(vehicleStatus);
            }
        }
        dataAdapter = new MyCustomAdapter(this, _myvehiclesSearchList);
        listView1.setAdapter(dataAdapter);
        dataAdapter.notifyDataSetChanged();
    }

    public void bindList(String tempControllingBranchID) {
        _myvehiclesTempList.clear();
        VehicleStatus vehicleStatus = null;
        for (int i = 0; i < _myvehiclesList.size(); i++) {
            vehicleStatus = _myvehiclesList.get(i);
            if (vehicleStatus.getControlling_Branch_ID().equalsIgnoreCase("" + tempControllingBranchID) || tempControllingBranchID.equalsIgnoreCase("01") ) {
                _myvehiclesTempList.add(vehicleStatus);
            }
        }

        dataAdapter = new MyCustomAdapter(this, _myvehiclesTempList);
        // Assign adapter to ListView
        listView1.setAdapter(dataAdapter);
        dataAdapter.notifyDataSetChanged();
    }

    public void setNoDataFoundError() {
        barProgressDialog.dismiss();
        Toast.makeText(VehicleStatusActivity.this, "Control Branch Not Found", Toast.LENGTH_LONG).show();
        this.finish();
    }

    public void bindarray(List<model.VehicleStatus> _myvehiclesList2) {
        barProgressDialog.dismiss();
        if (_myvehiclesList2.size() > 0) {
            this._myvehiclesList.clear();
            this._myvehiclesTempList.clear();
            this._myvehiclesTempList = _myvehiclesList2;
            this._myvehiclesList = _myvehiclesList2;
            dataAdapter = new MyCustomAdapter(this, _myvehiclesList2);
            // Assign adapter to ListView
            listView1.setAdapter(dataAdapter);
            dataAdapter.notifyDataSetChanged();
            Toast.makeText(VehicleStatusActivity.this, "Data Save Successfully", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(VehicleStatusActivity.this, "Problem with data saving", Toast.LENGTH_LONG).show();
        }
    }

    public void bindarrayNew() {

        AsyncVehicleStatus async = new AsyncVehicleStatus();
        async.execute(Login.companyid, Login.userid, VehicleStatusActivity.this);
    }


}

package com.riddhi.myr_track;

/**
 * Created by abc on 2/13/2015.
 */
public class SpinnerList {

    private String type;
    private int id;


    public SpinnerList(int id, String type) {
        this.type = type;
        this.id = id;
    }


    public int getLocId() {
        return id;
    }

    public String getType() {
        return type;
    }
}


package com.riddhi.myr_track;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import model.Searchlist;
import services.AsyncDateTimeVehicleAnalysis;
import support.InternetConnectionDetector;

public class DailyAnalysisTimeBaseReportList extends Activity {

    ListView listView;
    TextView textViewVehcileNo;
    ProgressDialog barProgressDialog;
    List<Searchlist> searchlistList = new ArrayList<Searchlist>();

    String vehicleno;
    String vehicleid;
    String unitno;
    String datefrom;
    String dateto;
    String fromTime, toTime;
    ReportListAdapter adaptercustom;
    private Boolean _isInternetPresent = false;
    private InternetConnectionDetector _clsInternetConDetect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_daily_analysis_time_base_report_list);
        //getActionBar().hide();

        barProgressDialog = ProgressDialog.show(DailyAnalysisTimeBaseReportList.this, "Please wait...", "Loading...", true);
        barProgressDialog.setCancelable(false);

        textViewVehcileNo = findViewById(R.id.tvvehicleno);
        listView = findViewById(R.id.lstSearchReportList);

        Intent intent = getIntent();

        vehicleno = intent.getStringExtra("vehicleno");
        vehicleid = intent.getStringExtra("vehicleid");
        unitno = intent.getStringExtra("unitno");
        datefrom = intent.getStringExtra("fromdate");
        dateto = intent.getStringExtra("todate");
        fromTime = intent.getStringExtra("fromTime");
        toTime = intent.getStringExtra("toTime");

        _clsInternetConDetect = new InternetConnectionDetector(getApplicationContext());

        checkGpsAndInternetConnection();
        if (_isInternetPresent) {
            populatevehiclelist();
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                    DailyAnalysisTimeBaseReportList.this);
            alertDialog.setTitle("Error");
            alertDialog.setMessage("Internet Connection Problem");
            alertDialog.setIcon(R.drawable.failtwo);
            alertDialog.setCancelable(false);
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog,
                                            int which) {
                            finish();
                            overridePendingTransition(R.anim.left_in, R.anim.right_out);

                        }
                    });
            alertDialog.show();
        }
        textViewVehcileNo.setText(vehicleno);
    }

    private void checkGpsAndInternetConnection() {

        _isInternetPresent = _clsInternetConDetect.isConnectingToInternet();
    }

    private void populatevehiclelist() {
        AsyncDateTimeVehicleAnalysis lvAsysnc = new AsyncDateTimeVehicleAnalysis();
        lvAsysnc.execute(vehicleid, unitno, datefrom, dateto, searchlistList, DailyAnalysisTimeBaseReportList.this, fromTime, toTime);
    }

    public void populatevehiclelistview(List<Searchlist> VList) {

        barProgressDialog.dismiss();
        adaptercustom = new ReportListAdapter(VList);
        listView.setAdapter(adaptercustom);
    }

    public void setNullError() {

        barProgressDialog.dismiss();

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                DailyAnalysisTimeBaseReportList.this);
        alertDialog.setTitle("Error");
        alertDialog.setMessage("Internet Connection Problem !");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog,
                                        int which) {
                        finish();
                        overridePendingTransition(R.anim.left_in, R.anim.right_out);
                    }
                });
        alertDialog.show();
    }

    public void setNoDataFoundError() {

        barProgressDialog.dismiss();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                DailyAnalysisTimeBaseReportList.this);
        alertDialog.setTitle("Error");
        alertDialog.setMessage("No Record Found !");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog,
                                        int which) {
                        finish();
                        overridePendingTransition(R.anim.left_in, R.anim.right_out);
                    }
                });
        alertDialog.show();
    }

    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    private class ReportListAdapter extends ArrayAdapter<Searchlist> {
        List<Searchlist> vlistinternal;

        public ReportListAdapter(List<Searchlist> vList) {
            super(DailyAnalysisTimeBaseReportList.this, R.layout.searchlistfiltershow, vList);
            vlistinternal = vList;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View vehicleinfoview = convertView;
            if (vehicleinfoview == null) {

                vehicleinfoview = getLayoutInflater().inflate(R.layout.searchlistfilterfortimebase, parent, false);
            }

            Searchlist searchlist = searchlistList.get(position);


            LinearLayout linearLayout = vehicleinfoview.findViewById(R.id.linearLayoutsearchlistfiltershowone);

            Double stringTotRunKm = Double.parseDouble(searchlist.getTotalrunningkms());

            TextView textViewsrno = vehicleinfoview.findViewById(R.id.textView2);
            TextView textViewstart = vehicleinfoview.findViewById(R.id.textView3);
            TextView textViewdate = vehicleinfoview.findViewById(R.id.textView4);
            TextView textViewTotalRunningKms = vehicleinfoview.findViewById(R.id.textView5);
            TextView textViewTotalRunningTimes = vehicleinfoview.findViewById(R.id.textView6);
            TextView textViewTotalStopageTime = vehicleinfoview.findViewById(R.id.textView7);

            vehicleinfoview.setBackgroundResource(R.color.light_gray);
            textViewsrno.setTextColor(getResources().getColor(R.color.black_color));
            textViewstart.setTextColor(getResources().getColor(R.color.black_color));
            textViewdate.setTextColor(getResources().getColor(R.color.black_color));
            textViewTotalRunningKms.setTextColor(getResources().getColor(R.color.black_color));
            textViewTotalRunningTimes.setTextColor(getResources().getColor(R.color.black_color));
            textViewTotalStopageTime.setTextColor(getResources().getColor(R.color.black_color));


            textViewsrno.setText(searchlist.getSrno());
            textViewstart.setText(searchlist.getStartlocation());
            textViewdate.setText(searchlist.getEndlocation());

            if (stringTotRunKm >= 0.00) {
                textViewTotalRunningKms.setText(stringTotRunKm.toString());
            } else {
                textViewTotalRunningKms.setText("");
            }

            textViewTotalRunningTimes.setText(searchlist.getTotalrunningtime());
            textViewTotalStopageTime.setText(searchlist.getTotalstopagetime());

            return vehicleinfoview;
        }
    }
}

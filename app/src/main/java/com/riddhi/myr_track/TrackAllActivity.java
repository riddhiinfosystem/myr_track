package com.riddhi.myr_track;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import model.NearByVehicleList;
import services.AsyncNearByVehicleData;
import services.AsyncTrackAllVehicleOnMap;
import services.SearchListAsync;
import support.InternetConnectionDetector;
import support.NearByVehicleAdapter;


public class TrackAllActivity extends FragmentActivity implements OnMapReadyCallback {

    public ArrayList<String> locationTypeList;
    List<NearByVehicleList> nearByVehicle = new ArrayList<NearByVehicleList>();
    ProgressDialog barProgressDialog;

    public ProgressBar progressBar;
    String lat, lon, vehicleno, reporteddatetime;
    String location, speed;
    Context context;
    LinearLayout layout;
    Marker myMarker;
    private com.google.android.gms.maps.GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private int mode = 0;
    private Boolean _isInternetPresent = false;
    private InternetConnectionDetector _clsInternetConDetect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_all);

        context = this;

        layout = new LinearLayout(this);
        progressBar = findViewById(R.id.progressBar);

        locationTypeList = new ArrayList<String>();
        barProgressDialog = ProgressDialog.show(TrackAllActivity.this, "Please wait", "Location loading...", true);
        barProgressDialog.setCancelable(false);

        _clsInternetConDetect = new InternetConnectionDetector(getApplicationContext());
        checkGpsAndInternetConnection();

        if (_isInternetPresent) {
            AsyncTrackAllVehicleOnMap async = new AsyncTrackAllVehicleOnMap();
            async.execute(Login.companyid, Login.userid, TrackAllActivity.this);
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                    TrackAllActivity.this);
            alertDialog.setTitle("Error");
            alertDialog.setMessage("Internet Connection Problem");
            alertDialog.setIcon(R.drawable.failtwo);
            alertDialog.setCancelable(false);
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                            overridePendingTransition(R.anim.left_in, R.anim.right_out);
                        }
                    });
            alertDialog.show();
        }
    }


    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    public void onMapReady(com.google.android.gms.maps.GoogleMap googleMap) {
        // Check if we were successful in obtaining the map.
        Log.e("onMapReady-->","called");
        if (mMap != null) {
            return;
        }
        mMap = googleMap;
        startDemo();

    }

    private void checkGpsAndInternetConnection() {

        _isInternetPresent = _clsInternetConDetect.isConnectingToInternet();

    }

    private void setUpMap() {
        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
    }

    protected void startDemo() {

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(21.7679, 78.8718), 5));



        if (mode == 0) {
            mMap.setMapType(com.google.android.gms.maps.GoogleMap.MAP_TYPE_NORMAL);
        } else {
            mMap.setMapType(com.google.android.gms.maps.GoogleMap.MAP_TYPE_SATELLITE);
        }

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        //When Map Loads Successfully
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                //Your code where exception occurs goes here...
                NearByVehicleList nearByVehicleList;

                for(int r = 0; r < nearByVehicle.size(); r++)
                {
                    nearByVehicleList = nearByVehicle.get(r);

                    vehicleno = ""+nearByVehicleList.getVehicleno();
                    speed = ""+nearByVehicleList.getSpeed();
                    reporteddatetime = ""+nearByVehicleList.getDate();
                    location = ""+nearByVehicleList.getLocation();

                    LatLng myloc = new LatLng(Double.valueOf(""+nearByVehicleList.getLatitude()), Double.valueOf(""+nearByVehicleList.getLongitude()));
                    float hue;
                    hue = BitmapDescriptorFactory.HUE_GREEN;
                    int speeds = Integer.parseInt(""+speed);
                    if(speeds <= 5)
                    {
                        hue = BitmapDescriptorFactory.HUE_RED;
                    }
                    mMap.addMarker(new MarkerOptions()
                            .position(myloc)
                            .title(vehicleno+"_"+reporteddatetime+"_"+speed+"_"+location)
                            .snippet("")
                            .icon(BitmapDescriptorFactory.defaultMarker(hue)));

                    mMap.setInfoWindowAdapter(new com.google.android.gms.maps.GoogleMap.InfoWindowAdapter() {
                        @Override
                        public View getInfoWindow(Marker arg0) {
                            return null;
                        }

                        @Override
                        public View getInfoContents(Marker arg0) {
                            View v = getLayoutInflater().inflate(R.layout.info_window_layout, null);
                            // Getting the position from the marker
                            String str=arg0.getTitle();
                            final String[] str2=str.split("_");

                            // Getting reference to the TextView to set latitude
                            TextView tvTrackVehicleVehicleNo = v.findViewById(R.id.tvTrackVechielNo);

                            // Getting reference to the TextView to set longitude
                            TextView tvTrackVehicleSpeed = v.findViewById(R.id.tvTrackVehicleSpeed);

                            TextView tvTrackVehicleDate = v.findViewById(R.id.tvTrackVehicleTime);

                            final TextView tvTrackVehicleLocation = v.findViewById(R.id.tvTrackVehicleLocation);

                            //TextView markLocationView = (TextView) v.findViewById(R.id.markLocationView);
                            // Setting the latitude
                            tvTrackVehicleVehicleNo.setText("Vehicle No :" + str2[0]);
                            // Setting the longitude
                            tvTrackVehicleSpeed.setText("Speed :" + str2[2]);

                            tvTrackVehicleDate.setText("Date & Time :" + str2[1]);

                            tvTrackVehicleLocation.setText("Location :" + str2[3]);
                            // Returning the view containing InfoWindow contents
                            return v;
                        }
                    });
                }

                barProgressDialog.dismiss();
                mMap.setMyLocationEnabled(false);
                mMap.setTrafficEnabled(true);

            }
        });


    }

    public void setNullError() {

        barProgressDialog.dismiss();

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                TrackAllActivity.this);
        alertDialog.setTitle("Error");
        alertDialog.setMessage("Internet Connection Problem !");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog,
                                        int which) {
                    }
                });
        alertDialog.show();

    }

    public void setNoDataFoundError() {

        barProgressDialog.dismiss();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                TrackAllActivity.this);
        alertDialog.setTitle("Error");
        alertDialog.setMessage("No Record Found");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog,
                                        int which) {

                    }
                });
        alertDialog.show();

    }

    public void setNearByVehicleData(List<NearByVehicleList> _myNearByVehicleList) {
        if(_myNearByVehicleList.size() > 0)
        {
            this.nearByVehicle = _myNearByVehicleList;
            setUpMap();
        }
        else
        {
            barProgressDialog.dismiss();
        }


        Log.e("nearByVehicle_size", "---->" + nearByVehicle.size());
    }


}

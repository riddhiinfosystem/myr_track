package com.riddhi.myr_track;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.androidplot.xy.XYPlot;

import org.achartengine.ChartFactory;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import services.AsyncTempPlot;
import support.InternetConnectionDetector;


public class TempraturePlotGraph extends Activity {
    String stringTempPlotVehicleNo;
    String stringTempPlotVehicleId;
    String stringTempPlotUnitNo;
    String stringTempPlotFromDate;
    String stringTempPlotToDate;
    String fromTime;
    String toTime;
    ProgressBar pbTempPlotGraph;
    LinearLayout linearLayoutTempPlotGraph;

    String[] tpgdateArray;
    String[] tpgsensorO1Array;
    String[] tpgsensorA1Array;
    String[] tpgSensorA2Array;
    String[] tpgSensorO2Array;
    String[] tpgSensorO3Array;
    int[] x;
    Double[] dSensorA1;
    Double[] dSensorA2;
    Double[] dSensorO1;
    Double[] dSensorO2;
    Double[] dSensorO3;
    int[] margins = {10, 30, 0, 10};

    private Boolean _isInternetPresent = false;
    private InternetConnectionDetector _clsInternetConDetect;

    private XYPlot xyPlot;

   /* private String[] mMonth = new String[] {
            "Jan", "Feb" , "Mar", "Apr", "May", "Jun",
            "Jul", "Aug" , "Sep", "Oct", "Nov", "Dec",
            "Jan", "Feb" , "Mar", "Apr", "May", "Jun",
            "Jul", "Aug" , "Sep", "Oct", "Nov", "Dec",
    };*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_temprature_plot_graph);
        //getActionBar().hide();

        /*ActionBar actionBar = getActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ff5f8aed")));*/


        pbTempPlotGraph = findViewById(R.id.pbTempPlotGraph);
        linearLayoutTempPlotGraph = findViewById(R.id.linearLayoutTempPlotGraphOne);
        pbTempPlotGraph.setVisibility(View.VISIBLE);
        linearLayoutTempPlotGraph.setVisibility(View.GONE);
        _clsInternetConDetect = new InternetConnectionDetector(getApplicationContext());

        Intent i = getIntent();
        stringTempPlotVehicleNo = i.getStringExtra("vehicleno");
        stringTempPlotVehicleId = i.getStringExtra("vehicleid");
        stringTempPlotUnitNo = i.getStringExtra("unitno");
        stringTempPlotFromDate = i.getStringExtra("fromdate");
        stringTempPlotToDate = i.getStringExtra("todate");
        fromTime = i.getStringExtra("fromTime");
        toTime = i.getStringExtra("toTime");

        checkGpsAndInternetConnection();

        if (_isInternetPresent) {
            AsyncTempPlot asyncTempPlot = new AsyncTempPlot();
            asyncTempPlot.execute(stringTempPlotUnitNo, stringTempPlotFromDate, stringTempPlotToDate, pbTempPlotGraph, Login.companyid, TempraturePlotGraph.this, linearLayoutTempPlotGraph, Login.userid, fromTime, toTime);
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                    TempraturePlotGraph.this);
            alertDialog.setTitle("Error");
            alertDialog.setMessage("Internet Connection Problem");
            alertDialog.setIcon(R.drawable.failtwo);
            alertDialog.setCancelable(false);
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog,
                                            int which) {
                            finish();
                            /*Intent intent = new Intent(getApplicationContext(),TempratureReport.class);
                            startActivity(intent);*/
                            overridePendingTransition(R.anim.left_in, R.anim.right_out);
                        }
                    });
            alertDialog.show();
        }


    }

    private void checkGpsAndInternetConnection() {

        _isInternetPresent = _clsInternetConDetect.isConnectingToInternet();

    }

    public void bindglobalarray(String[] unitnoArray, String[] vehicleNoArray, final String[] dateArray, String[] timeArray, String[] sensorO1Array, String[] sensorO2Array, String[] sensorO3Array, String[] sensorA1Array, String[] sensorA2Array) {

        tpgdateArray = dateArray;
        tpgsensorA1Array = sensorA1Array;
        tpgSensorA2Array = sensorA2Array;
        tpgsensorO1Array = sensorO1Array;
        tpgSensorO2Array = sensorO2Array;
        tpgSensorO3Array = sensorO3Array;
        x = new int[tpgdateArray.length];
        for (int i = 0; i < tpgdateArray.length; i++) {
            x[i] = i;
        }

       /*  dSensorA1 = new Double[tpgsensorA1Array.length];
        for(int i = 0; i<tpgsensorA1Array.length;i++)
        {
            dSensorA1[i] = Double.valueOf(tpgsensorA1Array[i]);
        }

        dSensorA2 = new Double[tpgSensorA2Array.length];
        for(int i = 0; i<tpgSensorA2Array.length;i++)
        {
            dSensorA2[i] = Double.valueOf(tpgSensorA2Array[i]);
        }*/

        dSensorO1 = new Double[tpgsensorO1Array.length];
        for (int i = 0; i < tpgsensorO1Array.length; i++) {
            dSensorO1[i] = Double.valueOf(tpgsensorO1Array[i]);
        }
        dSensorO2 = new Double[tpgSensorO2Array.length];
        for (int i = 0; i < tpgSensorO2Array.length; i++) {
            dSensorO2[i] = Double.valueOf(tpgSensorO2Array[i]);
        }

        dSensorO3 = new Double[tpgSensorO3Array.length];
        for (int i = 0; i < tpgSensorO2Array.length; i++) {
            dSensorO3[i] = Double.valueOf(tpgSensorO3Array[i]);
        }

        openChart();
    }

    private void openChart() {

      /*  String[] p = {"A","A","A","A","A","A","A","A","A","A","A","A","A","A","A","A"};
        int[] x = { 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
        Double[] income = { 2000.500,2500.20,2700.50,3000.65,2800.89,3500.36,3700.36,3800.89,2000.79,2500.36,2700.36,3000.85,2800.36,3500.89,3700.78,3800.87};
        Double[] expense = {2200.40, 2700.74, 2900.85, 2800.65, 2600.47, 3000.87, 3300.78, 3400.69,2200.78, 2700.54, 2900.78, 2800.56, 2600.78, 3000.56, 3300.78, 3400.56 };
*/
        // Creating an  XYSeries for Income
      /*  XYSeries sensorAOneSeries = new XYSeries("SensorA1");
        // Creating an  XYSeries for Income
        XYSeries sensorATwoSeries = new XYSeries("SensorA2");
        // Adding data to Income and Expense Series*/
        XYSeries sensorOOneSeries = new XYSeries("SensorO1");
        XYSeries sensorOTwoSeries = new XYSeries("SensorO2");
        XYSeries sensorOThreeSeries = new XYSeries("SensorO3");

        for (int i = 0; i < x.length; i++) {
           /* sensorAOneSeries.add(dSensorA1[i],dSensorA1[i]);
            sensorATwoSeries.add(dSensorA2[i],dSensorA2[i]);*/
            sensorOOneSeries.add(x[i], dSensorO1[i]);
            sensorOTwoSeries.add(x[i], dSensorO2[i]);
            sensorOThreeSeries.add(x[i], dSensorO3[i]);
        }

        // Creating a dataset to hold each series
        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
        // Adding Income Series to the dataset
        /*dataset.addSeries(sensorAOneSeries);
        // Adding Expense Series to dataset
        dataset.addSeries(sensorATwoSeries);*/
        dataset.addSeries(sensorOOneSeries);
        dataset.addSeries(sensorOTwoSeries);
        dataset.addSeries(sensorOThreeSeries);


      /*  // Creating XYSeriesRenderer to customize incomeSeries
        XYSeriesRenderer sensoratworenderer = new XYSeriesRenderer();
        sensoratworenderer.setColor(Color.BLUE);
        sensoratworenderer.setPointStyle(PointStyle.CIRCLE);
        sensoratworenderer.setFillPoints(true);
        sensoratworenderer.setLineWidth(2);
        sensoratworenderer.setDisplayChartValues(true);

        // Creating XYSeriesRenderer to customize expenseSeries
        XYSeriesRenderer sensoraoneRenderer = new XYSeriesRenderer();
        sensoraoneRenderer.setColor(Color.YELLOW);
        sensoraoneRenderer.setPointStyle(PointStyle.CIRCLE);
        sensoraoneRenderer.setFillPoints(true);
        sensoraoneRenderer.setLineWidth(2);
        sensoraoneRenderer.setDisplayChartValues(true);*/

        XYSeriesRenderer sensorooneRenderer = new XYSeriesRenderer();
        sensorooneRenderer.setColor(Color.BLUE);
        sensorooneRenderer.setPointStyle(PointStyle.CIRCLE);
        sensorooneRenderer.setFillPoints(true);

        sensorooneRenderer.setLineWidth(2);
        sensorooneRenderer.setDisplayChartValues(true);

        XYSeriesRenderer sensorotwoRenderer = new XYSeriesRenderer();
        sensorotwoRenderer.setColor(Color.RED);
        sensorotwoRenderer.setPointStyle(PointStyle.CIRCLE);
        sensorotwoRenderer.setFillPoints(true);
        sensorotwoRenderer.setLineWidth(2);
        sensorotwoRenderer.setDisplayChartValues(true);

        XYSeriesRenderer sensorothreeRenderer = new XYSeriesRenderer();
        sensorothreeRenderer.setColor(Color.GREEN);
        sensorothreeRenderer.setPointStyle(PointStyle.CIRCLE);
        sensorothreeRenderer.setFillPoints(true);
        sensorothreeRenderer.setLineWidth(2);
        sensorothreeRenderer.setDisplayChartValues(true);


        // Creating a XYMultipleSeriesRenderer to customize the whole chart
        XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
        multiRenderer.setXLabels(0);
        multiRenderer.setChartTitle("Temprature Chart");
        multiRenderer.setChartTitleTextSize(20);
        multiRenderer.setApplyBackgroundColor(true);
        multiRenderer.setBackgroundColor(Color.BLACK);
        multiRenderer.setGridColor(Color.BLACK);
        multiRenderer.setXTitle("Date and Time");
        multiRenderer.setAxisTitleTextSize(18);
        multiRenderer.setYLabelsAlign(Paint.Align.LEFT);
        multiRenderer.setXLabelsAlign(Paint.Align.CENTER);
        multiRenderer.setLegendTextSize(18);
        multiRenderer.setLabelsTextSize(18);
        multiRenderer.setYTitle("Temprature");
        multiRenderer.setZoomButtonsVisible(true);
        multiRenderer.setXAxisMin(0);
        multiRenderer.setXAxisMax(4);

        for (int i = 0; i < x.length; i++) {
            multiRenderer.addXTextLabel(i + 1, tpgdateArray[i]);
        }

        // Adding incomeRenderer and expenseRenderer to multipleRenderer
        // Note: The order of adding dataseries to dataset and renderers to multipleRenderer
        // should be same
       /* multiRenderer.addSeriesRenderer(sensoraoneRenderer);
        multiRenderer.addSeriesRenderer(sensoratworenderer);*/
        multiRenderer.addSeriesRenderer(sensorooneRenderer);
        multiRenderer.addSeriesRenderer(sensorotwoRenderer);
        multiRenderer.addSeriesRenderer(sensorothreeRenderer);

        // Creating an intent to plot line chart using dataset and multipleRenderer
        Intent intent = ChartFactory.getLineChartIntent(getBaseContext(), dataset, multiRenderer);

        // Start Activity
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        /*Intent i = new Intent(getApplicationContext(),TempratureReport.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);*/
        finish();

        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }


    public void setNullError() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                TempraturePlotGraph.this);
        alertDialog.setTitle("Error");
        alertDialog.setMessage("Internet Connection Problem !");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog,
                                        int which) {
                        finish();
                        /*Intent newIntent = new Intent(getApplicationContext(),TempratureReport.class);
                        startActivity(newIntent);*/
                        overridePendingTransition(R.anim.left_in, R.anim.right_out);
                    }
                });
        alertDialog.show();

    }

    public void setDataNotFindError() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                TempraturePlotGraph.this);
        alertDialog.setTitle("Error");
        alertDialog.setMessage("Record not Available!");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog,
                                        int which) {
                        finish();
                        /*Intent newIntent = new Intent(getApplicationContext(),TempratureReport.class);
                        startActivity(newIntent);*/
                        overridePendingTransition(R.anim.left_in, R.anim.right_out);
                    }
                });
        alertDialog.show();

    }
}

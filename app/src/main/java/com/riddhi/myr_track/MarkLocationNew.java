package com.riddhi.myr_track;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.BounceInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import services.AsynMarkLocationNew;
import services.AsyncMarkLocGetAddress;
import services.AsyncMarkLocaNewSaveLoc;
import support.InternetConnectionDetector;


public class MarkLocationNew extends FragmentActivity implements LocationListener, OnMapReadyCallback {

    //------------------------------------------
    public static final long NOTIFY_INTERVAL = 1 * 5000; // 10 seconds
    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 2;
    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 5 * 1;
    public ProgressDialog barProgressDialog;
    public List<SpinnerList> spinnerLists = new ArrayList<SpinnerList>();
    public Location _Location;
    public Marker myMarker;
    public int locationInterval = 2000;
    public int tempInterval = 0;
    // flag for GPS status
    boolean isGPSEnabled = false;
    // flag for network status
    boolean isNetworkEnabled = false;
    // flag for GPS status
    boolean canGetLocation = false;
    // flag for GPS Tracking is enabled
    boolean isGPSTrackingEnabled = false;
    double latitude;
    double longitude;
    LatLng latLng;
    //    All Tools of Activity
    private Button _btMarkLocNewMarkLocation;
    private Button _btMarkLocNewRefresh;
    private Button _btMarkLocNewMapType;
    private TextView _tvMarklLocAccuracy;
    private EditText _etMarkLocLocationAddress;
    private Spinner _spnMarkLocNew;
    private ProgressBar _pbMarkLocNew;
    private AlphaAnimation alphaAnimation;
    private Boolean _isInternetPresent = false;
    private Boolean _isGpsPresent = false;
    private Boolean isLocationAvailable = false;
    // Asynk Location Class
    private AsynMarkLocationNew asynMarkLocationNew;
    private List<String> list = new ArrayList<String>();
    private String selectedItem;
    private String selectedCountry = null;
    private Float _flAccuracy = 0.0f;
    private String _StringFinalAddress = "";
    private int locTypeId;
    private String _stringCityName = "";
    private Double _dbLongitude = 0.00;
    private Double _dbLatitude = 0.00;
    private Double _dbNewLatitude = 0.00;
    private Double _dbNewLongitude = 0.00;
    private LocationListener _LocationListener;
    private LocationManager _LocationManager;
    private Long _lngTime;
    private Long _lngCurrentTime;
    private InternetConnectionDetector _clsInternetConDetect;
    private int _intCheckLoc = 0;
    private int btnCondition = 0;
    private int mapType;
    private com.google.android.gms.maps.GoogleMap googleMap;
    // run on another Thread to avoid crash
    private Handler mHandler = new Handler();
    // timer handling
    private Timer mTimer = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_mark_location_new);

        allInitialization();
        checkGpsAndInternetConnection();

        if (_isInternetPresent) {
            if (_isGpsPresent) {
                allVisibleInvisibleEvent();

                _Location = null;

                final Handler handler = new Handler();
                final Runnable task = new Runnable() {
                    @Override
                    public void run() {
                        Log.e("MainActivity", "Doing task");

                        if (_Location != null || tempInterval > 10000) {
                            handler.removeMessages(0);
                        } else {
                            getLocation();
                        }

                        handler.postDelayed(this, locationInterval);
                        tempInterval = tempInterval + 1000;
                    }
                };

                handler.post(task);

                showProgressBar();
                callAsyncTask();

                // Button Click Listener of Refresh Button
                _btMarkLocNewRefresh.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        v.startAnimation(alphaAnimation);
                        finish();
                        Intent intent = new Intent(getApplicationContext(), MarkLocationNew.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                    }
                });

                // EditText Mark Location Address Click Listener
                _etMarkLocLocationAddress.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (ActivityCompat.checkSelfPermission(MarkLocationNew.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MarkLocationNew.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        // _LocationManager.removeUpdates(_LocationListener);
                    }
                });

                // Button Click Listener to save The GPS Location
                _btMarkLocNewMarkLocation.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        v.startAnimation(alphaAnimation);

                        if (ActivityCompat.checkSelfPermission(MarkLocationNew.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MarkLocationNew.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        //_LocationManager.removeUpdates(_LocationListener);

                        if (_etMarkLocLocationAddress.getText().toString().equals("")) {
                            Toast.makeText(getApplicationContext(), "Please wait for getting Location!", Toast.LENGTH_SHORT).show();
                        } else {
                            saveMarkLocation();
                        }
                    }
                });

                _btMarkLocNewMapType.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        v.startAnimation(alphaAnimation);
                        changeMapType();
                    }
                });
            } else {
                showAlertDialog(MarkLocationNew.this, "Gps Not Available", "Please Turn on GPS from Setting!", false);
            }
        } else {
            showAlertDialog(MarkLocationNew.this, "No Internet Connection", "You don't have Internet connection!", false);
        }
    }

    public void getLocation() {
        try {
            _LocationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
            isGPSEnabled = _LocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            // getting network status
            isNetworkEnabled = _LocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                Log.d("Network not Available", "Network------>");
                // no network provider is enabled
            } else {
                this.canGetLocation = true;
                this.isGPSTrackingEnabled = true;
                // First get location from Network Provider
                if (isNetworkEnabled) {
                    this.isGPSTrackingEnabled = true;
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    _LocationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    Log.e("Network", "Network");
                    if (_LocationManager != null) {
                        _Location = _LocationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                        if (_Location != null) {
                            latitude = _Location.getLatitude();
                            longitude = _Location.getLongitude();
                            _Location.getAccuracy();
                        }
                    }
                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    this.isGPSTrackingEnabled = true;
                    if (_Location == null) {
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                        }

                        _LocationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                        Log.d("GPS Enabled", "GPS Enabled");
                        if (_LocationManager != null) {
                            _Location = _LocationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);

                            if (_Location != null) {

                                latitude = _Location.getLatitude();
                                longitude = _Location.getLongitude();
                                _Location.getAccuracy();
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveMarkLocation() {

        showProgressBar();
        _StringFinalAddress = _etMarkLocLocationAddress.getText().toString();
        String state = "";
        String sLatitude = String.valueOf(_dbLatitude);
        String sLongitude = String.valueOf(_dbLongitude);
        int iLocTypeId = getLocationTypeId();

        AsyncMarkLocaNewSaveLoc asyncMarkLocation = new AsyncMarkLocaNewSaveLoc();
        asyncMarkLocation.execute(MarkLocationNew.this, _StringFinalAddress, sLatitude, sLongitude, _stringCityName, state, iLocTypeId, Login.companyid, Login.userid);
    }

    private void allVisibleInvisibleEvent() {

        _btMarkLocNewMarkLocation.setVisibility(View.GONE);
        _pbMarkLocNew.setVisibility(View.VISIBLE);
    }

    private int getLocationTypeId() {
        if (selectedItem.equals("General")) {
            locTypeId = 0;
        }
        if (selectedItem.equals("Branch")) {
            locTypeId = 1;
        }
        if (selectedItem.equals("Customer")) {
            locTypeId = 2;
        }
        if (selectedItem.equals("Godown")) {
            locTypeId = 3;
        }
        if (selectedItem.equals("Petrol Pump")) {
            locTypeId = 4;
        }
        if (selectedItem.equals("Toll Naka")) {
            locTypeId = 5;
        }
        return locTypeId;
    }

    /*
     * Alert Dialog Method
     * */

    public void showAlertDialog(Context context, String title, String message, Boolean status) {

        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.successtwo : R.drawable.failtwo);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finish();
                /*Intent intent = new Intent(getApplicationContext(), ReportList.class);
                startActivity(intent);*/
                overridePendingTransition(R.anim.left_in, R.anim.right_out);
            }
        });
        alertDialog.show();
    }

    /*
     * AsyncVehicleAnalysisRootBase Task Method
     * */
    private void callAsyncTask() {

        asynMarkLocationNew.execute(MarkLocationNew.this, spinnerLists);
    }

    /*
     * Check Internet Connection  Method
     * */
    private void checkGpsAndInternetConnection() {

        _isInternetPresent = _clsInternetConDetect.isConnectingToInternet();
        _isGpsPresent = _LocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

    }

    /*
     * Show Progress Bar Method
     * */
    private void showProgressBar() {
        barProgressDialog = ProgressDialog.show(MarkLocationNew.this, "Please wait ...", "Downloading...", true);
        barProgressDialog.setCancelable(false);
    }

    /*
     * All Initialization of Tools and Classes
     * */
    private void allInitialization() {

        _btMarkLocNewMarkLocation = findViewById(R.id.btMarkLocNewMarkLocation);
        _btMarkLocNewRefresh = findViewById(R.id.btMarkLocNewRefresh);
        _btMarkLocNewMapType = findViewById(R.id.btMarkLocNewMapType);
        _tvMarklLocAccuracy = findViewById(R.id.tvMarkLocNewAccuracy);
        _etMarkLocLocationAddress = findViewById(R.id.etMarkLocNewLocationAddress);
        _spnMarkLocNew = findViewById(R.id.spinMarkLocNew);
        _pbMarkLocNew = findViewById(R.id.pbMarlLocNew);

        SupportMapFragment mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.frgMarkLocNewGpsLocMap);
        mapFrag.getMapAsync(this);

        asynMarkLocationNew = new AsynMarkLocationNew();
        alphaAnimation = new AlphaAnimation(3.0F, 0.4F);
        _clsInternetConDetect = new InternetConnectionDetector(getApplicationContext());
        _LocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        _Location = _LocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
    }

    /*
     * AsyncVehicleAnalysisRootBase task null Exception
     * */
    public void setNullError() {

        barProgressDialog.dismiss();
        showAlertDialog(MarkLocationNew.this, "No Internet Connection", "You don't have Internet connection!", false);
    }

    /*
     * AsyncVehicleAnalysisRootBase Task No Record Found Error
     * */

    public void setNoDataFoundError() {

        barProgressDialog.dismiss();
        showAlertDialog(MarkLocationNew.this, "No Record Found!", "You don't have Any Record!", false);
    }

    /*
     * AsyncVehicleAnalysisRootBase Task Method of Getting data in String Array
     * */

    public void locationArrayAsync(String[] locationTypeId, String[] locationType) {
        for (int i = 0; i < locationType.length; i++) {
            list.add(locationType[i]);
        }
        createSpinnerDropDown();
    }

    private void createSpinnerDropDown() {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        _spnMarkLocNew.setAdapter(dataAdapter);
        _spnMarkLocNew.setOnItemSelectedListener(new GpsLocationTwo.MyOnItemSelectedListener());

        getCurrentLocationFromGps();
    }

    public void showLocationName(String[] stringsLocation) {

        _pbMarkLocNew.setVisibility(View.GONE);
        String _StringAddress = "";

        for (int i = 0; i < stringsLocation.length; i++) {
            _StringAddress = stringsLocation[i];
        }

        _etMarkLocLocationAddress.setText(_StringAddress);

        if (_flAccuracy < 10.00) {
            _btMarkLocNewMarkLocation.setVisibility(View.VISIBLE);
        } else {
            _btMarkLocNewMarkLocation.setVisibility(View.GONE);
        }

        if (btnCondition == 1) {
            _btMarkLocNewMarkLocation.setVisibility(View.GONE);
            Toast.makeText(getApplicationContext(), "Please try to get new Location!", Toast.LENGTH_SHORT).show();
            btnCondition = 0;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {

            _dbLatitude = location.getLatitude();
            _dbLongitude = location.getLongitude();
            _flAccuracy = location.getAccuracy();
            _lngTime = location.getTime();
            _tvMarklLocAccuracy.setText(String.valueOf(_flAccuracy) + "Accuracy");

            Double dbAlitude = location.getAltitude();

            isLocationAvailable = true;
            _pbMarkLocNew.setVisibility(View.VISIBLE);

            _dbNewLatitude = _dbLatitude;
            _dbNewLongitude = _dbLongitude;

            latLng = new LatLng(_dbLatitude, _dbLongitude);

            // Refresh Marker on google map


        } else {
            Toast.makeText(getApplicationContext(), "New Location Does not Exist!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }



    private void getCurrentLocationFromGps() {

        barProgressDialog.dismiss();

        if (_Location != null) {
            _intCheckLoc = 1;
            _dbLatitude = _Location.getLatitude();
            _dbLongitude = _Location.getLongitude();
            _flAccuracy = _Location.getAccuracy();
            _lngTime = _Location.getTime();
            _lngCurrentTime = System.currentTimeMillis() - 300000;

            if (_lngTime < _lngCurrentTime) {
                btnCondition = 1;
            }

            if (_dbLatitude != null) {
                _tvMarklLocAccuracy.setText(String.valueOf(_flAccuracy) + "Accuracy");

                isLocationAvailable = true;


                if (googleMap != null) {

                    googleMap.clear();
                    LatLng myloc = new LatLng(_dbLatitude, _dbLongitude);
                    myMarker = googleMap.addMarker(new MarkerOptions().position(myloc));

                    if (mapType == 1) {
                        googleMap.setMapType(com.google.android.gms.maps.GoogleMap.MAP_TYPE_NORMAL);
                    }
                    if (mapType == 2) {
                        googleMap.setMapType(com.google.android.gms.maps.GoogleMap.MAP_TYPE_HYBRID);
                    }
                    if (mapType == 3) {
                        googleMap.setMapType(com.google.android.gms.maps.GoogleMap.MAP_TYPE_SATELLITE);
                    }
                    if (mapType == 4) {
                        googleMap.setMapType(com.google.android.gms.maps.GoogleMap.MAP_TYPE_TERRAIN);
                    }

                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    googleMap.setMyLocationEnabled(false);
                    googleMap.setTrafficEnabled(false);
                    CameraUpdate schoolLocation = CameraUpdateFactory.newLatLngZoom(myloc, 16);
                    googleMap.animateCamera(schoolLocation);

                    dropPinEffect(myMarker);

                    AsyncMarkLocGetAddress asyncMarkLocGetAddress = new AsyncMarkLocGetAddress();
                    asyncMarkLocGetAddress.execute(MarkLocationNew.this, _dbLatitude, _dbLongitude, _pbMarkLocNew);
                }

            } else {
                Toast.makeText(getApplicationContext(), "Please try to get Current Location!", Toast.LENGTH_SHORT).show();
            }
        } else {
            getLocation();
        }


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        /*_LocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                0, 2, _LocationListener);*/
    }

    private void dropPinEffect(final Marker marker) {

        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final long duration = 3000;
        final android.view.animation.Interpolator interpolator =
                new BounceInterpolator();
        handler.post(new Runnable() {

            @Override

            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = Math.max(
                        1 - interpolator.getInterpolation((float) elapsed

                                / duration), 0);
                marker.setAnchor(0.5f, 1.0f + 14 * t);

                if (t > 0.0) {
                    handler.postDelayed(this, 15);
                } else { // done elapsing, show window
                    marker.showInfoWindow();
                }
            }

        });
    }

    public void onBackPressed() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        //_LocationManager.removeUpdates(_LocationListener);
        _pbMarkLocNew.setVisibility(View.VISIBLE);
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);

    }

    // Get Method From AsyncVehicleAnalysisRootBase Task To set Message
    public void markLocationMessage(String message, String msgid) {
        int myMsg = Integer.parseInt(msgid);
        barProgressDialog.dismiss();
        if (myMsg == 0) {
            Toast.makeText(getApplicationContext(), "Location Mark SuccessFully!", Toast.LENGTH_SHORT).show();
            finish();
            /*Intent i = new Intent(getApplicationContext(), ReportList.class);
            startActivity(i);*/
            overridePendingTransition(R.anim.left_in, R.anim.right_out);

        } else {
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
        }
    }

    public void changeMapType() {
        if (googleMap != null) {
            int type = googleMap.getMapType();
            switch (type) {
                case com.google.android.gms.maps.GoogleMap.MAP_TYPE_NORMAL:
                    mapType = 1;
                    _btMarkLocNewMapType.setText("Satellite");
                    googleMap.setMapType(com.google.android.gms.maps.GoogleMap.MAP_TYPE_SATELLITE);
                    Toast.makeText(getApplicationContext(),
                            "Map Type: SatteLight", Toast.LENGTH_LONG).show();
                    break;
                case com.google.android.gms.maps.GoogleMap.MAP_TYPE_SATELLITE:
                    mapType = 3;
                    _btMarkLocNewMapType.setText("Terrain");
                    googleMap.setMapType(com.google.android.gms.maps.GoogleMap.MAP_TYPE_TERRAIN);
                    Toast.makeText(getApplicationContext(), "Map Type: Street",
                            Toast.LENGTH_LONG).show();
                    break;
                case com.google.android.gms.maps.GoogleMap.MAP_TYPE_TERRAIN:
                    mapType = 4;
                    _btMarkLocNewMapType.setText("Hybrid");
                    googleMap.setMapType(com.google.android.gms.maps.GoogleMap.MAP_TYPE_HYBRID);
                    Toast.makeText(getApplicationContext(), "Map Type: Hybrid",
                            Toast.LENGTH_LONG).show();
                    break;
                case com.google.android.gms.maps.GoogleMap.MAP_TYPE_HYBRID:
                    mapType = 2;
                    _btMarkLocNewMapType.setText("Normal");
                    googleMap.setMapType(com.google.android.gms.maps.GoogleMap.MAP_TYPE_NORMAL);
                    Toast.makeText(getApplicationContext(), "Map Type: Normal",
                            Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }

    @Override
    public void onMapReady(com.google.android.gms.maps.GoogleMap googleMap)
    {
        if (_Location != null) {
            if (_intCheckLoc == 1) {

                try {
                    this.googleMap = googleMap;
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (mapType == 1) {
                    this.googleMap.setMapType(com.google.android.gms.maps.GoogleMap.MAP_TYPE_NORMAL);
                }
                if (mapType == 2) {
                    this.googleMap.setMapType(com.google.android.gms.maps.GoogleMap.MAP_TYPE_HYBRID);
                }
                if (mapType == 3) {
                    this.googleMap.setMapType(com.google.android.gms.maps.GoogleMap.MAP_TYPE_SATELLITE);
                }
                if (mapType == 4) {
                    this.googleMap.setMapType(com.google.android.gms.maps.GoogleMap.MAP_TYPE_TERRAIN);
                }

                this.googleMap.clear();
                MarkerOptions a = new MarkerOptions()
                        .position(latLng);
                myMarker = this.googleMap.addMarker(a);
                myMarker.setPosition(latLng);

                CameraUpdate schoolLocation = CameraUpdateFactory.newLatLngZoom(latLng, 16);
                this.googleMap.animateCamera(schoolLocation);

                AsyncMarkLocGetAddress asyncMarkLocGetAddress = new AsyncMarkLocGetAddress();
                asyncMarkLocGetAddress.execute(MarkLocationNew.this, _dbLatitude, _dbLongitude, _pbMarkLocNew);

            } else {

                _intCheckLoc = 1;
                if (this.googleMap == null) {
                    // Try to obtain the map from the SupportMapFragment.

                    this.googleMap = googleMap;

                    if (this.googleMap != null) {

                        this.googleMap.clear();
                        LatLng myloc = new LatLng(_dbLatitude, _dbLongitude);
                        myMarker = this.googleMap.addMarker(new MarkerOptions().position(myloc));

                        if (mapType == 1) {
                            this.googleMap.setMapType(com.google.android.gms.maps.GoogleMap.MAP_TYPE_NORMAL);
                        }
                        if (mapType == 2) {
                            this.googleMap.setMapType(com.google.android.gms.maps.GoogleMap.MAP_TYPE_HYBRID);
                        }
                        if (mapType == 3) {
                            this.googleMap.setMapType(com.google.android.gms.maps.GoogleMap.MAP_TYPE_SATELLITE);
                        }
                        if (mapType == 4) {
                            this.googleMap.setMapType(com.google.android.gms.maps.GoogleMap.MAP_TYPE_TERRAIN);
                        }

                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        this.googleMap.setMyLocationEnabled(false);
                        this.googleMap.setTrafficEnabled(false);

                        CameraUpdate schoolLocation = CameraUpdateFactory.newLatLngZoom(latLng, 16);
                        this.googleMap.animateCamera(schoolLocation);

                        dropPinEffect(myMarker);

                        _pbMarkLocNew.setVisibility(View.VISIBLE);

                        AsyncMarkLocGetAddress newasyncMarkLocGetAddress = new AsyncMarkLocGetAddress();

                        newasyncMarkLocGetAddress.execute(MarkLocationNew.this, _dbLatitude, _dbLongitude, _pbMarkLocNew);
                    }
                }
            }
        }

    }

    public class MyOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

            selectedItem = parent.getItemAtPosition(pos).toString();

            switch (parent.getId()) {
                case R.id.spinner:
                    if (selectedCountry != null) {
                    }
                    selectedCountry = selectedItem;
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    }
}

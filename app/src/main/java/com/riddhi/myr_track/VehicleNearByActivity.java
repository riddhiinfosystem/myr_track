package com.riddhi.myr_track;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import model.NearByVehicleList;
import services.AsyncNearByVehicleData;
import services.SearchListAsync;
import support.InternetConnectionDetector;
import support.NearByVehicleAdapter;

public class VehicleNearByActivity extends Activity {
    List<NearByVehicleList> nearByVehicle = new ArrayList<NearByVehicleList>();
    String data = "";
    ProgressDialog barProgressDialog;
    ArrayAdapter<String> adapterDailyVehPara;
    ArrayList<String> f;
    String[] vehicleName, vehicleNamecomplete = {}, vehicleUnitNo;
    private int mYear, mMonth, mDay;
    private String searchmode;
    private int selectedPosition;
    private AlphaAnimation alphaAnimation;
    private EditText button;
    private EditText etDistance;
    private ListView lstDailyVehParal;
    private Boolean _isInternetPresent = false;
    private InternetConnectionDetector _clsInternetConDetect;
    private EditText etDailyParaVehSelVehName;
    private LinearLayout dailyvehanaLinearLayoutone;
    private TextView textView3;


    private ListView lstNearByVehicle;
    private NearByVehicleAdapter nearByVehicleAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_vehicle_near_by);

        String customFont = "fonts/arlrdbd.ttf";
        Typeface typeface = Typeface.createFromAsset(getAssets(), customFont);
        textView3 = findViewById(R.id.textView3);
        textView3.setTypeface(typeface);

        searchmode = "normal";

        barProgressDialog = ProgressDialog.show(VehicleNearByActivity.this, "Please wait ...", "Downloading...", true);
        barProgressDialog.setCancelable(false);

        dailyvehanaLinearLayoutone = findViewById(R.id.dailyvehanaLinearLayoutone);
        lstDailyVehParal = findViewById(R.id.lstDailyVehParaVehName);
        etDailyParaVehSelVehName = findViewById(R.id.etDailyVehParaSelVeh);
        etDistance = findViewById(R.id.etDistance);
        etDistance.setText("10");
        lstNearByVehicle = findViewById(R.id.lstNearByVehicle);

        _clsInternetConDetect = new InternetConnectionDetector(getApplicationContext());


        checkGpsAndInternetConnection();
        alphaAnimation = new AlphaAnimation(3.0F, 0.4F);
        lstDailyVehParal.setVisibility(View.GONE);

        etDailyParaVehSelVehName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    etDailyParaVehSelVehName.setError(null);
                    lstDailyVehParal.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Exp - " + e.toString());
                }
            }
        });

        etDailyParaVehSelVehName.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {

                etDailyParaVehSelVehName.setError(null);
                lstDailyVehParal.setVisibility(View.VISIBLE);

                if (cs != null && cs.toString().length() > 0) {
                    searchmode = "search";
                    myfilter(cs);
                } else {
                    searchmode = "normal";
                    listShow();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });

        if (vehicleNamecomplete.length <= 0) {
            if (_isInternetPresent) {
                SearchListAsync async = new SearchListAsync();
                async.execute(Login.companyid, Login.userid, this, dailyvehanaLinearLayoutone, "8");
            } else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                        VehicleNearByActivity.this);
                alertDialog.setTitle("Error");
                alertDialog.setMessage("Internet Connection Problem");
                alertDialog.setIcon(R.drawable.failtwo);
                alertDialog.setCancelable(false);
                alertDialog.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog,
                                                int which) {
                                finish();
                                overridePendingTransition(R.anim.left_in, R.anim.right_out);
                            }
                        });
                alertDialog.show();
            }
        } else {
            bindglobalarray(vehicleNamecomplete);
        }

    }

    private void checkGpsAndInternetConnection() {

        _isInternetPresent = _clsInternetConDetect.isConnectingToInternet();

    }

    public void myfilter(CharSequence cs) {

        cs = cs.toString().toLowerCase();

        f = new ArrayList<String>();

        if (cs != null && cs.toString().length() > 0) {
            for (int i = 0; i < vehicleName.length; i++) {
                String product = vehicleName[i];
                if (product.toLowerCase().contains(cs))

                    f.add(product);
            }
            listShow();
        }
    }

    public void bindglobalarray(String[] vehicles) {
        barProgressDialog.dismiss();
        String vehicleno, vehicleunitno;
        vehicleNamecomplete = vehicles;
        String[] vehicleArray = new String[3];
        vehicleName = new String[vehicleNamecomplete.length];
        vehicleUnitNo = new String[vehicleNamecomplete.length];

        for (int i = 0; i <= vehicleNamecomplete.length - 1; i++) {
            vehicleArray = null;
            vehicleArray = vehicleNamecomplete[i].split(",");
            vehicleno = vehicleArray[0].toString();
            //vehicleno = vehicleNamecomplete[i].substring(0,vehicleNamecomplete[i].indexOf(","));
            vehicleunitno = vehicleArray[2].toString();
            vehicleName[i] = vehicleno;
            vehicleUnitNo[i] = vehicleunitno;
        }
        listShow();
    }

    private void listShow() {
        if (searchmode == "search") {
            adapterDailyVehPara = new ArrayAdapter<String>(this, R.layout.vehiclelist, R.id.tvVehicleListVehitem, f);
        } else {
            adapterDailyVehPara = new ArrayAdapter<String>(this, R.layout.vehiclelist, R.id.tvVehicleListVehitem, vehicleName);
        }

        lstDailyVehParal.setAdapter(adapterDailyVehPara);

        // Set Data From List View On click of List
        lstDailyVehParal.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                selectedPosition = position;

                etDailyParaVehSelVehName.setText(adapterDailyVehPara.getItem(position));

                int textLength = etDailyParaVehSelVehName.getText().length();
                etDailyParaVehSelVehName.setSelection(textLength, textLength);
                lstDailyVehParal.setVisibility(View.GONE);

                if (searchmode == "search" && f.size() > 0) {
                    Log.e("onItemClick", "onItemClick---->" + f.get(0).toString());
                    for (int k = 0; k < vehicleName.length; k++) {
                        if (f.get(0).equalsIgnoreCase("" + vehicleName[k].toString())) {
                            selectedPosition = k;

                            String tempVehicleUnitNo = "" + vehicleUnitNo[selectedPosition].toString();

                            if (_isInternetPresent) {
                                AsyncNearByVehicleData async = new AsyncNearByVehicleData();
                                async.execute(Login.companyid, tempVehicleUnitNo, "" + etDistance.getText().toString(), VehicleNearByActivity.this);
                            } else {
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                                        VehicleNearByActivity.this);
                                alertDialog.setTitle("Error");
                                alertDialog.setMessage("Internet Connection Problem");
                                alertDialog.setIcon(R.drawable.failtwo);
                                alertDialog.setCancelable(false);
                                alertDialog.setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {

                                            public void onClick(DialogInterface dialog, int which) {
                                                finish();
                                                overridePendingTransition(R.anim.left_in, R.anim.right_out);
                                            }
                                        });
                                alertDialog.show();
                            }
                        }
                    }
                }

            }
        });
    }

    public void onBackPressed() {
        if (lstDailyVehParal.getVisibility() == View.VISIBLE) {
            lstDailyVehParal.setVisibility(View.GONE);
        } else {
            finish();
            overridePendingTransition(R.anim.left_in, R.anim.right_out);
        }

    }

    public void setNullError() {

        barProgressDialog.dismiss();

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                VehicleNearByActivity.this);
        alertDialog.setTitle("Error");
        alertDialog.setMessage("Internet Connection Problem !");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog,
                                        int which) {
                        nearByVehicleAdapter = new NearByVehicleAdapter(VehicleNearByActivity.this, nearByVehicle);
                        // Assign adapter to ListView
                        lstNearByVehicle.setAdapter(nearByVehicleAdapter);
                        nearByVehicleAdapter.notifyDataSetChanged();
                    }
                });
        alertDialog.show();

    }

    public void setNoDataFoundError() {

        barProgressDialog.dismiss();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                VehicleNearByActivity.this);
        alertDialog.setTitle("Error");
        alertDialog.setMessage("No Record Found");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog,
                                        int which) {
                        nearByVehicleAdapter = new NearByVehicleAdapter(VehicleNearByActivity.this, nearByVehicle);
                        // Assign adapter to ListView
                        lstNearByVehicle.setAdapter(nearByVehicleAdapter);
                        nearByVehicleAdapter.notifyDataSetChanged();
                    }
                });
        alertDialog.show();

    }

    public void setNearByVehicleData(List<NearByVehicleList> _myNearByVehicleList) {
        this.nearByVehicle = _myNearByVehicleList;
        barProgressDialog.dismiss();
        nearByVehicleAdapter = new NearByVehicleAdapter(VehicleNearByActivity.this, nearByVehicle);
        // Assign adapter to ListView
        lstNearByVehicle.setAdapter(nearByVehicleAdapter);
        nearByVehicleAdapter.notifyDataSetChanged();

        Log.e("nearByVehicle_size", "---->" + nearByVehicle.size());
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}

package com.riddhi.myr_track;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import model.NearByVehicleList;
import services.LocationTypesAsyncNew;
import services.SaveMarkedLocationAsync;

public class trackvehicle extends FragmentActivity implements OnMapReadyCallback {

    public ArrayList<String> locationTypeList;
    public Spinner locationTypeSpinner;
    public ProgressBar progressBar;
    String lat, lon, vehicleno, unitNo, reporteddatetime;
    String location, speed;
    CountDownTimer cdt;
    Button btTrackVehicleSatelite;
    Button btTrackVehicleMapMode;
    Context context;
    String locationType[];
    int locationTypeId[];
    LinearLayout layout;
    EditText userInput;
    Marker myMarker, myMarker2;
    String newVechicleNo, newDate, newLatitude, newLongitude, newLocation, newSpeed;
    Button showDetails;
    Button hideDetails;
    Button markerButton;
    private com.google.android.gms.maps.GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private int mode = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_trackvehicle);

        context = this;

        layout = new LinearLayout(this);
        userInput = new EditText(this);
        locationTypeSpinner = new Spinner(this);
        progressBar = findViewById(R.id.progressBar);

        Bundle bundle = getIntent().getExtras();
        lat = bundle.getString("lat");
        lon = bundle.getString("lon");
        vehicleno = bundle.getString("vehicleno");
        reporteddatetime = bundle.getString("reporteddatetime");
        unitNo = bundle.getString("unitNo");
        location = bundle.getString("location");
        speed = bundle.getString("speed");

        btTrackVehicleSatelite = findViewById(R.id.btTrackVehicleSatelliteMode);
        btTrackVehicleMapMode = findViewById(R.id.btTrackVehicleMapMode);
        showDetails = findViewById(R.id.showDetails);
        hideDetails = findViewById(R.id.hideDetails);
        markerButton = findViewById(R.id.markerButton);
        showDetails.setVisibility(View.VISIBLE);

        locationTypeList = new ArrayList<String>();

        SupportMapFragment mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFrag.getMapAsync(this);

        showDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDetails.setVisibility(View.GONE);
                btTrackVehicleSatelite.setVisibility(View.VISIBLE);
                btTrackVehicleMapMode.setVisibility(View.VISIBLE);
                hideDetails.setVisibility(View.VISIBLE);
                markerButton.setVisibility(View.VISIBLE);
            }
        });

        hideDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDetails.setVisibility(View.VISIBLE);
                btTrackVehicleSatelite.setVisibility(View.GONE);
                btTrackVehicleMapMode.setVisibility(View.GONE);
                hideDetails.setVisibility(View.GONE);
                markerButton.setVisibility(View.GONE);
            }
        });

        markerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressBar.setVisibility(View.VISIBLE);
                LocationTypesAsyncNew asyncNew = new LocationTypesAsyncNew();
                asyncNew.execute(trackvehicle.this, context);

                layout.removeAllViews();

                layout = new LinearLayout(context);
                layout.setOrientation(LinearLayout.VERTICAL);

                final EditText userInput = new EditText(context);
                userInput.setText(newLocation);
                layout.addView(locationTypeSpinner);
                layout.addView(userInput);

                locationTypeList.add(0, "Select Location Type");

                final AlertDialog.Builder builder = new AlertDialog.Builder(trackvehicle.this);

                builder.setView(layout);

                builder.setTitle("Mark Location");
                builder.setMessage("Enter location");

                builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String location = userInput.getText().toString();
                        String locationType = String.valueOf(locationTypeSpinner.getSelectedItemId() - 1);

                        if (locationTypeSpinner.getSelectedItemId() != 0) {
                            progressBar.setVisibility(View.VISIBLE);
                            SaveMarkedLocationAsync async = new SaveMarkedLocationAsync();
                            async.execute(trackvehicle.this, locationType, location, newLatitude, newLongitude);
                        } else {
                            Toast.makeText(context, "Please select location type", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        });

        btTrackVehicleSatelite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mode = 0;
                mMap.setMapType(com.google.android.gms.maps.GoogleMap.MAP_TYPE_SATELLITE);
                Toast.makeText(getApplicationContext(), "Satellite Mode", Toast.LENGTH_SHORT).show();
                //btTrackVehicleSatelite.setVisibility(View.GONE);
                btTrackVehicleMapMode.setVisibility(View.VISIBLE);
            }
        });

        btTrackVehicleMapMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mode = 1;
                mMap.setMapType(com.google.android.gms.maps.GoogleMap.MAP_TYPE_NORMAL);
                Toast.makeText(getApplicationContext(), "Map Mode", Toast.LENGTH_SHORT).show();
                //btTrackVehicleMapMode.setVisibility(View.GONE);
                btTrackVehicleSatelite.setVisibility(View.VISIBLE);
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }


    @Override
    public void onMapReady(com.google.android.gms.maps.GoogleMap googleMap)
    {
        // Check if we were successful in obtaining the map.
        Log.e("onMapReady-->","called");
        mMap = googleMap;
        if (mMap != null) {

            LatLng myloc = new LatLng(Double.valueOf(lat), Double.valueOf(lon));
            //LatLng myloc = new LatLng(0,0);

            myMarker = mMap.addMarker(new MarkerOptions()
                    .position(myloc)
                    .title("")
                    .snippet("")
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myloc, 15));

            if (mode == 0) {
                mMap.setMapType(com.google.android.gms.maps.GoogleMap.MAP_TYPE_NORMAL);
            } else {
                mMap.setMapType(com.google.android.gms.maps.GoogleMap.MAP_TYPE_SATELLITE);
            }

            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }

        }


        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                //Your code where exception occurs goes here...
                mMap.setInfoWindowAdapter(new com.google.android.gms.maps.GoogleMap.InfoWindowAdapter() {
                    @Override
                    public View getInfoWindow(Marker arg0) {
                        return null;
                    }

                    @Override
                    public View getInfoContents(Marker arg0) {

                        View v = getLayoutInflater().inflate(R.layout.info_window_layout, null);

                        // Getting the position from the marker
                        LatLng latLng = arg0.getPosition();

                        // Getting reference to the TextView to set latitude
                        TextView tvTrackVehicleVehicleNo = v.findViewById(R.id.tvTrackVechielNo);

                        // Getting reference to the TextView to set longitude
                        TextView tvTrackVehicleSpeed = v.findViewById(R.id.tvTrackVehicleSpeed);

                        TextView tvTrackVehicleDate = v.findViewById(R.id.tvTrackVehicleTime);

                        final TextView tvTrackVehicleLocation = v.findViewById(R.id.tvTrackVehicleLocation);

                        //TextView markLocationView = (TextView) v.findViewById(R.id.markLocationView);
                        // Setting the latitude
                        tvTrackVehicleVehicleNo.setText("Vehicle No:" + vehicleno);
                        // Setting the longitude
                        tvTrackVehicleSpeed.setText("Speed:" + speed);

                        tvTrackVehicleDate.setText("Date & Time:" + reporteddatetime);

                        tvTrackVehicleLocation.setText("Location:" + location);
                        // Returning the view containing InfoWindow contents
                        return v;
                    }
                });
                mMap.setMyLocationEnabled(false);
                mMap.setTrafficEnabled(true);

            }
        });

    }
}
package com.riddhi.myr_track;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.Time;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import services.AsyncFuelReport;
import support.InternetConnectionDetector;


public class FuelReport extends Activity implements View.OnClickListener {

    ProgressDialog barProgressDialog;
    Spinner durationSpinner;
    String[] durationArray = {"Select Duration", "Today", "Day", "Week", "Month"};
    String[] vehicleName, vehicleNamecomplete;
    ArrayAdapter<String> adapterDailyVehPara;
    ArrayList<String> f;
    private EditText btFuelDateFrom;
    private EditText btFuelDateTo;
    private EditText fromTime, toTime;
    private Button btFuelSubmit;
    private EditText etFuelReport;
    private LinearLayout linearLayoutFuelReportOne;
    private LinearLayout linearLayoutFuelReportFour;
    private ListView lstFuelReport;
    private int mYear, mMonth, mDay;
    private String searchmode;
    private int selectedPosition;
    private AlphaAnimation alphaAnimation;
    private Date datecurrent = new Date();
    private Date dateButtonFrom = new Date();
    private Date dateButtonTo = new Date();
    private String data = "";
    private Boolean _isInternetPresent = false;
    private InternetConnectionDetector _clsInternetConDetect;
    private TextView textView3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_fuel_report);

        String customFont = "fonts/arlrdbd.ttf";
        Typeface typeface = Typeface.createFromAsset(getAssets(), customFont);
        textView3 = findViewById(R.id.textView3);
        textView3.setTypeface(typeface);

        durationSpinner = findViewById(R.id.durationSpinner);

        allInitialization();
        allOnClickListeners();

        checkGpsAndInternetConnection();

        alphaAnimation = new AlphaAnimation(3.0F, 0.4F);

        ArrayAdapter<String> durationAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, durationArray);
        durationAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        durationSpinner.setAdapter(durationAdapter);

        durationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (durationSpinner.getSelectedItemPosition() != 0) {
                    String durationString = durationArray[position];
                    setFrom_To_Date(durationString);
                } else {
                    btFuelDateTo.setText(" Date");
                    btFuelDateFrom.setText(" Date");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        barProgressDialog = ProgressDialog.show(FuelReport.this, "Please wait ...", "Downloading...", true);
        barProgressDialog.setCancelable(false);

        searchmode = "normal";

        //linearLayoutFuelReportOne.setVisibility(View.GONE);
        lstFuelReport.setVisibility(View.GONE);

        if (_isInternetPresent) {
            AsyncFuelReport asyncFuelReport = new AsyncFuelReport();
            asyncFuelReport.execute(Login.companyid, Login.userid, this, linearLayoutFuelReportOne);
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                    FuelReport.this);
            alertDialog.setTitle("Error");
            alertDialog.setMessage("Internet Connection Problem");
            alertDialog.setIcon(R.drawable.failtwo);
            alertDialog.setCancelable(false);
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog,
                                            int which) {
                            finish();
                            overridePendingTransition(R.anim.left_in, R.anim.right_out);

                        }
                    });
            alertDialog.show();
        }

        etFuelReport.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence cs, int start, int count, int after) {

                if (cs != null && cs.toString().length() > 0) {
                    searchmode = "search";
                    myfilter(cs);
                } else {
                    searchmode = "normal";
                    listShow();
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btFuelSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v == btFuelSubmit) {
                    if (etFuelReport.getText().toString().trim().equals("")) {
                        etFuelReport.setError("Field can not be blank!");

                        Toast.makeText(getApplicationContext(), "Vehicle Number Can not be Blank!", Toast.LENGTH_SHORT).show();
                    } else if (btFuelDateFrom.getText().toString().trim().equals(" Date")) {

                        btFuelDateFrom.setError("Please Select Date!");
                        Toast.makeText(getApplicationContext(), "Please Select Date!", Toast.LENGTH_SHORT).show();
                    } else if (btFuelDateTo.getText().toString().trim().equals(" Date")) {
                        btFuelDateTo.setError("Please Select Date!");
                        Toast.makeText(getApplicationContext(), "Please Select Date!", Toast.LENGTH_SHORT).show();
                    } else if (dateButtonFrom.after(datecurrent)) {
                        Toast.makeText(getApplicationContext(), "You Are selected Date From is Future Date!", Toast.LENGTH_SHORT).show();
                    } else if (dateButtonTo.after(datecurrent)) {
                        Toast.makeText(getApplicationContext(), "You Are selected Date To is Future Date!", Toast.LENGTH_SHORT).show();
                    } else if (dateButtonTo.before(dateButtonFrom)) {
                        Toast.makeText(getApplicationContext(), "Date To Should not be before! " + btFuelDateFrom.getText().toString().trim(), Toast.LENGTH_SHORT).show();
                    } else if (fromTime.getText().toString().equals(" Time")) {
                        fromTime.setError("Please select from time");
                    } else if (toTime.getText().toString().equals(" Time")) {
                        toTime.setError("Please select to time");
                    } else {
                        String[] vehicleDetails;
                        String searchString;

                        searchString = etFuelReport.getText().toString().trim();
                        for (int i = 0; i < vehicleNamecomplete.length - 1; i++) {
                            if (vehicleNamecomplete[i].contains(searchString)) {
                                selectedPosition = i;
                            }
                        }
                        vehicleDetails = vehicleNamecomplete[selectedPosition].split(",");

                        String vehicleno = vehicleDetails[0];
                        String vehicleid = vehicleDetails[1];
                        String unitno = vehicleDetails[2];

                        Intent i = new Intent(getApplicationContext(), FuelPlotGraph.class);
                        i.putExtra("vehicleno", vehicleno.toString());
                        i.putExtra("vehicleid", vehicleid.toString());
                        i.putExtra("unitno", unitno.toString());
                        i.putExtra("fromdate", btFuelDateFrom.getText().toString().trim());
                        i.putExtra("todate", btFuelDateTo.getText().toString().trim());
                        i.putExtra("fromTime", fromTime.getText().toString());
                        i.putExtra("toTime", toTime.getText().toString());

                        startActivity(i);
                        overridePendingTransition(R.anim.right_in, R.anim.left_out);
                    }
                }
            }
        });
    }

    private void checkGpsAndInternetConnection() {

        _isInternetPresent = _clsInternetConDetect.isConnectingToInternet();

    }

    private void myfilter(CharSequence cs) {

        cs = cs.toString().toLowerCase();

        f = new ArrayList<String>();
//        for (String product : vehicleName) {
//            p.add(product);
//        }
        if (vehicleName != null) {
            if (cs != null && cs.toString().length() > 0) {
                for (int i = 0; i < vehicleName.length; i++) {
                    String product = vehicleName[i];
                    if (product.toLowerCase().contains(cs))

                        f.add(product);
                }
                listShow();
            }
        }
    }

    private void allOnClickListeners() {

        btFuelDateFrom.setOnClickListener(this);
        btFuelDateTo.setOnClickListener(this);
        etFuelReport.setOnClickListener(this);
        btFuelSubmit.setOnClickListener(this);
        fromTime.setOnClickListener(this);
        toTime.setOnClickListener(this);

    }

    public void setNullError() {

        barProgressDialog.dismiss();

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                FuelReport.this);
        alertDialog.setTitle("Error");
        alertDialog.setMessage("Internet Connection Problem !");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog,
                                        int which) {
                        finish();
                        /*Intent newIntent = new Intent(getApplicationContext(),ReportList.class);
                        startActivity(newIntent);*/
                        overridePendingTransition(R.anim.left_in, R.anim.right_out);
                    }
                });
        alertDialog.show();

    }

    public void setNoDataFoundError() {

        barProgressDialog.dismiss();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                FuelReport.this);
        alertDialog.setTitle("Error");
        alertDialog.setMessage("No Record Found");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog,
                                        int which) {
                        finish();
                        /*Intent newIntent = new Intent(getApplicationContext(),ReportList.class);
                        startActivity(newIntent);*/
                        overridePendingTransition(R.anim.left_in, R.anim.right_out);
                    }
                });
        alertDialog.show();

    }

    private void allInitialization() {

        btFuelDateFrom = findViewById(R.id.btFuelReportDateFrom);
        btFuelDateTo = findViewById(R.id.btFuelReportDateTo);
        btFuelSubmit = findViewById(R.id.btFuelReportSubmit);
        etFuelReport = findViewById(R.id.etFuelReportVehicleNo);
        linearLayoutFuelReportOne = findViewById(R.id.LinearLayoutFuelReportOne);
        lstFuelReport = findViewById(R.id.lstFuelReport);
        linearLayoutFuelReportFour = findViewById(R.id.LinearLayoutFuelReportFour);
        _clsInternetConDetect = new InternetConnectionDetector(getApplicationContext());
        fromTime = findViewById(R.id.timeFrom);
        toTime = findViewById(R.id.timeTo);
    }

    public void bindglobalarray(String[] searchArray) {
        barProgressDialog.dismiss();
        String vehicleno;
        vehicleNamecomplete = searchArray;
        vehicleName = new String[vehicleNamecomplete.length];
        for (int i = 0; i <= vehicleNamecomplete.length - 1; i++) {
            vehicleno = vehicleNamecomplete[i].substring(0, vehicleNamecomplete[i].indexOf(","));
            vehicleName[i] = vehicleno;
        }
        listShow();

    }

    private void listShow() {

        if (searchmode == "search") {
            adapterDailyVehPara = new ArrayAdapter<String>(this, R.layout.vehiclelist, R.id.tvVehicleListVehitem, f);

        } else {
            adapterDailyVehPara = new ArrayAdapter<String>(this, R.layout.vehiclelist, R.id.tvVehicleListVehitem, vehicleName);
        }

        lstFuelReport.setAdapter(adapterDailyVehPara);

        // Set Data From List View On click of List
        lstFuelReport.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                selectedPosition = position;

                etFuelReport.setText(adapterDailyVehPara.getItem(position));
                int textLength = etFuelReport.getText().length();
                etFuelReport.setSelection(textLength, textLength);

                lstFuelReport.setVisibility(View.GONE);
                //linearLayoutFuelReportFour.setVisibility(View.VISIBLE);
            }
        });
    }

    public void setDuration_Time() {
        Time today = new Time(Time.getCurrentTimezone());
        today.setToNow();

        fromTime.setText("00" + ":" + "01" + " ");
        toTime.setText(today.format("%k:%M"));
    }

    public void setFrom_To_Date(String duration) {
        if (duration.equals("Today")) {
            Calendar calendar = Calendar.getInstance();
            //calendar.add(Calendar.DATE, -20);
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
            String dateString = dateFormat.format(calendar.getTime());

            btFuelDateFrom.setText(dateString);
            btFuelDateTo.setText(dateString);

            Time today = new Time(Time.getCurrentTimezone());
            today.setToNow();

            fromTime.setText("00" + ":" + "01" + " ");
            toTime.setText(today.format("%k:%M"));

        } else if (duration.equals("Day")) {
            Time today = new Time(Time.getCurrentTimezone());
            today.setToNow();
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, -1);
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
            String dateString = dateFormat.format(calendar.getTime());
            String currentDate = String.valueOf(today.monthDay + "-" + (today.month + 1) + "-" + today.year);

            btFuelDateFrom.setText(dateString);
            btFuelDateTo.setText(currentDate);

            fromTime.setText("00" + ":" + "01" + " ");
            toTime.setText(today.format("%k:%M"));
        } else if (duration.equals("Week")) {
            Time today = new Time(Time.getCurrentTimezone());
            today.setToNow();
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, -7);
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
            String dateString = dateFormat.format(calendar.getTime());
            String currentDate = String.valueOf(today.monthDay + "-" + (today.month + 1) + "-" + today.year);

            btFuelDateFrom.setText(dateString);
            btFuelDateTo.setText(currentDate);

            fromTime.setText("00" + ":" + "01" + " ");
            toTime.setText(today.format("%k:%M"));
        } else if (duration.equals("Month")) {
            Time today = new Time(Time.getCurrentTimezone());
            today.setToNow();
            int month;
            int daysInMonth;
            Calendar calendar = Calendar.getInstance();
            boolean isLeapYear = ((Calendar.YEAR % 4 == 0) && (Calendar.YEAR % 100 != 0) || (Calendar.YEAR % 400 == 0));
            month = calendar.get(Calendar.MONTH);
            month = month + 1;
            String currentDate = String.valueOf(today.monthDay + "-" + (today.month + 1) + "-" + today.year);

            if (month == 4 || month == 6 || month == 9 || month == 11) {
                daysInMonth = 30;
            } else if (month == 2) {

                daysInMonth = (isLeapYear) ? 29 : 28;
            } else
                daysInMonth = 31;

            calendar.add(Calendar.DATE, -daysInMonth);
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
            String dateString = dateFormat.format(calendar.getTime());

            btFuelDateFrom.setText(dateString);
            btFuelDateTo.setText(currentDate);

            fromTime.setText("00" + ":" + "01" + " ");
            toTime.setText(today.format("%k:%M"));
        }
    }

    public void onBackPressed() {
        if (lstFuelReport.getVisibility() == View.VISIBLE) {
            lstFuelReport.setVisibility(View.GONE);
        } else {
            finish();
            overridePendingTransition(R.anim.left_in, R.anim.right_out);
        }
    }

    @Override
    public void onClick(View v) {

        if (v == btFuelDateFrom) {
            v.startAnimation(alphaAnimation);
            btFuelDateFrom.setError(null);

            // Process to get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            // Launch Date Picker Dialog
            DatePickerDialog dpd = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            // Display Selected date in textbox
                            btFuelDateFrom.setText(dayOfMonth + "-"
                                    + (monthOfYear + 1) + "-" + year);

                            String stringButtonDateFrom = btFuelDateFrom.getText().toString();

                            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                            data = dateFormat.format(c.getTime());

                            try {
                                datecurrent = dateFormat.parse(data.toString().trim());
                                dateButtonFrom = dateFormat.parse(stringButtonDateFrom.toString().trim());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }


                        }
                    }, mYear, mMonth, mDay);
            dpd.show();
        }
        /*
         * Button Fuel Date To
         */

        if (v == btFuelDateTo) {
            v.startAnimation(alphaAnimation);
            btFuelDateTo.setError(null);

            // Process to get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            // Launch Date Picker Dialog
            DatePickerDialog dpd = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            // Display Selected date in textbox
                            btFuelDateTo.setText(dayOfMonth + "-"
                                    + (monthOfYear + 1) + "-" + year);

                            String sButtonDateTo = btFuelDateTo.getText().toString().trim();

                            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                            data = dateFormat.format(c.getTime());

                            try {
                                datecurrent = dateFormat.parse(data.toString().trim());
                                dateButtonTo = dateFormat.parse(sButtonDateTo.toString().trim());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                        }
                    }, mYear, mMonth, mDay);
            dpd.show();

        }

        if (v == etFuelReport) {
            //linearLayoutFuelReportFour.setVisibility(View.GONE);
            lstFuelReport.setVisibility(View.VISIBLE);
        }

        if (v == fromTime) {
            final Calendar calendar = Calendar.getInstance();
            int intHour = calendar.get(Calendar.HOUR_OF_DAY);
            int intMinute = calendar.get(Calendar.MINUTE);
            TimePickerDialog tpd = new TimePickerDialog(this,
                    new TimePickerDialog.OnTimeSetListener() {
                        public void onTimeSet(TimePicker view, int hour,
                                              int minute) {
                            if (hour == 00) {
                                hour += 00;
//                                stringTimeZone = "AM";
                            }
                            /*else if (hour == 12) {
                                stringTimeZone = "PM";
                            } else if (hour > 12) {
                                hour -= 12;
                                stringTimeZone = "PM";
                            } else {
                                stringTimeZone = "AM";
                            }*/
                            fromTime.setText(hour + ":" + minute + " ");
                        }
                    }, intHour, intMinute, false);
            tpd.show();
        }

        if (v == toTime) {
            final Calendar calendar = Calendar.getInstance();
            int intHour = calendar.get(Calendar.HOUR_OF_DAY);
            int intMinute = calendar.get(Calendar.MINUTE);
            TimePickerDialog tpd = new TimePickerDialog(this,
                    new TimePickerDialog.OnTimeSetListener() {
                        public void onTimeSet(TimePicker view, int hour,
                                              int minute) {
                            if (hour == 00) {
                                hour += 00;
//                                stringTimeZone = "AM";
                            }
                            /*else if (hour == 12) {
                                stringTimeZone = "PM";
                            } else if (hour > 12) {
                                hour -= 12;
                                stringTimeZone = "PM";
                            } else {
                                stringTimeZone = "AM";
                            }*/
                            toTime.setText(hour + ":" + minute + " ");
                        }
                    }, intHour, intMinute, false);
            tpd.show();
        }
    }


}

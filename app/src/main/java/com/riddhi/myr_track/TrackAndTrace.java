package com.riddhi.myr_track;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;

import services.AsyncTrackandTraceLatLon;
import support.InternetConnectionDetector;


public class TrackAndTrace extends FragmentActivity implements OnMapReadyCallback {

    ProgressDialog barProgressDialog;
    String fromTime, toTime;
    String stringVehicleNo;
    String stringDateTo;
    String stringDateFrom;
    String stringUnitNo;
    String stringVehicleId;
    String stringFirstPos = "";
    String stringLastPos = "";
    String stringFirstDateTime = "";
    String stringLastDateTime = "";
    LinearLayout linearLayout;
    LatLng position;
    LatLng firstPosition;
    PolylineOptions polyLineOptions = new PolylineOptions();
    com.google.android.gms.maps.GoogleMap googleMap;
    ArrayList<LatLng> points;
    String getClass = "TrackAndTrace.class";
    private String[] stringsLat;
    private String[] stringslon;
    private String[] stringsLoc;
    private String[] stringstrackDateTime;
    private String[] stringtrackReason;
    private Boolean _isInternetPresent = false;
    private InternetConnectionDetector _clsInternetConDetect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_track_and_trace);

        barProgressDialog = ProgressDialog.show(TrackAndTrace.this, "Please wait ...", "Downloading...", true);
        barProgressDialog.setCancelable(false);
        _clsInternetConDetect = new InternetConnectionDetector(getApplicationContext());

        try {
            Class newClass = Class.forName(getClass);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        linearLayout = findViewById(R.id.linearLayoutTrackandTrace);

        points = new ArrayList<LatLng>();

        SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        fm.getMapAsync(this);

        linearLayout.setVisibility(View.GONE);

        Intent geti = getIntent();

        stringVehicleNo = geti.getStringExtra("vehicleno");
        stringDateTo = geti.getStringExtra("todate");
        stringDateFrom = geti.getStringExtra("fromdate");
        stringUnitNo = geti.getStringExtra("unitno");
        stringVehicleId = geti.getStringExtra("vehicleid");
        fromTime = geti.getStringExtra("fromTime");
        toTime = geti.getStringExtra("toTime");

        checkGpsAndInternetConnection();

        if (_isInternetPresent) {
            callAsyncTask();
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                    TrackAndTrace.this);
            alertDialog.setTitle("Error");
            alertDialog.setMessage("Internet Connection Problem");
            alertDialog.setIcon(R.drawable.failtwo);
            alertDialog.setCancelable(false);
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog,
                                            int which) {
                            finish();
                            overridePendingTransition(R.anim.left_in, R.anim.right_out);
                        }
                    });
            alertDialog.show();
        }

        // Instantiates a new Polyline object and adds points to define a rectangle

    }

    private void checkGpsAndInternetConnection() {
        _isInternetPresent = _clsInternetConDetect.isConnectingToInternet();
    }

    public void setNullError() {

        barProgressDialog.dismiss();

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                TrackAndTrace.this);
        alertDialog.setTitle("Error");
        alertDialog.setMessage("Internet Connection Problem !");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog,
                                        int which) {
                        finish();

                        overridePendingTransition(R.anim.left_in, R.anim.right_out);
                    }
                });
        alertDialog.show();

    }

    public void setNoDataFoundError() {

        barProgressDialog.dismiss();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                TrackAndTrace.this);
        alertDialog.setTitle("Error");
        alertDialog.setMessage("No Record Found");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog,
                                        int which) {
                        finish();
                        overridePendingTransition(R.anim.left_in, R.anim.right_out);
                    }
                });
        alertDialog.show();
    }

    private void callAsyncTask() {

        AsyncTrackandTraceLatLon asyncTrackandTraceLatLon = new AsyncTrackandTraceLatLon();
        asyncTrackandTraceLatLon.execute(stringUnitNo, stringDateFrom, stringDateTo, Login.companyid, TrackAndTrace.this, linearLayout, Login.userid, fromTime, toTime);
    }


    public void showdata(String[] stringsLatitude, String[] stringsLongitude, String[] stringsLocalDateTime, String[] stringsReason, String[] stringsLocation) {

        barProgressDialog.dismiss();

        stringsLat = stringsLatitude;
        stringslon = stringsLongitude;
        stringstrackDateTime = stringsLocalDateTime;
        stringtrackReason = stringsReason;
        stringsLoc = stringsLocation;
        int latLength = 0;
        if (stringsLat != null) {
            latLength = stringsLat.length;
        }

        for (int i = 0; i < latLength; i++) {
            double lat = Double.parseDouble(stringsLat[i]);
            double lng = Double.parseDouble(stringslon[i]);

            double dlat = Double.parseDouble(stringsLat[0]);
            double dlon = Double.parseDouble(stringslon[0]);

            firstPosition = new LatLng(dlat, dlon);

            if (stringsLoc != null) {
                stringFirstPos = stringsLoc[0];
                stringLastPos = stringsLoc[i];

                stringFirstDateTime = stringstrackDateTime[0];
                stringLastDateTime = stringstrackDateTime[i];
            }

            position = new LatLng(lat, lng);
            points.add(position);
        }


        googleMap.addMarker(new MarkerOptions().position(firstPosition).title("").snippet("").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
        googleMap.addMarker(new MarkerOptions().position(position).title("").snippet("").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));


        final float red = 244.0f;
        final float green = 201.0f;
        final float yellow = 19.0f;

        /*runOnUiThread(new Runnable() {
            @Override
            public void run() {
                float redSteps = (red / result.size());
                float greenSteps = (green / result.size());
                float yellowSteps = (yellow / result.size());
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                builder.include(result.get(0));
                for (int i = 1; i < result.size(); i++) {
                    builder.include(result.get(i));
                    int redColor = (int) (red - (redSteps * i));
                    int greenColor = (int) (green - (greenSteps * i));
                    int yellowColor = (int) (yellow - (yellowSteps * i));
                    Log.e("Color", "" + redColor);
                    int color = Color.rgb(redColor, greenColor, yellowColor);

                    PolylineOptions options = new PolylineOptions().width(8).color(color).geodesic(true);
                    options.add(result.get(i - 1));
                    options.add(result.get(i));
                    Polyline line = mMap.addPolyline(options);
                    line.setEndCap(new RoundCap());
                }
            }
        });*/

        /*LatLngBounds bounds = builder.build();
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 0);
        mMap.animateCamera(cu);*/

        polyLineOptions.addAll(points);
        polyLineOptions.width(5);
        polyLineOptions.color(Color.BLUE);
        polyLineOptions.geodesic(true);
        googleMap.addPolyline(polyLineOptions);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 15));
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.setTrafficEnabled(false);

        googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker arg0) {

                View v = getLayoutInflater().inflate(R.layout.info_window_layout, null);

                // Getting the position from the marker
                LatLng latLng = arg0.getPosition();
                String tempLat = "" + latLng.latitude;
                String tempLong = "" + latLng.longitude;
                // Getting reference to the TextView to set latitude
                TextView tvTrackVehicleVehicleNo = v.findViewById(R.id.tvTrackVechielNo);

                // Getting reference to the TextView to set longitude
                TextView tvTrackVehicleSpeed = v.findViewById(R.id.tvTrackVehicleSpeed);

                TextView tvTrackVehicleDate = v.findViewById(R.id.tvTrackVehicleTime);

                final TextView tvTrackVehicleLocation = v.findViewById(R.id.tvTrackVehicleLocation);

                tvTrackVehicleSpeed.setVisibility(View.GONE);

                if (firstPosition.latitude == latLng.latitude) {
                    tvTrackVehicleVehicleNo.setText("Vehicle No:" + stringVehicleNo);
                    // Setting the longitude
                    tvTrackVehicleDate.setText("Date & Time:" + stringFirstDateTime);

                    tvTrackVehicleLocation.setText("Location:" + stringFirstPos);
                } else {
                    tvTrackVehicleVehicleNo.setText("Vehicle No:" + stringVehicleNo);
                    // Setting the longitude
                    tvTrackVehicleDate.setText("Date & Time:" + stringLastDateTime);

                    tvTrackVehicleLocation.setText("Location:" + stringLastPos);
                }

                // Returning the view containing InfoWindow content
                return v;
            }
        });


    }

    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    public void onMapReady(GoogleMap gMap) {
        googleMap = gMap;
        if (googleMap != null) {
            googleMap = gMap;
        }
    }
}
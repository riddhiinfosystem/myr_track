package com.riddhi.myr_track;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.Time;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import services.TempReportAsync;
import support.FontChangeCrawler;
import support.InternetConnectionDetector;


public class TempratureReport extends Activity implements OnClickListener {

    ArrayList<String> f;
    ProgressDialog barProgressDialog;
    FontChangeCrawler fontChanger;
    Spinner durationSpinner;
    String[] durationArray = {"Select Duration", "Today", "Day", "Week", "Month"};
    ArrayAdapter<String> adapterDailyVehPara;
    private EditText btTempReportDateFrom;
    private EditText btTempReportDateTo;
    private EditText fromTime, toTime;
    private Button btTempReportSubmit;
    private ListView listViewTempReport;
    private LinearLayout linearLayoutTempratureReport;
    private String[] vehicleName, vehicleNamecomplete;
    private int mYear, mMonth, mDay;
    private EditText etTempReportVehicleNo;
    private String searchmode;
    private LinearLayout linearLayoutTempFour;
    private int selectedPosition;
    private Boolean _isInternetPresent = false;
    private InternetConnectionDetector _clsInternetConDetect;
    private Date datecurrent = new Date();
    private Date dateButtonFrom = new Date();
    private Date dateButtonTo = new Date();
    private String data = "";
    private TextView textView3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_temprature_report);
        //getActionBar().hide();

        barProgressDialog = ProgressDialog.show(TempratureReport.this, "Please wait ...", "Downloading...", true);
        barProgressDialog.setCancelable(false);

        String customFont = "fonts/arlrdbd.ttf";
        Typeface typeface = Typeface.createFromAsset(getAssets(), customFont);
        textView3 = findViewById(R.id.textView3);
        textView3.setTypeface(typeface);

        allInitialization();

        ArrayAdapter<String> durationAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, durationArray);
        durationAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        durationSpinner.setAdapter(durationAdapter);

        durationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (durationSpinner.getSelectedItemPosition() != 0) {
                    String durationString = durationArray[position];
                    setFrom_To_Date(durationString);
                } else {
                    btTempReportDateTo.setText(" Date");
                    btTempReportDateFrom.setText(" Date");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        searchmode = "normal";
        //linearLayoutTempratureReport.setVisibility(View.GONE);

        /*
         * Calling AsyncVehicleAnalysisRootBase Task
         */

        checkGpsAndInternetConnection();

        if (_isInternetPresent) {
            TempReportAsync async = new TempReportAsync();
            async.execute(Login.companyid, Login.userid, this, linearLayoutTempratureReport);
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                    TempratureReport.this);
            alertDialog.setTitle("Error");
            alertDialog.setMessage("Internet Connection Problem");
            alertDialog.setIcon(R.drawable.failtwo);
            alertDialog.setCancelable(false);
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog,
                                            int which) {
                            finish();
                            /*Intent intent = new Intent(getApplicationContext(),ReportList.class);
                            startActivity(intent);*/
                            overridePendingTransition(R.anim.left_in, R.anim.right_out);
                        }
                    });
            alertDialog.show();
        }

        btTempReportSubmit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

//                pbTempReport.setVisibility(View.VISIBLE);

                if (etTempReportVehicleNo.getText().toString().trim().equals("")) {
                    etTempReportVehicleNo.setError("Field can not be blank!");

                    Toast.makeText(getApplicationContext(), "Vehicle Number Can not be Blank!", Toast.LENGTH_SHORT).show();
                } else if (btTempReportDateFrom.getText().toString().trim().equals("Click to Select Date")) {

                    btTempReportDateFrom.setError("Please Select Date!");
                    Toast.makeText(getApplicationContext(), "Please Select Date!", Toast.LENGTH_SHORT).show();
                } else if (btTempReportDateTo.getText().toString().trim().equals("Click to Select Date")) {
                    btTempReportDateTo.setError("Please Select Date!");
                    Toast.makeText(getApplicationContext(), "Please Select Date!", Toast.LENGTH_SHORT).show();
                } else if (dateButtonFrom.after(datecurrent)) {
                    Toast.makeText(getApplicationContext(), "You Are selected From Date is Future Date!", Toast.LENGTH_SHORT).show();
                } else if (dateButtonTo.after(datecurrent)) {
                    Toast.makeText(getApplicationContext(), "You Are selected To Date is Future Date!", Toast.LENGTH_SHORT).show();
                } else if (dateButtonTo.before(dateButtonFrom)) {
                    Toast.makeText(getApplicationContext(), "Date To Should not be before! " + btTempReportDateFrom.getText().toString().trim(), Toast.LENGTH_SHORT).show();
                }
                if (fromTime.getText().toString().equals(" Time")) {
                    fromTime.setError("Please select from time");
                } else if (toTime.getText().toString().equals(" Time")) {
                    toTime.setError("Please select to time");
                } else {
                    String[] vehicleDetails;
                    String searchString;

                    searchString = etTempReportVehicleNo.getText().toString().trim();
                    for (int i = 0; i < vehicleNamecomplete.length - 1; i++) {
                        if (vehicleNamecomplete[i].contains(searchString)) {
                            selectedPosition = i;
                        }
                    }

                    vehicleDetails = vehicleNamecomplete[selectedPosition].split(",");
                    String vehicleno = vehicleDetails[0];

                    String vehicleid = vehicleDetails[1];

//                int vehicleid = Integer.parseInt(vehicledetails[1]);

                    String unitno = vehicleDetails[2];
                    //finish();
                    Intent i = new Intent(getApplicationContext(), TempraturePlotGraph.class);
                    i.putExtra("vehicleno", vehicleno.toString());
                    i.putExtra("vehicleid", vehicleid.toString());
                    i.putExtra("unitno", unitno.toString());
                    i.putExtra("fromdate", btTempReportDateFrom.getText().toString().trim());
                    i.putExtra("todate", btTempReportDateTo.getText().toString().trim());
                    i.putExtra("fromTime", fromTime.getText().toString());
                    i.putExtra("toTime", toTime.getText().toString());

                    startActivity(i);
                    overridePendingTransition(R.anim.right_in, R.anim.left_out);
                }
            }
        });

        etTempReportVehicleNo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                etTempReportVehicleNo.setError(null);
                listViewTempReport.setVisibility(View.VISIBLE);
                //linearLayoutTempFour.setVisibility(View.GONE);

            }
        });

        /*
         * Edit Text Vehicle no Text Change Listener
         * */

        etTempReportVehicleNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence cs, int start, int count, int after) {


                etTempReportVehicleNo.setError(null);
                listViewTempReport.setVisibility(View.VISIBLE);
                //linearLayoutTempFour.setVisibility(View.GONE);

                if (cs != null && cs.toString().length() > 0) {
                    searchmode = "search";
                    myfilter(cs);
                } else {
                    searchmode = "normal";
                    listShow();
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void checkGpsAndInternetConnection() {

        _isInternetPresent = _clsInternetConDetect.isConnectingToInternet();
    }

    private void listShow() {

        if (searchmode == "search") {
            adapterDailyVehPara = new ArrayAdapter<String>(this, R.layout.vehiclelist, R.id.tvVehicleListVehitem, f);
        } else {
            adapterDailyVehPara = new ArrayAdapter<String>(this, R.layout.vehiclelist, R.id.tvVehicleListVehitem, vehicleName);
        }

        listViewTempReport.setAdapter(adapterDailyVehPara);

        // Set Data From List View On click of List
        listViewTempReport.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                selectedPosition = position;

                etTempReportVehicleNo.setText(adapterDailyVehPara.getItem(position));
                int textLength = etTempReportVehicleNo.getText().length();
                etTempReportVehicleNo.setSelection(textLength, textLength);
                listViewTempReport.setVisibility(View.GONE);
                //.setVisibility(View.VISIBLE);
            }
        });
        /*
         * All Click Listener
         *
         * */

        btTempReportDateFrom.setOnClickListener(this);
        btTempReportDateTo.setOnClickListener(this);
        fromTime.setOnClickListener(this);
        toTime.setOnClickListener(this);
    }

    private void myfilter(CharSequence cs) {

        cs = cs.toString().toLowerCase();
        f = new ArrayList<String>();

        if (cs != null && cs.toString().length() > 0) {
            for (int i = 0; i < vehicleName.length; i++) {
                String product = vehicleName[i];
                if (product.toLowerCase().contains(cs))

                    f.add(product);
            }
        }
        listShow();
    }

    private void allInitialization() {

        btTempReportDateFrom = findViewById(R.id.btTempReportDateFrom);
        btTempReportDateTo = findViewById(R.id.btTempReportDateTo);
        btTempReportSubmit = findViewById(R.id.btTempReportSubmit);
        etTempReportVehicleNo = findViewById(R.id.etTempReportVehicleName);
        listViewTempReport = findViewById(R.id.lstTempReport);
        linearLayoutTempFour = findViewById(R.id.linearLayoutTempFour);
        linearLayoutTempratureReport = findViewById(R.id.linearLayoutTempratureReport);
        _clsInternetConDetect = new InternetConnectionDetector(getApplicationContext());
        durationSpinner = findViewById(R.id.durationSpinner);
        fromTime = findViewById(R.id.timeFrom);
        toTime = findViewById(R.id.timeTo);
    }
    /*
     *  On Click Method
     * */

    public void setNullError() {

        barProgressDialog.dismiss();

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                TempratureReport.this);
        alertDialog.setTitle("Error");
        alertDialog.setMessage("Internet Connection Problem !");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog,
                                        int which) {
                        finish();
                        /*Intent newIntent = new Intent(getApplicationContext(),ReportList.class);
                        startActivity(newIntent);*/
                        overridePendingTransition(R.anim.left_in, R.anim.right_out);
                    }
                });
        alertDialog.show();
    }

    public void setNoDataFoundError() {

        barProgressDialog.dismiss();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                TempratureReport.this);
        alertDialog.setTitle("Error");
        alertDialog.setMessage("No Record Found");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog,
                                        int which) {
                        finish();
                        /*Intent newIntent = new Intent(getApplicationContext(),ReportList.class);
                        startActivity(newIntent);*/
                        overridePendingTransition(R.anim.left_in, R.anim.right_out);
                    }
                });
        alertDialog.show();

    }

    public void bindglobalarray(String[] searchArray) {

        barProgressDialog.dismiss();

        String vehicleno;
        vehicleNamecomplete = searchArray;
        vehicleName = new String[vehicleNamecomplete.length];
        for (int i = 0; i <= vehicleNamecomplete.length - 1; i++) {
            vehicleno = vehicleNamecomplete[i].substring(0, vehicleNamecomplete[i].indexOf(","));
            vehicleName[i] = vehicleno;
        }
        listShow();
    }

    public void setFrom_To_Date(String duration) {
        if (duration.equals("Today")) {
            Calendar calendar = Calendar.getInstance();
            //calendar.add(Calendar.DATE, -20);
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
            String dateString = dateFormat.format(calendar.getTime());

            btTempReportDateFrom.setText(dateString);
            btTempReportDateTo.setText(dateString);

            Time today = new Time(Time.getCurrentTimezone());
            today.setToNow();

            fromTime.setText("00" + ":" + "01" + " ");
            toTime.setText(today.format("%k:%M"));

        } else if (duration.equals("Day")) {
            Time today = new Time(Time.getCurrentTimezone());
            today.setToNow();
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, -1);
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
            String dateString = dateFormat.format(calendar.getTime());
            String currentDate = String.valueOf(today.monthDay + "-" + (today.month + 1) + "-" + today.year);

            btTempReportDateFrom.setText(dateString);
            btTempReportDateTo.setText(currentDate);

            fromTime.setText("00" + ":" + "01" + " ");
            toTime.setText(today.format("%k:%M"));
        } else if (duration.equals("Week")) {
            Time today = new Time(Time.getCurrentTimezone());
            today.setToNow();
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, -7);
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
            String dateString = dateFormat.format(calendar.getTime());
            String currentDate = String.valueOf(today.monthDay + "-" + (today.month + 1) + "-" + today.year);

            btTempReportDateFrom.setText(dateString);
            btTempReportDateTo.setText(currentDate);

            fromTime.setText("00" + ":" + "01" + " ");
            toTime.setText(today.format("%k:%M"));
        } else if (duration.equals("Month")) {
            Time today = new Time(Time.getCurrentTimezone());
            today.setToNow();
            int month;
            int daysInMonth;
            Calendar calendar = Calendar.getInstance();
            boolean isLeapYear = ((Calendar.YEAR % 4 == 0) && (Calendar.YEAR % 100 != 0) || (Calendar.YEAR % 400 == 0));
            month = calendar.get(Calendar.MONTH);
            month = month + 1;
            String currentDate = String.valueOf(today.monthDay + "-" + (today.month + 1) + "-" + today.year);

            if (month == 4 || month == 6 || month == 9 || month == 11) {
                daysInMonth = 30;
            } else if (month == 2) {

                daysInMonth = (isLeapYear) ? 29 : 28;
            } else
                daysInMonth = 31;

            calendar.add(Calendar.DATE, -daysInMonth);
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
            String dateString = dateFormat.format(calendar.getTime());

            btTempReportDateFrom.setText(dateString);
            btTempReportDateTo.setText(currentDate);

            fromTime.setText("00" + ":" + "01" + " ");
            toTime.setText(today.format("%k:%M"));
        }
    }

    public void onBackPressed() {
        if (listViewTempReport.getVisibility() == View.VISIBLE) {
            listViewTempReport.setVisibility(View.GONE);
        } else {
            finish();
            overridePendingTransition(R.anim.left_in, R.anim.right_out);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == btTempReportDateFrom) {
            // Process to get Current Date
            btTempReportDateFrom.setError(null);
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            // Launch Date Picker Dialog
            DatePickerDialog dpd = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            // Display Selected date in textbox
                            btTempReportDateFrom.setText(dayOfMonth + "-"
                                    + (monthOfYear + 1) + "-" + year);

                            String stringButtonDateFrom = btTempReportDateFrom.getText().toString();

                            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                            data = dateFormat.format(c.getTime());

                            try {
                                datecurrent = dateFormat.parse(data.toString().trim());
                                dateButtonFrom = dateFormat.parse(stringButtonDateFrom.toString().trim());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                        }
                    }, mYear, mMonth, mDay);
            dpd.show();
        }

        if (v == btTempReportDateTo) {
            // Process to get Current Date
            btTempReportDateTo.setError(null);
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            // Launch Date Picker Dialog
            DatePickerDialog dpd = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            // Display Selected date in textbox
                            btTempReportDateTo.setText(dayOfMonth + "-"
                                    + (monthOfYear + 1) + "-" + year);

                            String sButtonDateTo = btTempReportDateTo.getText().toString().trim();

                            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                            data = dateFormat.format(c.getTime());

                            try {
                                datecurrent = dateFormat.parse(data.toString().trim());
                                dateButtonTo = dateFormat.parse(sButtonDateTo.toString().trim());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                        }
                    }, mYear, mMonth, mDay);
            dpd.show();
        }

        if (v == fromTime) {
            final Calendar calendar = Calendar.getInstance();
            int intHour = calendar.get(Calendar.HOUR_OF_DAY);
            int intMinute = calendar.get(Calendar.MINUTE);
            TimePickerDialog tpd = new TimePickerDialog(this,
                    new TimePickerDialog.OnTimeSetListener() {
                        public void onTimeSet(TimePicker view, int hour,
                                              int minute) {
                            if (hour == 00) {
                                hour += 00;
//                                stringTimeZone = "AM";
                            }
                            /*else if (hour == 12) {
                                stringTimeZone = "PM";
                            } else if (hour > 12) {
                                hour -= 12;
                                stringTimeZone = "PM";
                            } else {
                                stringTimeZone = "AM";
                            }*/
                            fromTime.setText(hour + ":" + minute + " ");
                        }
                    }, intHour, intMinute, false);
            tpd.show();
        }

        if (v == toTime) {
            final Calendar calendar = Calendar.getInstance();
            int intHour = calendar.get(Calendar.HOUR_OF_DAY);
            int intMinute = calendar.get(Calendar.MINUTE);
            TimePickerDialog tpd = new TimePickerDialog(this,
                    new TimePickerDialog.OnTimeSetListener() {
                        public void onTimeSet(TimePicker view, int hour,
                                              int minute) {
                            if (hour == 00) {
                                hour += 00;
//                                stringTimeZone = "AM";
                            }
                            /*else if (hour == 12) {
                                stringTimeZone = "PM";
                            } else if (hour > 12) {
                                hour -= 12;
                                stringTimeZone = "PM";
                            } else {
                                stringTimeZone = "AM";
                            }*/
                            toTime.setText(hour + ":" + minute + " ");
                        }
                    }, intHour, intMinute, false);
            tpd.show();
        }
    }

}

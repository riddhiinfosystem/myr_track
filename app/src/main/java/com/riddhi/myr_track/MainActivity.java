package com.riddhi.myr_track;

import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import Adapter.ViewPagerAdapter;
import Fragment.AboutFragment;
import Fragment.HomeFragment;
import Fragment.ReportFragment;
import Fragment.UtilitiesFragment;
import Fragment.VehicleFragment;


public class MainActivity extends AppCompatActivity
{
    public RelativeLayout main_relative, center_relative;
    BottomNavigationView bottomNavigationView;
    //Fragments
    HomeFragment homeFragment;
    VehicleFragment vehicleFragment;
    UtilitiesFragment utilitiesFragment;
    ReportFragment reportFragment;
    AboutFragment aboutFragment;
    MenuItem prevMenuItem;
    long Delay = 4000;
    //This is our viewPager
    public static ViewPager viewPager;
    private TextView textView3;
    private long firstTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (ActivityCompat.checkSelfPermission(MainActivity.this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(MainActivity.this,
                        android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION,
                            android.Manifest.permission.ACCESS_FINE_LOCATION},
                    99);
        } else {
            proceedAfterPermission();
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        homeFragment = new HomeFragment();
        vehicleFragment = new VehicleFragment();
        reportFragment = new ReportFragment();
        utilitiesFragment = new UtilitiesFragment();
        aboutFragment = new AboutFragment();
        adapter.addFragment(homeFragment);
        adapter.addFragment(vehicleFragment);
        adapter.addFragment(reportFragment);
        adapter.addFragment(utilitiesFragment);
        adapter.addFragment(aboutFragment);
        viewPager.setAdapter(adapter);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public boolean onKeyUp(int keyCode, KeyEvent event) {

        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                long secondTime = System.currentTimeMillis();
                if (secondTime - firstTime > 2000) {
                    if (viewPager.getCurrentItem() == 0) {
                        Toast.makeText(this, "Do you want to Exit!", Toast.LENGTH_SHORT).show();
                        firstTime = secondTime;
                        return true;
                    } else {
                        viewPager.setCurrentItem(0);
                        return true;
                    }
                } else {
                    Login.server = 72;
                    finish();
                    System.exit(0);
                }
                break;
        }
        return super.onKeyUp(keyCode, event);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 99) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                //Got Permission
                proceedAfterPermission();
            }
        }
    }

    public void proceedAfterPermission() {
        center_relative = findViewById(R.id.center_relative);
        main_relative = findViewById(R.id.main_relative);

        main_relative.setVisibility(View.GONE);

        viewPager = findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(1);
        //Initializing the bottomNavigationView
        bottomNavigationView = findViewById(R.id.bottom_navigation);

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:
                                viewPager.setCurrentItem(0);
                                break;
                            case R.id.action_vehicles:
                                viewPager.setCurrentItem(1);
                                break;
                            case R.id.action_Reports:
                                viewPager.setCurrentItem(2);
                                break;
                            case R.id.action_utilities:
                                viewPager.setCurrentItem(3);
                                break;
                            case R.id.action_about:
                                viewPager.setCurrentItem(4);
                                break;
                        }
                        return false;
                    }
                });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (prevMenuItem != null) {
                    prevMenuItem.setChecked(false);
                } else {
                    bottomNavigationView.getMenu().getItem(0).setChecked(false);
                }
                Log.d("page", "onPageSelected: " + position);
                bottomNavigationView.getMenu().getItem(position).setChecked(true);
                prevMenuItem = bottomNavigationView.getMenu().getItem(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        setupViewPager(viewPager);
    }
}

package com.riddhi.myr_track;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import model.FualConsumptionReport;
import services.AsyncFuelPlotGraph;
import support.InternetConnectionDetector;


public class FuelPlotGraph extends Activity {

    String stringFuelPlotVehicleNo;
    String stringFuelPlotVehicleId;
    String stringFuelPlotUnitNo;
    String stringFuelPlotFromDate;
    String stringFuelPlotToDate;
    String fromTime, toTime;
    String stringTempPlotFromTime;
    String stringTempPlotToTime;
    ReportListAdapter adaptercustom;
    List<FualConsumptionReport> searchlistList = new ArrayList<FualConsumptionReport>();
    ProgressDialog barProgressDialog;
    ListView listView;
    TextView tvvehicleno;
    private Boolean _isInternetPresent = false;
    private InternetConnectionDetector _clsInternetConDetect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_fuel_plot_graph);

        barProgressDialog = ProgressDialog.show(FuelPlotGraph.this, "Please wait ...", "Downloading...", true);
        barProgressDialog.setCancelable(false);


        _clsInternetConDetect = new InternetConnectionDetector(getApplicationContext());


        listView = findViewById(R.id.lstSearchReportList);
        tvvehicleno = findViewById(R.id.tvvehicleno);
        Intent i = getIntent();
        stringFuelPlotVehicleNo = i.getStringExtra("vehicleno");
        stringFuelPlotVehicleId = i.getStringExtra("vehicleid");
        stringFuelPlotUnitNo = i.getStringExtra("unitno");
        stringFuelPlotFromDate = i.getStringExtra("fromdate");
        stringFuelPlotToDate = i.getStringExtra("todate");
        fromTime = i.getStringExtra("fromTime");
        toTime = i.getStringExtra("toTime");
        tvvehicleno.setText("" + stringFuelPlotVehicleNo);
        checkGpsAndInternetConnection();
        if (_isInternetPresent) {
            AsyncFuelPlotGraph asyncFuelPlotGraph = new AsyncFuelPlotGraph();
            asyncFuelPlotGraph.execute(stringFuelPlotUnitNo, stringFuelPlotFromDate, stringFuelPlotToDate, Login.companyid, FuelPlotGraph.this, Login.userid, fromTime, toTime, searchlistList);
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                    FuelPlotGraph.this);
            alertDialog.setTitle("Error");
            alertDialog.setMessage("Internet Connection Problem");
            alertDialog.setIcon(R.drawable.failtwo);
            alertDialog.setCancelable(false);
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog,
                                            int which) {
                            finish();
                            /*Intent intent = new Intent(getApplicationContext(),FuelReport.class);
                            startActivity(intent);*/
                            overridePendingTransition(R.anim.left_in, R.anim.right_out);

                        }
                    });
            alertDialog.show();
        }
    }

    private void checkGpsAndInternetConnection() {

        _isInternetPresent = _clsInternetConDetect.isConnectingToInternet();
    }

    public void setNullError() {

        barProgressDialog.dismiss();

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                FuelPlotGraph.this);
        alertDialog.setTitle("Error");
        alertDialog.setMessage("Internet Connection Problem !");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog,
                                        int which) {
                        finish();
                        /*Intent newIntent = new Intent(getApplicationContext(),FuelReport.class);
                        startActivity(newIntent);*/
                        overridePendingTransition(R.anim.left_in, R.anim.right_out);
                    }
                });
        alertDialog.show();
    }

    public void setNoDataFoundError() {

        barProgressDialog.dismiss();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                FuelPlotGraph.this);
        alertDialog.setTitle("Error");
        alertDialog.setMessage("No Record Found");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog,
                                        int which) {
                        finish();
                        /*Intent newIntent = new Intent(getApplicationContext(),FuelReport.class);
                        startActivity(newIntent);*/
                        overridePendingTransition(R.anim.left_in, R.anim.right_out);
                    }
                });
        alertDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_fuel_plot_graph, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void populatevehiclelistview(List<FualConsumptionReport> VList) {

        barProgressDialog.dismiss();
        adaptercustom = new ReportListAdapter(VList);
        listView.setAdapter(adaptercustom);
    }

    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    private class ReportListAdapter extends ArrayAdapter<FualConsumptionReport> {
        List<FualConsumptionReport> vlistinternal;

        public ReportListAdapter(List<FualConsumptionReport> vList) {
            super(FuelPlotGraph.this, R.layout.searchlistfiltershow, vList);
            vlistinternal = vList;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View vehicleinfoview = convertView;
            if (vehicleinfoview == null) {

                vehicleinfoview = getLayoutInflater().inflate(R.layout.fualconsumptionlistrow, parent, false);
            }

            FualConsumptionReport searchlist = searchlistList.get(position);


            LinearLayout linearLayout = vehicleinfoview.findViewById(R.id.linearLayoutsearchlistfiltershowone);

            TextView textViewsrno = vehicleinfoview.findViewById(R.id.textView2);
            TextView textViewactivitytype = vehicleinfoview.findViewById(R.id.textView3);
            TextView textViewTime = vehicleinfoview.findViewById(R.id.textView4);
            TextView textViewstart = vehicleinfoview.findViewById(R.id.textView5);
            TextView textViewend = vehicleinfoview.findViewById(R.id.textView6);
            TextView textViewKms = vehicleinfoview.findViewById(R.id.textView7);


            vehicleinfoview.setBackgroundResource(R.color.light_gray);
            textViewsrno.setTextColor(getResources().getColor(R.color.black_color));
            textViewactivitytype.setTextColor(getResources().getColor(R.color.black_color));
            textViewTime.setTextColor(getResources().getColor(R.color.black_color));
            textViewstart.setTextColor(getResources().getColor(R.color.black_color));
            textViewend.setTextColor(getResources().getColor(R.color.black_color));
            textViewKms.setTextColor(getResources().getColor(R.color.black_color));

            textViewsrno.setText(searchlist._Vehicle_No);
            textViewactivitytype.setText(searchlist._speed);
            textViewTime.setText(searchlist._Date);
            textViewstart.setText(searchlist._Time);
            textViewend.setText(searchlist._Fuel_Level);
            textViewKms.setText(searchlist._Location);

            return vehicleinfoview;
        }
    }
}

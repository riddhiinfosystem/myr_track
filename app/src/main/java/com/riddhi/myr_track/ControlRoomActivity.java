package com.riddhi.myr_track;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import Fragment.HomeFragment;
import services.AsyncControlRoomDetail;
import services.AsyncDashboardDetail;

public class ControlRoomActivity extends Activity {

    RelativeLayout center_relative;
    private TextView textView3,continue_stoppage_24_value, continue_stoppage_48_value, continue_stoppage_72_value,continue_driving_4_value, continue_driving_8_value, over_speed_value,night_driving_value, power_disconnect_value, inactive_vehicle_value;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control_room);

        String customFont = "fonts/arlrdbd.ttf";
        Typeface typeface = Typeface.createFromAsset(this.getAssets(), customFont);
        textView3 = (TextView)findViewById(R.id.textView3);
        textView3.setTypeface(typeface);

        center_relative = (RelativeLayout) findViewById(R.id.center_relative);
        continue_stoppage_24_value = (TextView)findViewById(R.id.continue_stoppage_24_value);
        continue_stoppage_48_value = (TextView)findViewById(R.id.continue_stoppage_48_value);
        continue_stoppage_72_value = (TextView)findViewById(R.id.continue_stoppage_72_value);
        continue_driving_4_value = (TextView)findViewById(R.id.continue_driving_4_value);
        continue_driving_8_value = (TextView)findViewById(R.id.continue_driving_8_value);
        over_speed_value = (TextView)findViewById(R.id.over_speed_value);
        night_driving_value = (TextView)findViewById(R.id.night_driving_value);
        power_disconnect_value = (TextView)findViewById(R.id.power_disconnect_value);
        inactive_vehicle_value = (TextView)findViewById(R.id.inactive_vehicle_value);

        populateDashboardData();
    }

    private void populateDashboardData() {
        AsyncControlRoomDetail lvAsysnc = new AsyncControlRoomDetail();
        lvAsysnc.execute(Login.companyid, Login.userid, center_relative, ControlRoomActivity.this);
    }

    public void setDashboardDataOnView(String continue_stoppage_24, String continue_stoppage_48, String continue_stoppage_72, String continue_driving_4, String continue_driving_8, String over_speed, String night_driving, String power_disconnect, String inactive_vehicle) {
        center_relative.setVisibility(View.GONE);

        continue_stoppage_24_value.setText("" + continue_stoppage_24);
        continue_stoppage_48_value.setText("" + continue_stoppage_48);
        continue_stoppage_72_value.setText("" + continue_stoppage_72);
        continue_driving_4_value.setText("" + continue_driving_4);
        continue_driving_8_value.setText("" + continue_driving_8);
        over_speed_value.setText("" + over_speed);
        night_driving_value.setText("" + night_driving);
        power_disconnect_value.setText("" + power_disconnect);
        inactive_vehicle_value.setText("" + inactive_vehicle);

    }

    public void GoToPageX(View v){
        int i = Integer.parseInt(""+v.getTag());
        Log.e("continue_stoppage_24","==>"+i);
        String typeStr = "";
        int tempCount = 0 ;
        if(i == 1)
        {
            tempCount = Integer.parseInt(""+continue_stoppage_24_value.getText().toString());
            typeStr = "Continue Stoppage(24 Hrs)";
        }
        else if(i==2)
        {
            tempCount = Integer.parseInt(""+continue_stoppage_48_value.getText().toString());
            typeStr = "Continue Stoppage(48 Hrs)";
        }
        else if(i==3)
        {
            tempCount = Integer.parseInt(""+continue_stoppage_72_value.getText().toString());
            typeStr = "Continue Stoppage(72 Hrs)";
        }
        else if(i==4)
        {
            tempCount = Integer.parseInt(""+continue_driving_4_value.getText().toString());
            typeStr = "Continue Driving(4 Hrs)";
        }
        else if(i==5)
        {
            tempCount = Integer.parseInt(""+continue_driving_8_value.getText().toString());
            typeStr = "Continue Driving(8 Hrs)";
        }
        else if(i==6)
        {
            tempCount = Integer.parseInt(""+over_speed_value.getText().toString());
            typeStr = "Over Speed";
        }
        else if(i==7)
        {
            tempCount = Integer.parseInt(""+night_driving_value.getText().toString());
            typeStr = "Night Driving(11PM to 5AM)";
        }
        else if(i==8)
        {
            tempCount = Integer.parseInt(""+power_disconnect_value.getText().toString());
            typeStr = "Power Disconnect";
        }
        else if(i==9)
        {
            tempCount = Integer.parseInt(""+inactive_vehicle_value.getText().toString());
            typeStr = "Inactive Vehicle";
        }

        if(tempCount > 0) {
            Intent intent = new Intent(this, ControlRoomDetailActivity.class);
            intent.putExtra("Type", "" + i);
            intent.putExtra("Text", "" + typeStr);
            startActivity(intent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        }
        else
        {
            Toast.makeText(ControlRoomActivity.this,"Data Not Found",Toast.LENGTH_LONG).show();
        }
    }
}

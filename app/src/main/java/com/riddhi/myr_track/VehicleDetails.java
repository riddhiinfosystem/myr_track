package com.riddhi.myr_track;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import services.AsyncVehicleDetails;
import support.InternetConnectionDetector;

/**
 * Created by Vinayak on 4/27/2018.
 */

public class VehicleDetails extends Activity {
    final Handler handler = new Handler();
    ProgressDialog barProgressDialog;
    ArrayAdapter<String> adapterDailyVehPara;
    int workingVehicleColor = 0;
    int blackColor = 0;
    int textBackColor = 0;
    int notWorkingVehicleColor = 0;
    int transparentColor = 0;
    ArrayList<String> f;
    String[] vehicleName, vehicleNamecomplete = {};
    List<model.VehicleDetails> _myvehiclesDetails = new ArrayList<model.VehicleDetails>();
    Typeface fontRegular;
    InputMethodManager imm;
    String tempVehicleDetail = "";
    private ListView lstDailyVehParal;
    private Boolean _isInternetPresent = false;
    private InternetConnectionDetector _clsInternetConDetect;
    private EditText etDailyParaVehSelVehName;
    private String searchmode;
    private TextView textView3;
    private LinearLayout linearLayoutDailyVehAnaPara;
    private LinearLayout dailyvehanaLinearLayoutone;
    private TextView tvVehicleNoVal;
    private TextView tvUnitNoVal;
    private TextView tvLastReportDateVal;
    private TextView tvInstallDateVal;
    private TextView tvPowerStatusVal;
    private TextView tvRemarkVal;
    private TextView tvCurLocationVal;
    private TextView tvConBranchVal;
    private TextView tvVehGrpVal;
    private TextView tvVehTypeVal;
    private ScrollView verticalScrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_vehicle_details);

        imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);


        String customFont = "fonts/arlrdbd.ttf";
        Typeface typeface = Typeface.createFromAsset(getAssets(), customFont);
        textView3 = findViewById(R.id.textView3);
        textView3.setTypeface(typeface);

        fontRegular = Typeface.createFromAsset(this.getAssets(), "fonts/Roboto-Light.ttf");

        workingVehicleColor = this.getResources().getColor(R.color.tabbgcolor);
        blackColor = this.getResources().getColor(R.color.black_color);
        textBackColor = this.getResources().getColor(R.color.notreportingback);
        notWorkingVehicleColor = this.getResources().getColor(R.color.color20);
        transparentColor = this.getResources().getColor(R.color.layout_backgrount);

        searchmode = "normal";

        barProgressDialog = ProgressDialog.show(VehicleDetails.this, "Please wait ...", "Downloading...", true);
        barProgressDialog.setCancelable(false);

        dailyvehanaLinearLayoutone = findViewById(R.id.vehicleDetailLinearLayoutForList);

        lstDailyVehParal = findViewById(R.id.lstvehicleDetailVehName);
        etDailyParaVehSelVehName = findViewById(R.id.etDailyVehParaSelVeh);

        tvVehicleNoVal = findViewById(R.id.tvVehicleNoVal);
        tvUnitNoVal = findViewById(R.id.tvUnitNoVal);
        tvLastReportDateVal = findViewById(R.id.tvLastReportDateVal);
        tvInstallDateVal = findViewById(R.id.tvInstallDateVal);
        tvPowerStatusVal = findViewById(R.id.tvPowerStatusVal);
        tvRemarkVal = findViewById(R.id.tvRemarkVal);
        tvCurLocationVal = findViewById(R.id.tvCurLocationVal);
        tvConBranchVal = findViewById(R.id.tvConBranchVal);
        tvVehGrpVal = findViewById(R.id.tvVehGrpVal);
        tvVehTypeVal = findViewById(R.id.tvVehTypeVal);

        verticalScrollView = findViewById(R.id.verticalScrollView);
        verticalScrollView.setVisibility(View.GONE);

        _clsInternetConDetect = new InternetConnectionDetector(getApplicationContext());
        checkGpsAndInternetConnection();

        lstDailyVehParal.setVisibility(View.GONE);

        if (vehicleNamecomplete.length <= 0) {
            if (_isInternetPresent) {
                AsyncVehicleDetails async = new AsyncVehicleDetails();
                async.execute(Login.companyid, Login.userid, this, dailyvehanaLinearLayoutone);
            } else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                        VehicleDetails.this);
                alertDialog.setTitle("Error");
                alertDialog.setMessage("Internet Connection Problem");
                alertDialog.setIcon(R.drawable.failtwo);
                alertDialog.setCancelable(false);
                alertDialog.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog,
                                                int which) {
                                finish();
                                overridePendingTransition(R.anim.left_in, R.anim.right_out);
                            }
                        });
                alertDialog.show();
            }
        } else {
            bindglobalarray(vehicleNamecomplete, _myvehiclesDetails);
        }

        etDailyParaVehSelVehName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    etDailyParaVehSelVehName.setError(null);
                    lstDailyVehParal.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Exp - " + e.toString());
                }
            }
        });


        etDailyParaVehSelVehName.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {

                etDailyParaVehSelVehName.setError(null);
                lstDailyVehParal.setVisibility(View.VISIBLE);

                if (cs != null && cs.toString().length() > 0) {
                    searchmode = "search";
                    myfilter(cs);
                } else {
                    searchmode = "normal";
                    listShow();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });

    }

    public void myfilter(CharSequence cs) {

        cs = cs.toString().toLowerCase();

        f = new ArrayList<String>();

        if (cs != null && cs.toString().length() > 0) {
            for (int i = 0; i < vehicleName.length; i++) {
                String product = vehicleName[i];
                if (product.toLowerCase().contains(cs))

                    f.add(product);
            }
            listShow();
        }
    }

    @Override
    public void onBackPressed() {

        this.finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void bindglobalarray(String[] vehicles, List<model.VehicleDetails> _myvehicles) {
        barProgressDialog.dismiss();
        String vehicleno;
        vehicleNamecomplete = vehicles;
        vehicleName = new String[vehicleNamecomplete.length];

        _myvehiclesDetails = _myvehicles;
        for (int i = 0; i <= vehicleNamecomplete.length - 1; i++) {
            vehicleno = vehicleNamecomplete[i].toString();
            vehicleName[i] = vehicleno;
        }
        listShow();
    }

    private void listShow() {
        if (searchmode == "search") {
            adapterDailyVehPara = new ArrayAdapter<String>(this, R.layout.vehiclelist, R.id.tvVehicleListVehitem, f);
        } else {
            adapterDailyVehPara = new ArrayAdapter<String>(this, R.layout.vehiclelist, R.id.tvVehicleListVehitem, vehicleName);
        }

        lstDailyVehParal.setAdapter(adapterDailyVehPara);

        // Set Data From List View On click of List
        lstDailyVehParal.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                etDailyParaVehSelVehName.setText(adapterDailyVehPara.getItem(position));
                int textLength = etDailyParaVehSelVehName.getText().length();
                etDailyParaVehSelVehName.setSelection(textLength, textLength);
                lstDailyVehParal.setVisibility(View.GONE);
                String temp = etDailyParaVehSelVehName.getText().toString();
                verticalScrollView.setVisibility(View.VISIBLE);

                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

                for (int i = 0; i < _myvehiclesDetails.size(); i++) {
                    model.VehicleDetails vehicleDetails = _myvehiclesDetails.get(i);
                    if (vehicleDetails.getVehicle_No().equalsIgnoreCase("" + temp)) {
                        tvVehicleNoVal.setText("" + vehicleDetails.getVehicle_No());
                        tvVehicleNoVal.setTypeface(fontRegular, Typeface.BOLD);

                        if (vehicleDetails.getVehicleWorking().equalsIgnoreCase("yes")) {
                            tvVehicleNoVal.setTextColor(workingVehicleColor);
                            tvLastReportDateVal.setTextColor(blackColor);
                            tvLastReportDateVal.setBackgroundColor(textBackColor);

                            tempVehicleDetail = "yes";


                        } else {

                            tvVehicleNoVal.setTextColor(notWorkingVehicleColor);
                            tvLastReportDateVal.setTextColor(notWorkingVehicleColor);
                            tvLastReportDateVal.setBackgroundColor(transparentColor);
                            tempVehicleDetail = "no";
                            blink();
                        }

                        tvVehicleNoVal.setText("" + vehicleDetails.getVehicle_No());
                        tvUnitNoVal.setText("" + vehicleDetails.getUnitNo());
                        tvLastReportDateVal.setText("" + vehicleDetails.getUtcToLocalDateTime());
                        tvInstallDateVal.setText("" + vehicleDetails.getInstallation_Date());
                        tvPowerStatusVal.setText("" + vehicleDetails.getPower_Satatus());
                        tvRemarkVal.setText("" + vehicleDetails.getRemarks());
                        tvCurLocationVal.setText("" + vehicleDetails.getLastLocation());
                        tvConBranchVal.setText("" + vehicleDetails.getVehicle_Controlling_Branch());
                        tvVehGrpVal.setText("" + vehicleDetails.getVehicle_Group());
                        tvVehTypeVal.setText("" + vehicleDetails.getVehicle_Type());
                    }
                }


            }
        });
    }

    private void checkGpsAndInternetConnection() {

        _isInternetPresent = _clsInternetConDetect.isConnectingToInternet();

    }

    public void setNullError() {

        barProgressDialog.dismiss();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                VehicleDetails.this);
        alertDialog.setTitle("Error");
        alertDialog.setMessage("Internet Connection Problem !");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog,
                                        int which) {
                        finish();
                        /*Intent newIntent = new Intent(getApplicationContext(),ReportList.class);
                        startActivity(newIntent);*/
                        overridePendingTransition(R.anim.left_in, R.anim.right_out);
                    }
                });
        alertDialog.show();

    }

    public void setNoDataFoundError() {

        barProgressDialog.dismiss();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                VehicleDetails.this);
        alertDialog.setTitle("Error");
        alertDialog.setMessage("No Record Found");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog,
                                        int which) {
                        finish();
                        /*Intent newIntent = new Intent(getApplicationContext(),ReportList.class);
                        startActivity(newIntent);*/
                        overridePendingTransition(R.anim.left_in, R.anim.right_out);
                    }
                });
        alertDialog.show();

    }

    private void blink() {

        /*new Thread(new Runnable() {
            @Override
            public void run() {
                int timeToBlink = 1000;    //in milissegunds
                try{Thread.sleep(timeToBlink);}catch (Exception e) {}
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if(tvLastReportDateVal.getVisibility() == View.VISIBLE){
                            tvLastReportDateVal.setVisibility(View.INVISIBLE);
                        }else{
                            tvLastReportDateVal.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }
        }).start();*/


        final Runnable task = new Runnable() {
            @Override
            public void run() {
                Log.e("MainActivity", "Doing task");


                if (tvLastReportDateVal.getVisibility() == View.VISIBLE) {
                    tvLastReportDateVal.setVisibility(View.INVISIBLE);
                } else {
                    tvLastReportDateVal.setVisibility(View.VISIBLE);
                }


                if (tempVehicleDetail.equalsIgnoreCase("yes")) {
                    tvLastReportDateVal.setVisibility(View.VISIBLE);
                    handler.removeMessages(0);
                }


                handler.postDelayed(this, 1000);

                if (tempVehicleDetail.equalsIgnoreCase("yes")) {
                    tvLastReportDateVal.setVisibility(View.VISIBLE);
                    handler.removeMessages(0);
                }

            }
        };
        handler.post(task);

    }


}

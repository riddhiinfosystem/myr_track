package com.riddhi.myr_track;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import pl.droidsonroids.gif.GifImageView;
import services.AsyncLogin;

public class Login extends Activity {

    public static int tempVehicleFrag = 1;
    public static int tempHomeFrag = 1;
    public static Boolean imgFlag = false;
    public static int server = 72, companyid = 0, userid = 0;
    public static String companyname;
    static int checkremember;
    static String url = "";
    EditText txtpassword, txtusername;
    Button btnlogin;
    CheckBox chkopt;
    ProgressBar progressBar;
    RelativeLayout linearLayoutLoginOne;
    ArrayList<String> lstUserId;
    ArrayList<String> lstCompanyID;
    Cursor cursor;
    String username;
    String pass;
    int count;
    EditText _nameText;
    // @InjectView(R.id.input_email) EditText _emailText;
    EditText _passwordText;
    ImageView imageView8;
    long Delay = 4000;
    RelativeLayout login_relative;
    GifImageView gif_img1;
    private Context _context;
    private long firstTime = 0;
    private SQLiteDatabase sqLiteDatabase = null;
    private String DB_NAME = "DEMO_DB";
    private String TABLE_NAME = "Login";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);
        //getActionBar().hide();

        login_relative = findViewById(R.id.login_relative);
        gif_img1 = findViewById(R.id.gif_img1);

        progressBar = findViewById(R.id.pbLogin);
        linearLayoutLoginOne = findViewById(R.id.linearLayoutLoginOne);

        sqLiteDatabase = openOrCreateDatabase(DB_NAME, MODE_PRIVATE, null);

        sqLiteDatabase = openOrCreateDatabase(DB_NAME, MODE_PRIVATE, null);
        String CREATE_LOGIN = "CREATE TABLE IF NOT EXISTS Login(loginid integer primary key autoincrement,username string,password string)";
        sqLiteDatabase.execSQL(CREATE_LOGIN);

        String customFont = "fonts/HARLOWSI.TTF";
        Typeface typeface = Typeface.createFromAsset(getAssets(), customFont);

        txtpassword = findViewById(R.id.txtpassword);
        txtusername = findViewById(R.id.txtusername);
        btnlogin = findViewById(R.id.btnlogin);
        imageView8 = findViewById(R.id.imageView8);


        CheckBox chkopt = findViewById(R.id.chkserver);

        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String getUserName = txtusername.getText().toString();
                String getPass = txtpassword.getText().toString();

                fluctuateActivity(getUserName, getPass);
            }
        });

        chkopt.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
                // TODO Auto-generated method stub
                if (arg1) {
                    checkremember = 1;
                } else {
                    checkremember = 0;
                }
            }
        });

        Timer RunSplash = new Timer();
        TimerTask ShowSplash = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ViewGone();
                    }
                });

            }

        };
        RunSplash.schedule(ShowSplash, Delay);
    }

    public void ViewGone() {
        //gif_img1.setVisibility(View.GONE);

        cursor = sqLiteDatabase.rawQuery("select * from Login;", null);
        count = cursor.getCount();

        if (count > 0) {
            try {
                cursor.moveToFirst();
                username = cursor.getString(1);
                pass = cursor.getString(2);
                fluctuateActivity(username, pass);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            gif_img1.setVisibility(View.GONE);
            login_relative.setVisibility(View.VISIBLE);
            Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),
                    R.anim.right_in);
            login_relative.startAnimation(animation);
        }
    }

    public void responseNull() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                Login.this);
        alertDialog.setTitle("Error");
        alertDialog.setMessage("Internet Connection Problem");
        alertDialog.setIcon(R.drawable.failtwo);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog,
                                        int which) {

                    }
                });
        alertDialog.show();
        gif_img1.setVisibility(View.GONE);
        login_relative.setVisibility(View.VISIBLE);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.right_in);
        login_relative.startAnimation(animation);
    }

    public void reponseZero() {
        AlertDialog.Builder dlgAlert = new AlertDialog.Builder(Login.this);
        dlgAlert.setMessage("Wrong Username/Password");
        dlgAlert.setTitle("Error!!!");
        dlgAlert.setPositiveButton("OK", null);
        dlgAlert.setCancelable(true);
        dlgAlert.setIcon(android.R.drawable.ic_delete);
        dlgAlert.create().show();

        sqLiteDatabase.execSQL("delete from " + TABLE_NAME);

        gif_img1.setVisibility(View.GONE);
        login_relative.setVisibility(View.VISIBLE);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.right_in);
        login_relative.startAnimation(animation);
    }

    public void otherWiseCondition(Integer _companyId, String _companyname, String _url, Integer _userid, String asyncUserName, String asyncUserPass) {

        companyid = _companyId;
        companyname = _companyname;
        url = _url;
        userid = _userid;

        if (checkremember == 1) {
            sqLiteDatabase.execSQL("insert into Login(username,password)VALUES"
                    + "('"
                    + asyncUserName
                    + "',"
                    + "'"
                    + asyncUserPass + "')");

        }

        finish();
        Intent i = new Intent(getBaseContext(), MainActivity.class);
        startActivity(i);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    public void fluctuateActivity(String fluusername, String flupass) {
        progressBar.setVisibility(View.VISIBLE);
        linearLayoutLoginOne.setVisibility(View.GONE);

        String myUserName = fluusername;
        String myPass = flupass;

        AsyncLogin asyncLogin = new AsyncLogin();
        asyncLogin.execute(myUserName, myPass, progressBar, linearLayoutLoginOne, this);
    }

    public boolean isConnectingToInternet() {

        ConnectivityManager connectivity = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }


    public boolean onKeyUp(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                long secondTime = System.currentTimeMillis();
                if (secondTime - firstTime > 2000) {                                         //2?
                    Toast.makeText(this, "Do you want to Exit!", Toast.LENGTH_SHORT).show();


                    firstTime = secondTime;//firstTime
                    return true;
                } else {                                                    //2?
                    System.exit(0);
                }
                break;
        }

        return super.onKeyUp(keyCode, event);
    }
}

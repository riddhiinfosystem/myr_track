package com.riddhi.myr_track;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import model.Searchlist;
import services.AsyncVehicleAnalysisRootBase;
import support.InternetConnectionDetector;

public class DailyAnalysisRootBaseReportList extends Activity {

    ListView listView;
    TextView textViewVehcileNo;
    ProgressDialog barProgressDialog;
    List<Searchlist> searchlistList = new ArrayList<Searchlist>();

    String vehicleno;
    String vehicleid;
    String unitno;
    String datefrom;
    String dateto;
    String fromTime, toTime;
    ReportListAdapter adaptercustom;
    private Boolean _isInternetPresent = false;
    private InternetConnectionDetector _clsInternetConDetect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_analysis_root_base_report_list);
        barProgressDialog = ProgressDialog.show(DailyAnalysisRootBaseReportList.this, "Please wait...", "Loading...", true);
        barProgressDialog.setCancelable(false);
        textViewVehcileNo = findViewById(R.id.textView3);
        listView = findViewById(R.id.lstSearchReportList);

        Intent intent = getIntent();

        vehicleno = intent.getStringExtra("vehicleno");
        vehicleid = intent.getStringExtra("vehicleid");
        unitno = intent.getStringExtra("unitno");
        datefrom = intent.getStringExtra("fromdate");
        dateto = intent.getStringExtra("todate");
        fromTime = intent.getStringExtra("fromTime");
        toTime = intent.getStringExtra("toTime");

        _clsInternetConDetect = new InternetConnectionDetector(getApplicationContext());

        checkGpsAndInternetConnection();
        if (_isInternetPresent) {
            populatevehiclelist();
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                    DailyAnalysisRootBaseReportList.this);
            alertDialog.setTitle("Error");
            alertDialog.setMessage("Internet Connection Problem");
            alertDialog.setIcon(R.drawable.failtwo);
            alertDialog.setCancelable(false);
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog,
                                            int which) {
                            finish();
                            overridePendingTransition(R.anim.left_in, R.anim.right_out);

                        }
                    });
            alertDialog.show();
        }
        textViewVehcileNo.setText(vehicleno);
    }

    private void checkGpsAndInternetConnection() {

        _isInternetPresent = _clsInternetConDetect.isConnectingToInternet();
    }

    private void populatevehiclelist() {
        AsyncVehicleAnalysisRootBase lvAsysnc = new AsyncVehicleAnalysisRootBase();
        lvAsysnc.execute(vehicleid, unitno, datefrom, dateto, searchlistList, DailyAnalysisRootBaseReportList.this);
    }

    public void populatevehiclelistview(List<Searchlist> VList) {
        barProgressDialog.dismiss();
        adaptercustom = new ReportListAdapter(VList);
        listView.setAdapter(adaptercustom);
        adaptercustom.notifyDataSetChanged();
    }

    public void setNullError() {

        barProgressDialog.dismiss();

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                DailyAnalysisRootBaseReportList.this);
        alertDialog.setTitle("Error");
        alertDialog.setMessage("Internet Connection Problem !");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog,
                                        int which) {
                        finish();
                        overridePendingTransition(R.anim.left_in, R.anim.right_out);
                    }
                });
        alertDialog.show();
    }

    public void setNoDataFoundError() {

        barProgressDialog.dismiss();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                DailyAnalysisRootBaseReportList.this);
        alertDialog.setTitle("Error");
        alertDialog.setMessage("No Record Found !");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog,
                                        int which) {
                        finish();
                        overridePendingTransition(R.anim.left_in, R.anim.right_out);
                    }
                });
        alertDialog.show();
    }

    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    private class ReportListAdapter extends ArrayAdapter<Searchlist> {
        List<Searchlist> vlistinternal;

        public ReportListAdapter(List<Searchlist> vList) {
            super(DailyAnalysisRootBaseReportList.this, R.layout.daily_analysis_root_base_report_row, vList);
            vlistinternal = vList;
        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        public View getView(int position, View convertView, ViewGroup parent) {
            View vehicleinfoview = convertView;
            if (vehicleinfoview == null) {
                vehicleinfoview = getLayoutInflater().inflate(R.layout.daily_analysis_root_base_report_row, parent, false);
            }
            Searchlist searchlist = searchlistList.get(position);
            ImageView imageView = vehicleinfoview.findViewById(R.id.imageView1);
            TextView tv_activity_type = vehicleinfoview.findViewById(R.id.tv_activity_type);
            TextView tv_from_date = vehicleinfoview.findViewById(R.id.tv_from_date);
            TextView tv_to_date = vehicleinfoview.findViewById(R.id.tv_to_date);
            TextView tv_start_location = vehicleinfoview.findViewById(R.id.tv_start_location);
            TextView tv_stop_location = vehicleinfoview.findViewById(R.id.tv_stop_location);
            TextView tv_time = vehicleinfoview.findViewById(R.id.tv_time);
            TextView tv_kms = vehicleinfoview.findViewById(R.id.tv_kms);

            if (searchlist.getActivitytype().equalsIgnoreCase("Stoppage")) {
                imageView.setBackground(getDrawable(R.drawable.stop_truck_icon));
            } else {
                imageView.setBackground(getDrawable(R.drawable.running_truck_icon));
            }
            tv_activity_type.setText("" + searchlist.getActivitytype());
            tv_from_date.setText("" + searchlist.getFromdate());
            tv_to_date.setText("" + searchlist.getTodate());
            tv_start_location.setText("" + searchlist.getStartlocation());
            tv_stop_location.setText("" + searchlist.getEndlocation());
            tv_time.setText("" + searchlist.getdate());
            tv_kms.setText("" + searchlist.getTotalrunningkms() + " KM");

            return vehicleinfoview;
        }
    }
}
package com.riddhi.myr_track;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.Time;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import services.SearchListAsync;
import support.InternetConnectionDetector;

public class DailyAnalysisRootBase extends Activity implements View.OnClickListener {

    Date datecurrent = new Date();
    Date dateButton = new Date();
    Date dateButtonTo = new Date();
    String data = "";
    ProgressDialog barProgressDialog;
    ArrayAdapter<String> adapterDailyVehPara;
    ArrayList<String> f;
    String[] vehicleName, vehicleNamecomplete = {};
    String grapString;
    Spinner durationSpinner;
    String[] durationArray = {"Select Duration", "Today", "Day", "Week", "Month"};
    private int mYear, mMonth, mDay;
    private String searchmode;
    private int selectedPosition;
    private AlphaAnimation alphaAnimation;
    private EditText button;
    private EditText buttonone;
    private Button btSearchReport;
    private ListView lstDailyVehParal;
    private Boolean _isInternetPresent = false;
    private InternetConnectionDetector _clsInternetConDetect;
    private EditText etDailyParaVehSelVehName;
    private LinearLayout linearLayoutDailyVehAnaPara;
    private LinearLayout dailyvehanaLinearLayoutone;
    private TextView textView3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_daily_analysis_root_base);

        String customFont = "fonts/arlrdbd.ttf";
        Typeface typeface = Typeface.createFromAsset(getAssets(), customFont);
        textView3 = findViewById(R.id.textView3);
        textView3.setTypeface(typeface);

        searchmode = "normal";

        barProgressDialog = ProgressDialog.show(DailyAnalysisRootBase.this, "Please wait ...", "Downloading...", true);
        barProgressDialog.setCancelable(false);

        linearLayoutDailyVehAnaPara = findViewById(R.id.linearLayoutDailyVehAnaParaTwo);
        dailyvehanaLinearLayoutone = findViewById(R.id.dailyvehanaLinearLayoutone);
        btSearchReport = findViewById(R.id.btSearchReportSubmit);
        lstDailyVehParal = findViewById(R.id.lstDailyVehParaVehName);
        etDailyParaVehSelVehName = findViewById(R.id.etDailyVehParaSelVeh);
        button = findViewById(R.id.btSearchReportDateFrom);
        buttonone = findViewById(R.id.btSearchReportDateto);
        _clsInternetConDetect = new InternetConnectionDetector(getApplicationContext());
        durationSpinner = findViewById(R.id.durationSpinner);


        checkGpsAndInternetConnection();
        alphaAnimation = new AlphaAnimation(3.0F, 0.4F);
        lstDailyVehParal.setVisibility(View.GONE);

        etDailyParaVehSelVehName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    etDailyParaVehSelVehName.setError(null);
                    lstDailyVehParal.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Exp - " + e.toString());
                }
            }
        });

        ArrayAdapter<String> durationAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, durationArray);
        durationAdapter.setDropDownViewResource(R.layout.spinner_item);
        durationSpinner.setAdapter(durationAdapter);

        durationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (durationSpinner.getSelectedItemPosition() != 0) {
                    String durationString = durationArray[position];
                    setFrom_To_Date(durationString);
                } else {
                    button.setText(" Date");
                    buttonone.setText(" Date");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        etDailyParaVehSelVehName.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {

                etDailyParaVehSelVehName.setError(null);
                lstDailyVehParal.setVisibility(View.VISIBLE);

                if (cs != null && cs.toString().length() > 0) {
                    searchmode = "search";
                    myfilter(cs);
                } else {
                    searchmode = "normal";
                    listShow();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });

        button.setOnClickListener(this);
        buttonone.setOnClickListener(this);


        btSearchReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean checked = true;
                v.startAnimation(alphaAnimation);

                if (TextUtils.isEmpty(etDailyParaVehSelVehName.getText().toString())) {
                    etDailyParaVehSelVehName.setError("Field can not be blank!");

                    Toast.makeText(getApplicationContext(), "Vehicle Number Can not be Blank!", Toast.LENGTH_SHORT).show();
                } else if (button.getText().toString().trim().equals(" Date")) {

                    button.setError("Please Select Date!");
                    Toast.makeText(getApplicationContext(), "Please Select Date!", Toast.LENGTH_SHORT).show();
                } else if (buttonone.getText().toString().trim().equals(" Date")) {
                    buttonone.setError("Please Select Date!");
                    Toast.makeText(getApplicationContext(), "Please Select Date!", Toast.LENGTH_SHORT).show();
                } else if (dateButton.after(datecurrent)) {
                    Toast.makeText(getApplicationContext(), "You Are selected From Date is Future Date!", Toast.LENGTH_SHORT).show();
                } else if (dateButtonTo.after(datecurrent)) {
                    Toast.makeText(getApplicationContext(), "You Are selected To Date is Future Date!", Toast.LENGTH_SHORT).show();
                } else if (dateButtonTo.before(dateButton)) {
                    Toast.makeText(getApplicationContext(), "Date To Should not be before! " + button.getText().toString().trim(), Toast.LENGTH_SHORT).show();
                } else {
                    String[] vehicledetails;
                    String searchstring;

                    searchstring = etDailyParaVehSelVehName.getText().toString().trim();
                    for (int i = 0; i < vehicleNamecomplete.length - 1; i++) {
                        if (vehicleNamecomplete[i].contains(searchstring)) {
                            selectedPosition = i;
                        }
                    }

                    vehicledetails = vehicleNamecomplete[selectedPosition].split(",");
                    String vehicleno = vehicledetails[0];
                    String vehicleid = vehicledetails[1];
                    String unitno = vehicledetails[2];
                    grapString = vehicleno + vehicleid + unitno;

                    Intent intent = new Intent(getApplicationContext(), DailyAnalysisRootBaseReportList.class);
                    intent.putExtra("vehicleno", vehicleno.toString());
                    intent.putExtra("vehicleid", vehicleid.toString());
                    intent.putExtra("unitno", unitno.toString());
                    intent.putExtra("fromdate", button.getText().toString().trim());
                    intent.putExtra("todate", buttonone.getText().toString().trim());
                    intent.putExtra("fromTime", "00:00");
                    intent.putExtra("toTime", "00:00");

                    startActivity(intent);

                    overridePendingTransition(R.anim.right_in, R.anim.left_out);
                }
            }
        });

        if (vehicleNamecomplete.length <= 0) {
            if (_isInternetPresent) {
                SearchListAsync async = new SearchListAsync();
                async.execute(Login.companyid, Login.userid, this, dailyvehanaLinearLayoutone, "3");
            } else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                        DailyAnalysisRootBase.this);
                alertDialog.setTitle("Error");
                alertDialog.setMessage("Internet Connection Problem");
                alertDialog.setIcon(R.drawable.failtwo);
                alertDialog.setCancelable(false);
                alertDialog.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog,
                                                int which) {
                                finish();
                                overridePendingTransition(R.anim.left_in, R.anim.right_out);
                            }
                        });
                alertDialog.show();
            }
        } else {
            bindglobalarray(vehicleNamecomplete);
        }
    }

    private void checkGpsAndInternetConnection() {

        _isInternetPresent = _clsInternetConDetect.isConnectingToInternet();

    }

    public void myfilter(CharSequence cs) {

        cs = cs.toString().toLowerCase();

        f = new ArrayList<String>();

        if (cs != null && cs.toString().length() > 0) {
            for (int i = 0; i < vehicleName.length; i++) {
                String product = vehicleName[i];
                if (product.toLowerCase().contains(cs))

                    f.add(product);
            }
            listShow();
        }
    }

    public void bindglobalarray(String[] vehicles) {
        barProgressDialog.dismiss();
        String vehicleno;
        vehicleNamecomplete = vehicles;
        vehicleName = new String[vehicleNamecomplete.length];

        for (int i = 0; i <= vehicleNamecomplete.length - 1; i++) {
            vehicleno = vehicleNamecomplete[i].substring(0, vehicleNamecomplete[i].indexOf(","));
            vehicleName[i] = vehicleno;
        }
        listShow();
    }

    private void listShow() {
        if (searchmode == "search") {
            adapterDailyVehPara = new ArrayAdapter<String>(this, R.layout.vehiclelist, R.id.tvVehicleListVehitem, f);
        } else {
            adapterDailyVehPara = new ArrayAdapter<String>(this, R.layout.vehiclelist, R.id.tvVehicleListVehitem, vehicleName);
        }

        lstDailyVehParal.setAdapter(adapterDailyVehPara);

        // Set Data From List View On click of List
        lstDailyVehParal.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                selectedPosition = position;

                etDailyParaVehSelVehName.setText(adapterDailyVehPara.getItem(position));
                int textLength = etDailyParaVehSelVehName.getText().length();
                etDailyParaVehSelVehName.setSelection(textLength, textLength);
                lstDailyVehParal.setVisibility(View.GONE);
            }
        });
    }

    public void setFrom_To_Date(String duration) {
        if (duration.equals("Today")) {
            Calendar calendar = Calendar.getInstance();
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
            String dateString = dateFormat.format(calendar.getTime());

            button.setText(dateString);
            buttonone.setText(dateString);

            Time today = new Time(Time.getCurrentTimezone());
            today.setToNow();

        } else if (duration.equals("Day")) {
            Time today = new Time(Time.getCurrentTimezone());
            today.setToNow();
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, -1);
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
            String dateString = dateFormat.format(calendar.getTime());
            String currentDate = String.valueOf(today.monthDay + "-" + (today.month + 1) + "-" + today.year);

            button.setText(dateString);
            buttonone.setText(currentDate);


        } else if (duration.equals("Week")) {
            Time today = new Time(Time.getCurrentTimezone());
            today.setToNow();
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, -7);
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
            String dateString = dateFormat.format(calendar.getTime());
            String currentDate = String.valueOf(today.monthDay + "-" + (today.month + 1) + "-" + today.year);

            button.setText(dateString);
            buttonone.setText(currentDate);


        } else if (duration.equals("Month")) {
            Time today = new Time(Time.getCurrentTimezone());
            today.setToNow();
            int month;
            int daysInMonth;
            Calendar calendar = Calendar.getInstance();
            boolean isLeapYear = ((Calendar.YEAR % 4 == 0) && (Calendar.YEAR % 100 != 0) || (Calendar.YEAR % 400 == 0));
            month = calendar.get(Calendar.MONTH);
            month = month + 1;
            String currentDate = String.valueOf(today.monthDay + "-" + (today.month + 1) + "-" + today.year);

            if (month == 4 || month == 6 || month == 9 || month == 11) {
                daysInMonth = 30;
            } else if (month == 2) {

                daysInMonth = (isLeapYear) ? 29 : 28;
            } else
                daysInMonth = 31;

            calendar.add(Calendar.DATE, -daysInMonth);
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
            String dateString = dateFormat.format(calendar.getTime());

            button.setText(dateString);
            buttonone.setText(currentDate);


        }
    }

    public void onBackPressed() {
        if (lstDailyVehParal.getVisibility() == View.VISIBLE) {
            lstDailyVehParal.setVisibility(View.GONE);
        } else {
            finish();
            overridePendingTransition(R.anim.left_in, R.anim.right_out);
        }

    }

    public void setNullError() {

        barProgressDialog.dismiss();

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                DailyAnalysisRootBase.this);
        alertDialog.setTitle("Error");
        alertDialog.setMessage("Internet Connection Problem !");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog,
                                        int which) {
                        finish();
                        /*Intent newIntent = new Intent(getApplicationContext(),ReportList.class);
                        startActivity(newIntent);*/
                        overridePendingTransition(R.anim.left_in, R.anim.right_out);
                    }
                });
        alertDialog.show();

    }

    public void setNoDataFoundError() {

        barProgressDialog.dismiss();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                DailyAnalysisRootBase.this);
        alertDialog.setTitle("Error");
        alertDialog.setMessage("No Record Found");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog,
                                        int which) {
                        finish();
                        /*Intent newIntent = new Intent(getApplicationContext(),ReportList.class);
                        startActivity(newIntent);*/
                        overridePendingTransition(R.anim.left_in, R.anim.right_out);
                    }
                });
        alertDialog.show();

    }

    @Override
    public void onClick(View v) {

        if (v == button) {
            v.startAnimation(alphaAnimation);
            button.setError(null);

            // Process to get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            // Launch Date Picker Dialog
            final DatePickerDialog dpd = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            button.setText(dayOfMonth + "-"
                                    + (monthOfYear + 1) + "-" + year);

                            String currdate = button.getText().toString().trim();

                            SimpleDateFormat dfDate = new SimpleDateFormat("dd-MM-yyyy");

                            data = dfDate.format(c.getTime());

                            try {
                                datecurrent = dfDate.parse(data.toString().trim());
                                dateButton = dfDate.parse(currdate.toString().trim());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                    }, mYear, mMonth, mDay);
            dpd.show();
        }

        if (v == buttonone) {

            v.startAnimation(alphaAnimation);
            buttonone.setError(null);
            // Process to get Current Date
            final Calendar c = Calendar.getInstance();

            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            // Launch Date Picker Dialog
            DatePickerDialog dpd = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            // Display Selected date in textbox

                            buttonone.setText(dayOfMonth + "-"
                                    + (monthOfYear + 1) + "-" + year);

                            String buttonDateTo = buttonone.getText().toString();
                            SimpleDateFormat dfDate = new SimpleDateFormat("dd-MM-yyyy");
                            data = dfDate.format(c.getTime());

                            try {
                                datecurrent = dfDate.parse(data.toString().trim());
                                dateButtonTo = dfDate.parse(buttonDateTo.toString().trim());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                        }
                    }, mYear, mMonth, mDay);
            dpd.show();
        }


    }

}

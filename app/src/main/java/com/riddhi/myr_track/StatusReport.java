package com.riddhi.myr_track;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import services.StatusReportAsync;

public class StatusReport extends Activity implements View.OnClickListener {

    public EditText dateET;
    public LinearLayout reportLayout;
    public ProgressBar progressBar;
    public ArrayList<String> nameList = new ArrayList<String>();
    public ArrayList<Integer> valueList = new ArrayList<Integer>();
    Button showStatusReport;
    Context context;
    int mYear;
    int mMonth;
    int mDay;
    String data = "";
    Date datecurrent = new Date();
    Date dateButton = new Date();
    PieChart mChart;
    int colorArray[] = {R.color.color1, R.color.color2, R.color.color3, R.color.color4, R.color.color5,
            R.color.color6, R.color.color7, R.color.color8, R.color.color9, R.color.color10, R.color.color11,
            R.color.color12, R.color.color13, R.color.color14, R.color.color15, R.color.color16, R.color.color17,
            R.color.color18, R.color.color19, R.color.color20};
    private TextView textView3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_status_report);
        //getActionBar().hide();

        context = this;

        Calendar calendar = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        String dateString = dateFormat.format(calendar.getTime());

        String customFont = "fonts/arlrdbd.ttf";
        Typeface typeface = Typeface.createFromAsset(getAssets(), customFont);
        textView3 = findViewById(R.id.textView3);
        textView3.setTypeface(typeface);

        dateET = findViewById(R.id.etUptoDate);
        dateET.setText("" + dateString);
        showStatusReport = findViewById(R.id.btShowReport);
        reportLayout = findViewById(R.id.reportLayout);
        progressBar = findViewById(R.id.progressBar);
        mChart = findViewById(R.id.chart1);

        mChart.setUsePercentValues(true);
        mChart.setDescription("Vehicle status report");
        mChart.setDescriptionTextSize(20);

        mChart.setDrawHoleEnabled(true);
        mChart.setHoleColorTransparent(true);

        mChart.setHoleRadius(7);
        mChart.setTransparentCircleRadius(10);

        mChart.setRotationAngle(0);
        mChart.setRotationEnabled(true);

        dateET.setOnClickListener(this);
        showStatusReport.setOnClickListener(this);
    }

    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    public void onClick(View v) {

        if (v == dateET) {
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            final DatePickerDialog dpd = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            dateET.setText(dayOfMonth + "-"
                                    + (monthOfYear + 1) + "-" + year);

                            String currdate = dateET.getText().toString().trim();

                            SimpleDateFormat dfDate = new SimpleDateFormat("dd-MM-yyyy");

                            data = dfDate.format(c.getTime());

                            try {
                                datecurrent = dfDate.parse(data.toString().trim());
                                dateButton = dfDate.parse(currdate.toString().trim());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                    }, mYear, mMonth, mDay);

            dpd.show();
        } else if (v == showStatusReport) {
            if (dateButton.after(datecurrent)) {
                dateET.setError("Please select date before of today's date");
            } else {
                reportLayout.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);

                StatusReportAsync async = new StatusReportAsync();
                async.execute(dateET.getText().toString(), this, context);
            }
        }
    }

    public void showPieChart(final StatusReport report) {
        mChart.setVisibility(View.VISIBLE);

        ArrayList<Entry> yVals1 = new ArrayList<Entry>();

        for (int i = 0; i < report.valueList.size(); i++)
            yVals1.add(new Entry(report.valueList.get(i), i));

        ArrayList<String> xVals = new ArrayList<String>();

        for (int i = 0; i < report.nameList.size(); i++)
            xVals.add(report.nameList.get(i));

        PieDataSet dataSet = new PieDataSet(yVals1, "");
        dataSet.setSelectionShift(5);

        // need to set colors for slices.
        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());
        dataSet.setColors(colors);

        PieData data = new PieData(xVals, dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(13f);
        data.setValueTextColor(Color.BLACK);

        mChart.setData(data);
        mChart.highlightValues(null);

        mChart.invalidate();

        mChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, int i, Highlight highlight) {

                if (e == null)
                    return;

                Toast.makeText(StatusReport.this, report.nameList.get(e.getXIndex()) + " = " + e.getVal() + "%", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected() {

            }
        });

    }
}

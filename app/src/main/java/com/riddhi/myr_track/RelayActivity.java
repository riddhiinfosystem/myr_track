package com.riddhi.myr_track;

import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;

public class RelayActivity extends AppCompatActivity {

    @BindView(R.id.btnON)
    Button btnON;
    @BindView(R.id.btnOFF)
    Button btnOFF;
    private SQLiteDatabase sqLiteDatabase = null;
    private String TABLE_NAME = "Login";
    private String DB_NAME = "DEMO_DB";

    Cursor cursor;
    String username;
    String pass;
    int count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relay);
        sqLiteDatabase = openOrCreateDatabase(DB_NAME, MODE_PRIVATE, null);

    }

    public void BtnRelayClick(View view)
    {
        if(view.getTag().toString().equalsIgnoreCase("1"))
        {
            showNoteDialog(true);
        }
        else
        {
            showNoteDialog(false);
        }
    }

    private void showNoteDialog(final boolean shouldUpdate) {
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(getApplicationContext());
        View view = layoutInflaterAndroid.inflate(R.layout.note_dialog, null);

        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(RelayActivity.this);
        alertDialogBuilderUserInput.setView(view);

        final EditText inputNote = view.findViewById(R.id.note);
        TextView dialogTitle = view.findViewById(R.id.dialog_title);
        dialogTitle.setText(shouldUpdate ? "Do you Want to ON Engine ?" : "Do you Want to OFF Engine ?");


        alertDialogBuilderUserInput
                .setCancelable(false)
                .setPositiveButton(shouldUpdate ? "ON" : "OFF", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {

                    }
                })
                .setNegativeButton("CANCEL",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });

        final AlertDialog alertDialog = alertDialogBuilderUserInput.create();
        alertDialog.show();

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Show toast message when no text is entered
                if (TextUtils.isEmpty(inputNote.getText().toString())) {
                    inputNote.setError("Enter Password!");
                    Toast.makeText(RelayActivity.this, "Enter Password!", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    //alertDialog.dismiss();
                    cursor = sqLiteDatabase.rawQuery("select * from Login;", null);
                    count = cursor.getCount();

                    if (count > 0) {
                        try {
                            cursor.moveToFirst();
                            username = cursor.getString(1);
                            pass = cursor.getString(2);
                            if(pass.equalsIgnoreCase(""+inputNote.getText().toString()))
                            {
                                String tempval = shouldUpdate ? "Engine ON Successfully" : "Engine OFF Successfully";
                                Toast.makeText(RelayActivity.this, ""+tempval, Toast.LENGTH_LONG).show();
                                String msgVal = shouldUpdate ? "ridd 999 setdigout 1" : "ridd 999 setdigout 0";
                                sendSMS("+915754250521647",msgVal);
                                alertDialog.dismiss();
                            }
                            else
                            {
                                inputNote.setError("Wrong Password!");
                                Toast.makeText(RelayActivity.this, "Wrong Password!", Toast.LENGTH_SHORT).show();
                                return;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                    else
                    {
                        inputNote.setError("Wrong Password!");
                        Toast.makeText(RelayActivity.this, "Wrong Password!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

            }
        });
    }

    public void sendSMS(String phoneNo, String msg) {
        /*try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg, null, null);
            Toast.makeText(getApplicationContext(), "Message Sent",
                    Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(),ex.getMessage().toString(),
                    Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }*/
    }
}
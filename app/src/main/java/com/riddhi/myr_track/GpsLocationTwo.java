package com.riddhi.myr_track;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.Marker;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import services.AsyncLocationType;
import services.AsyncMarkLocation;
import support.LocationAddressName;


public class GpsLocationTwo extends FragmentActivity {

    public static int IsLocType = 1;
    static String selectedItem;
    private static String selectedCountry = null;
    public List<SpinnerList> spinnerLists = new ArrayList<SpinnerList>();
    public List<SpinnerList> newSpinnerList = new ArrayList<SpinnerList>();
    AppLocationService appLocationService;
    double mainlatitude;
    double mainlongitude;
    Marker myMarker;
    Float accuracy = new Float(0);
    List<String> list = new ArrayList<String>();
    private TextView addressLabel;
    private TextView locationLabel;
    private Button getLocationBtn;
    private Button disconnectBtn;
    private Button connectBtn;
    private Button btEditLocation;
    private Button btGpsLocMarLocation;
    private EditText etGpsLocLat;
    private EditText etGpsLocLon;
    private EditText tvGpsLocCurrentAddress;
    private RelativeLayout relativeLayoutGpsLocOne;
    private RelativeLayout relativeLayoutGpsLocTwo;
    private RelativeLayout relativeLayoutGpsLocMain;
    private LinearLayout linearLAyoutGpsLoclast;
    private EditText tvGpsLocCurrentCity;
    private EditText tvGpsLocCurrentCountry;
    private EditText etGpsLocAccuracy;
    private ProgressBar pbGpsLoc;
    private int locTypeId;
    private Spinner spinner;
    private AlphaAnimation alphaAnimation;

    private com.google.android.gms.maps.GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_gps_location);

        try {
            appLocationService = new AppLocationService(GpsLocationTwo.this);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error On Trace!", Toast.LENGTH_SHORT).show();
        }
        alphaAnimation = new AlphaAnimation(3.0F, 0.4F);

        locationLabel = findViewById(R.id.locationLabel);
        addressLabel = findViewById(R.id.addressLabel);
        getLocationBtn = findViewById(R.id.btGpsLocationGetLocation);
        btEditLocation = findViewById(R.id.btGpsLocEditLoc);
        btGpsLocMarLocation = findViewById(R.id.btGpsLocMarkLoc);
        etGpsLocLat = findViewById(R.id.etGpsLocLatitude);
        etGpsLocLon = findViewById(R.id.etGpsLocLongitude);
        tvGpsLocCurrentAddress = findViewById(R.id.etGpsLocLocation);
        etGpsLocAccuracy = findViewById(R.id.etGpsLocAccuracy);
        relativeLayoutGpsLocOne = findViewById(R.id.relativeLayoutgpsLocOne);
        relativeLayoutGpsLocTwo = findViewById(R.id.relativeLayoutGpsLocTwo);
        relativeLayoutGpsLocMain = findViewById(R.id.relativeLayoutGpsLocMain);
        tvGpsLocCurrentCity = findViewById(R.id.etGpsLocCity);
        tvGpsLocCurrentCountry = findViewById(R.id.etGpsLocCountry);
        linearLAyoutGpsLoclast = findViewById(R.id.linearLAyoutGpsLoclast);
        pbGpsLoc = findViewById(R.id.pbGpsLoc);
        spinner = findViewById(R.id.spinner);

        linearLAyoutGpsLoclast.setVisibility(View.GONE);
        tvGpsLocCurrentAddress.setEnabled(false);
        tvGpsLocCurrentCity.setEnabled(false);
        tvGpsLocCurrentCountry.setEnabled(false);


        relativeLayoutGpsLocMain.setVisibility(View.GONE);
        pbGpsLoc.setVisibility(View.VISIBLE);

        try {
            AsyncLocationType asyncLocationType = new AsyncLocationType();
            asyncLocationType.execute(pbGpsLoc, relativeLayoutGpsLocMain, this, spinnerLists);

        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error On Asyc Task!", Toast.LENGTH_SHORT).show();
        }

        relativeLayoutGpsLocOne.setVisibility(View.GONE);
        getLocationBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    displayCurrentLocation();
                } else {
                    showSettingsAlert("GPS");
                }

            }
        });

        btEditLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                v.startAnimation(alphaAnimation);


                tvGpsLocCurrentAddress.setEnabled(true);

            }
        });

        etGpsLocLon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etGpsLocLon.setError(null);
            }
        });

        etGpsLocLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etGpsLocLat.setError(null);
            }
        });
        tvGpsLocCurrentAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvGpsLocCurrentAddress.setError(null);
            }
        });
        tvGpsLocCurrentCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvGpsLocCurrentCity.setError(null);
            }
        });
        tvGpsLocCurrentCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvGpsLocCurrentCountry.setError(null);
            }
        });

        btGpsLocMarLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(alphaAnimation);
                if (etGpsLocLat.getText().toString().equals("")) {
                    etGpsLocLon.setError("Field Can't be blank");
                    Toast.makeText(getApplicationContext(), "Longitude can not be blank!", Toast.LENGTH_SHORT).show();
                } else if (etGpsLocLon.getText().toString().equals("")) {
                    etGpsLocLat.setError("Field Can't be blank");
                    Toast.makeText(getApplicationContext(), "Latitude can not be blank!", Toast.LENGTH_SHORT).show();
                } else if (tvGpsLocCurrentAddress.getText().toString().equals("")) {
                    tvGpsLocCurrentAddress.setError("Field Can't be blank");
                    Toast.makeText(getApplicationContext(), "Current Address can not be blank!", Toast.LENGTH_SHORT).show();
                } else if (tvGpsLocCurrentCity.getText().toString().equals("")) {
                    tvGpsLocCurrentCity.setError("Field can't be blank");
                    Toast.makeText(getApplicationContext(), "City can not be blank!", Toast.LENGTH_SHORT).show();
                } else if (tvGpsLocCurrentCountry.getText().toString().equals("")) {
                    tvGpsLocCurrentCountry.setError("Field can't be blank");
                    Toast.makeText(getApplicationContext(), "Country can not be blank!", Toast.LENGTH_SHORT).show();
                } else if (etGpsLocAccuracy.getText().toString().equals("")) {
                    etGpsLocAccuracy.setError("Accuracy Field can't be blank");
                    Toast.makeText(getApplicationContext(), "Country can not be blank!", Toast.LENGTH_SHORT).show();
                } else if (accuracy > 10.0) {
                    showAccuracyAlertDialog();
                } else {

                    etGpsLocLon.setError(null);
                    etGpsLocLat.setError(null);
                    tvGpsLocCurrentAddress.setError(null);
                    tvGpsLocCurrentCity.setError(null);
                    tvGpsLocCurrentCountry.setError(null);
                    etGpsLocAccuracy.setError(null);

                    saveMarkLocation();

                }
            }
        });
    }

    private void saveMarkLocation() {

        if (selectedItem.equals("General")) {
            locTypeId = 0;
        }
        if (selectedItem.equals("Branch")) {
            locTypeId = 1;
        }
        if (selectedItem.equals("Customer")) {
            locTypeId = 2;
        }
        if (selectedItem.equals("Godown")) {
            locTypeId = 3;
        }
        if (selectedItem.equals("Petrol Pump")) {
            locTypeId = 4;
        }
        if (selectedItem.equals("Toll Naka")) {
            locTypeId = 5;
        }

        String gpsState = "";
        String scurrentAddress = tvGpsLocCurrentAddress.getText().toString();
        String scurrentCity = tvGpsLocCurrentCity.getText().toString();
        String scurrentCountry = tvGpsLocCurrentCountry.getText().toString();
        String slatitude = etGpsLocLat.getText().toString();
        String slongitude = etGpsLocLon.getText().toString();

        AsyncMarkLocation asyncMarkLocation = new AsyncMarkLocation();
        asyncMarkLocation.execute(relativeLayoutGpsLocMain, GpsLocationTwo.this, pbGpsLoc, scurrentAddress, slatitude, slongitude, scurrentCity, gpsState, locTypeId, Login.companyid, Login.userid);
    }

    private void showAccuracyAlertDialog() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                GpsLocationTwo.this);

        alertDialog.setTitle("Accuracy");

        alertDialog
                .setMessage("Accuracy of Gps is Greater than 10 Meters! Are you want to Mark Location?");

        alertDialog.setPositiveButton("Mark Location",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        saveMarkLocation();
                    }
                });

        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog.show();
    }

    private void displayCurrentLocation() {

        relativeLayoutGpsLocOne.setVisibility(View.VISIBLE);
        linearLAyoutGpsLoclast.setVisibility(View.VISIBLE);
        relativeLayoutGpsLocTwo.setVisibility(View.GONE);
        Location gpsLocation = appLocationService
                .getLocation(LocationManager.GPS_PROVIDER);

        spinner.setOnItemSelectedListener(new MyOnItemSelectedListener());

        if (gpsLocation != null) {
            mainlatitude = gpsLocation.getLatitude();
            mainlongitude = gpsLocation.getLongitude();
            etGpsLocLat.setText(String.valueOf(mainlatitude));
            etGpsLocLon.setText(String.valueOf(mainlongitude));
           /* Toast.makeText(
                    getApplicationContext(),
                    "Mobile Location (GPS): \nLatitude: " + mainlatitude
                            + "\nLongitude: " + mainlongitude,
                    Toast.LENGTH_LONG).show();
*/
            accuracy = gpsLocation.getAccuracy();
            etGpsLocAccuracy.setText(String.valueOf(accuracy) + " " + "Meters");

            getLocationAddress();
        } else {
            Toast.makeText(getApplicationContext(), "Please wait Your Gps Location Can not be Access!", Toast.LENGTH_SHORT).show();
        }

        //Dynamically generate a spinner data
        createSpinnerDropDown();
    }

    private void showSettingsAlert(String gps) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                GpsLocationTwo.this);

        alertDialog.setTitle(gps + " SETTINGS");

        alertDialog
                .setMessage(gps + " is not enabled! Want to go to settings menu?");

        alertDialog.setPositiveButton("Settings",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(
                                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        GpsLocationTwo.this.startActivity(intent);
                    }
                });

        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog.show();
    }

    private void getLocationAddress() {

        if (etGpsLocLat.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Gps Location notFound!", Toast.LENGTH_SHORT).show();
        } else {

            double latitude = Double.parseDouble(etGpsLocLat.getText().toString());
            double longitude = Double.parseDouble(etGpsLocLon.getText().toString());
            LocationAddressName locationAddress = new LocationAddressName();
            LocationAddressName.getAddressFromLocation(latitude, longitude,
                    getApplicationContext(), new GeocoderHandler());
        }
    }

    public void markLocationMessage(String message, String msgid) {
        int myMsg = Integer.parseInt(msgid);

        if (myMsg == 0) {
            Toast.makeText(getApplicationContext(), "Location Mark SuccessFully!", Toast.LENGTH_SHORT).show();
            finish();
            overridePendingTransition(R.anim.left_in, R.anim.right_out);

        } else {
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
        }
    }

    public void nullException() {

        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void locationArrayAsync(String[] locationTypeId, String[] locationType) {

        for (int i = 0; i < locationType.length; i++) {
            list.add(locationType[i]);
        }
    }

    private void createSpinnerDropDown() {
        //Array list of animals to display in the spinner
        //create an ArrayAdaptar from the String Array

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        //set the view for the Drop down list
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //set the ArrayAdapter to the spinner
        spinner.setAdapter(dataAdapter);
        //attach the listener to the spinner
        spinner.setOnItemSelectedListener(new MyOnItemSelectedListener());
    }

    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public static class MyOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

            selectedItem = parent.getItemAtPosition(pos).toString();

            //check which spinner triggered the listener
            switch (parent.getId()) {
                //country spinner
                case R.id.spinner:
                    //make sure the country was already selected during the onCreate
                    if (selectedCountry != null) {
                                /*Toast.makeText(parent.getContext(), "Country you selected is " + selectedItem,
                                        Toast.LENGTH_LONG).show();*/
                    }
                    selectedCountry = selectedItem;
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    break;
                default:
                    locationAddress = null;
            }

            StringTokenizer tokens = new StringTokenizer(locationAddress, "\n");
            String firstAddress = tokens.nextToken();// this will contain "Fruit"
            String second = tokens.nextToken();
            String Third = tokens.nextToken();
            String four = tokens.nextToken();
            String five = tokens.nextToken();
            String Six = tokens.nextToken();
            String seven = tokens.nextToken();
            String eight = tokens.nextToken();

//            Toast.makeText(getApplicationContext(),"First:"+firstAddress + "Second:" +second + "Third:" + Third+ "Four:" + four + "Five:" + five+ "Six:" + Six+ "Seven:" + seven + "Eight:" + eight,Toast.LENGTH_LONG).show();

            tvGpsLocCurrentAddress.setText(Third + "," + four + "," + five);
            tvGpsLocCurrentCity.setText(seven);

//            setValueToGoogleMap();
        }
    }
}

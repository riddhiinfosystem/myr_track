package com.riddhi.myr_track;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationManager;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import Fragment.VehicleFragment;
import model.vehiclelist;
import services.AsyncGetTopTenLatestLocation;
import services.ListVehicleAsyncNew;
import services.LocationTypesAsyncNew;
import services.SaveMarkedLocationAsync;
import support.CustomVehicleAdapter;

public class LiveTrackTraceSingleVehicleActivity extends FragmentActivity implements OnMapReadyCallback
{
    public ArrayList<String> locationTypeList;
    public List<vehiclelist> myvehiclelist = new ArrayList<vehiclelist>();
    public Spinner locationTypeSpinner;
    public ProgressBar progressBar;
    String lat, lon, vehicleno, unitNo, reporteddatetime;
    String location, speed,digital3,ignition;
    CountDownTimer cdt;
    Button btTrackVehicleSatelite;
    Button btTrackVehicleMapMode;
    Button RelayDetail;
    Context context;
    String locationType[];
    int locationTypeId[];
    LinearLayout layout;
    EditText userInput;
    Marker myMarker;
    String newVechicleNo, newDate, newLatitude, newLongitude, newLocation, newSpeed;
    Button showDetails;
    Button hideDetails;
    Button markerButton;
    private com.google.android.gms.maps.GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private int mode = 0;

    String preLat="",preLong="";

    private boolean isMarkerRotating = false;
    ArrayList<LatLng> listOfPoints = new ArrayList<>();
    int currentPt = 0;
    LatLng finalPosition;

    private SQLiteDatabase sqLiteDatabase = null;
    private String TABLE_NAME = "Login";
    private String DB_NAME = "DEMO_DB";

    Cursor cursor;
    String username;
    String pass;
    int count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_track_trace_single_vehicle);

        context = this;

        layout = new LinearLayout(this);
        userInput = new EditText(this);
        locationTypeSpinner = new Spinner(this);
        progressBar = findViewById(R.id.progressBar);

        sqLiteDatabase = openOrCreateDatabase(DB_NAME, MODE_PRIVATE, null);

        Bundle bundle = getIntent().getExtras();
        lat = ""+bundle.getString("lat");
        lon = ""+bundle.getString("lon");
        vehicleno = ""+bundle.getString("vehicleno");
        reporteddatetime = ""+bundle.getString("reporteddatetime");
        unitNo = ""+bundle.getString("unitNo");
        location = ""+bundle.getString("location");
        speed = ""+bundle.getString("speed");
        digital3 = ""+bundle.getString("digital3");
        ignition = ""+bundle.getString("ignition");
        preLat=lat;
        preLong=lon;

        btTrackVehicleSatelite = findViewById(R.id.btTrackVehicleSatelliteMode);
        btTrackVehicleMapMode = findViewById(R.id.btTrackVehicleMapMode);
        showDetails = findViewById(R.id.showDetails);
        hideDetails = findViewById(R.id.hideDetails);
        markerButton = findViewById(R.id.markerButton);
        RelayDetail = findViewById(R.id.RelayDetail);

        showDetails.setVisibility(View.VISIBLE);

        locationTypeList = new ArrayList<String>();

        SupportMapFragment mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFrag.getMapAsync(this);


        showDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDetails.setVisibility(View.GONE);
                btTrackVehicleSatelite.setVisibility(View.VISIBLE);
                btTrackVehicleMapMode.setVisibility(View.VISIBLE);
                hideDetails.setVisibility(View.VISIBLE);
                markerButton.setVisibility(View.VISIBLE);
            }
        });

        hideDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDetails.setVisibility(View.VISIBLE);
                btTrackVehicleSatelite.setVisibility(View.GONE);
                btTrackVehicleMapMode.setVisibility(View.GONE);
                hideDetails.setVisibility(View.GONE);
                markerButton.setVisibility(View.GONE);
            }
        });

        markerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressBar.setVisibility(View.VISIBLE);
                LocationTypesAsyncNew asyncNew = new LocationTypesAsyncNew();
                asyncNew.execute(LiveTrackTraceSingleVehicleActivity.this, context);

                layout.removeAllViews();

                layout = new LinearLayout(context);
                layout.setOrientation(LinearLayout.VERTICAL);

                final EditText userInput = new EditText(context);
                userInput.setText(newLocation);
                layout.addView(locationTypeSpinner);
                layout.addView(userInput);

                locationTypeList.add(0, "Select Location Type");

                final AlertDialog.Builder builder = new AlertDialog.Builder(LiveTrackTraceSingleVehicleActivity.this);

                builder.setView(layout);

                builder.setTitle("Mark Location");
                builder.setMessage("Enter location");

                builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String location = userInput.getText().toString();
                        String locationType = String.valueOf(locationTypeSpinner.getSelectedItemId() - 1);

                        if (locationTypeSpinner.getSelectedItemId() != 0) {
                            progressBar.setVisibility(View.VISIBLE);
                            SaveMarkedLocationAsync async = new SaveMarkedLocationAsync();
                            async.execute(LiveTrackTraceSingleVehicleActivity.this, locationType, location, newLatitude, newLongitude);
                        } else {
                            Toast.makeText(context, "Please select location type", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        });

        btTrackVehicleSatelite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mode = 0;
                mMap.setMapType(com.google.android.gms.maps.GoogleMap.MAP_TYPE_SATELLITE);
                Toast.makeText(getApplicationContext(), "Satellite Mode", Toast.LENGTH_SHORT).show();
                //btTrackVehicleSatelite.setVisibility(View.GONE);
                btTrackVehicleMapMode.setVisibility(View.VISIBLE);
            }
        });

        btTrackVehicleMapMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mode = 1;
                mMap.setMapType(com.google.android.gms.maps.GoogleMap.MAP_TYPE_NORMAL);
                Toast.makeText(getApplicationContext(), "Map Mode", Toast.LENGTH_SHORT).show();
                //btTrackVehicleMapMode.setVisibility(View.GONE);
                btTrackVehicleSatelite.setVisibility(View.VISIBLE);
            }
        });

        RelayDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,RelayActivity.class);
                startActivity(intent);
            }
        });

        cursor = sqLiteDatabase.rawQuery("select * from Login;", null);
        count = cursor.getCount();

        if (count > 0) {
            try {
                cursor.moveToFirst();
                username = cursor.getString(1);
                pass = cursor.getString(2);
                if(username.equalsIgnoreCase("snehal"))
                {
                    RelayDetail.setVisibility(View.VISIBLE);
                }
                else
                {
                    RelayDetail.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        else
        {
            RelayDetail.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        // Check if we were successful in obtaining the map.
        Log.e("onMapReady-->","called");
        mMap = googleMap;
        if (mMap != null) {

            LatLng myloc = new LatLng(Double.valueOf(lat), Double.valueOf(lon));
            //LatLng myloc = new LatLng(0,0);


            if(Login.companyid == 142)
            {
                myMarker = mMap.addMarker(new MarkerOptions()
                        .position(myloc)
                        .title("")
                        .snippet("")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.truck_moving_police)));
            }
            else if(Login.companyid == 181)
            {
                myMarker = mMap.addMarker(new MarkerOptions()
                        .position(myloc)
                        .title("")
                        .snippet("")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.truck_moving_ambulance)));
            }
            else {
                myMarker = mMap.addMarker(new MarkerOptions()
                        .position(myloc)
                        .title("")
                        .snippet("")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.truck_moving)));
            }
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myloc, 15));

            if (mode == 0) {
                mMap.setMapType(com.google.android.gms.maps.GoogleMap.MAP_TYPE_NORMAL);
            } else {
                mMap.setMapType(com.google.android.gms.maps.GoogleMap.MAP_TYPE_SATELLITE);
            }

            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }

        }

        mMap.setInfoWindowAdapter(new com.google.android.gms.maps.GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker arg0) {

                View v = getLayoutInflater().inflate(R.layout.info_window_layout, null);

                // Getting the position from the marker
                LatLng latLng = arg0.getPosition();

                // Getting reference to the TextView to set latitude
                TextView tvTrackVehicleVehicleNo = v.findViewById(R.id.tvTrackVechielNo);

                TextView tvTrackVehicleAC = v.findViewById(R.id.tvTrackVehicleAC);

                TextView tvTrackVehicleIgnation = v.findViewById(R.id.tvTrackVehicleIgnation);

                // Getting reference to the TextView to set longitude
                TextView tvTrackVehicleSpeed = v.findViewById(R.id.tvTrackVehicleSpeed);

                TextView tvTrackVehicleDate = v.findViewById(R.id.tvTrackVehicleTime);

                final TextView tvTrackVehicleLocation = v.findViewById(R.id.tvTrackVehicleLocation);

                //TextView markLocationView = (TextView) v.findViewById(R.id.markLocationView);
                // Setting the latitude
                tvTrackVehicleVehicleNo.setText("Vehicle No:" + vehicleno);

                if(Login.userid == 448 || Login.userid == 477 || Login.userid == 621 || Login.userid == 476)
                {
                    tvTrackVehicleAC.setVisibility(View.VISIBLE);
                    tvTrackVehicleAC.setText("AC Status : " + digital3);
                }
                else
                {
                    tvTrackVehicleAC.setVisibility(View.GONE);
                }
                if(!ignition.equalsIgnoreCase(""))
                {
                    tvTrackVehicleIgnation.setVisibility(View.VISIBLE);
                    if (ignition.equalsIgnoreCase("0"))
                    {
                        tvTrackVehicleIgnation.setText("Ignition: OFF");
                    } else {
                        tvTrackVehicleIgnation.setText("Ignition: ON");
                    }
                }
                else
                {
                    tvTrackVehicleIgnation.setVisibility(View.GONE);
                }

                // Setting the longitude
                tvTrackVehicleSpeed.setText("Speed:" + speed);


                tvTrackVehicleDate.setText("Date & Time:" + reporteddatetime);

                tvTrackVehicleLocation.setText("Location:" + location);
                // Returning the view containing InfoWindow contents
                return v;
            }
        });

        mMap.setMyLocationEnabled(false);
        mMap.setTrafficEnabled(true);

        final Handler handler = new Handler();

        //new handler
        handler.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                Log.e("postDelayed-->","Called");
                populatevehiclelist();
                handler.postDelayed(this, 20000);
            }
        }, 20000);
    }

    public void populatevehiclelistview(List<vehiclelist> VList)
    {
        if(VList.size() > 0)
        {
            if(!preLat.equalsIgnoreCase(""+VList.get(0).getLat()) && !preLong.equalsIgnoreCase(""+VList.get(0).getLon()))
            {
                //speed,reporteddatetime,location;
                preLat = VList.get(0).getLat();
                preLong = VList.get(0).getLon();
                speed = VList.get(0).getSpeed();
                reporteddatetime = VList.get(0).getLastreported();
                location = VList.get(0).getLocation();

                Location location = new Location(LocationManager.GPS_PROVIDER);
                location.setLatitude(Double.parseDouble(preLat));
                location.setLongitude(Double.parseDouble(preLong));
                animateMarkerNew(location,myMarker);
            }
        }
    }

    private void populatevehiclelist() {

        AsyncGetTopTenLatestLocation lvAsysnc = new AsyncGetTopTenLatestLocation();
        lvAsysnc.execute(unitNo, myvehiclelist, progressBar, LiveTrackTraceSingleVehicleActivity.this);
    }


    private void animateMarkerNew(final Location destination, final Marker marker) {

        if (marker != null)
        {
            final LatLng startPosition = marker.getPosition();
            final LatLng endPosition = new LatLng(destination.getLatitude(), destination.getLongitude());

            final float startRotation = marker.getRotation();
            final LatLngInterpolatorNew latLngInterpolator = new LatLngInterpolatorNew.LinearFixed();

            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
            valueAnimator.setDuration(3000); // duration 3 second
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    try {
                        float v = animation.getAnimatedFraction();
                        LatLng newPosition = latLngInterpolator.interpolate(v, startPosition, endPosition);
                        marker.setPosition(newPosition);

                        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                                .target(newPosition)
                                .zoom(15)
                                .build()));

                        marker.setRotation(getBearing(startPosition, new LatLng(destination.getLatitude(), destination.getLongitude())));

                    } catch (Exception ex) {
                       ex.printStackTrace();
                    }
                }
            });
            valueAnimator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);

                    // if (marker != null) {
                   //      marker.remove();
                   // }
                   // marker = mMap.addMarker(new MarkerOptions().position(endPosition).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car)));

                }
            });
            valueAnimator.start();
        }
    }

    private interface LatLngInterpolatorNew {
        LatLng interpolate(float fraction, LatLng a, LatLng b);

        class LinearFixed implements LatLngInterpolatorNew {
            @Override
            public LatLng interpolate(float fraction, LatLng a, LatLng b) {
                double lat = (b.latitude - a.latitude) * fraction + a.latitude;
                double lngDelta = b.longitude - a.longitude;
                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360;
                }
                double lng = lngDelta * fraction + a.longitude;
                return new LatLng(lat, lng);
            }
        }
    }


    //Method for finding bearing between two points
    private float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;
    }
}

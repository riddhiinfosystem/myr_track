package com.riddhi.myr_track;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import model.Searchlist;
import services.DateWiseReportDataAsync;
import support.InternetConnectionDetector;


public class SearchReportList extends Activity {

    ListView listView;
    TextView textViewVehcileNo;
    ProgressDialog barProgressDialog;
    List<Searchlist> searchlistList = new ArrayList<Searchlist>();

    String vehicleno;
    String vehicleid;
    String unitno;
    String datefrom;
    String dateto;
    String fromTime, toTime;
    ReportListAdapter adaptercustom;
    private Boolean _isInternetPresent = false;
    private InternetConnectionDetector _clsInternetConDetect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_search_report_list);
        //getActionBar().hide();

        barProgressDialog = ProgressDialog.show(SearchReportList.this, "Please wait...", "Loading...", true);
        barProgressDialog.setCancelable(false);

        textViewVehcileNo = findViewById(R.id.tvvehicleno);
        listView = findViewById(R.id.lstSearchReportList);

        Intent intent = getIntent();

        vehicleno = intent.getStringExtra("vehicleno");
        vehicleid = intent.getStringExtra("vehicleid");
        unitno = intent.getStringExtra("unitno");
        datefrom = intent.getStringExtra("fromdate");
        dateto = intent.getStringExtra("todate");
        fromTime = intent.getStringExtra("fromTime");
        toTime = intent.getStringExtra("toTime");

        _clsInternetConDetect = new InternetConnectionDetector(getApplicationContext());

        checkGpsAndInternetConnection();
        if (_isInternetPresent) {
            populatevehiclelist();
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                    SearchReportList.this);
            alertDialog.setTitle("Error");
            alertDialog.setMessage("Internet Connection Problem");
            alertDialog.setIcon(R.drawable.failtwo);
            alertDialog.setCancelable(false);
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog,
                                            int which) {
                            finish();
                            overridePendingTransition(R.anim.left_in, R.anim.right_out);

                        }
                    });
            alertDialog.show();
        }
        textViewVehcileNo.setText(vehicleno);
    }

    private void checkGpsAndInternetConnection() {

        _isInternetPresent = _clsInternetConDetect.isConnectingToInternet();
    }

    private void populatevehiclelist() {
        DateWiseReportDataAsync lvAsysnc = new DateWiseReportDataAsync();
        lvAsysnc.execute(vehicleid, unitno, datefrom, dateto, searchlistList, SearchReportList.this, fromTime, toTime);
    }

    public void populatevehiclelistview(List<Searchlist> VList) {

        barProgressDialog.dismiss();
        adaptercustom = new ReportListAdapter(VList);
        listView.setAdapter(adaptercustom);
    }

    public void setNullError() {

        barProgressDialog.dismiss();

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                SearchReportList.this);
        alertDialog.setTitle("Error");
        alertDialog.setMessage("Internet Connection Problem !");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog,
                                        int which) {
                        finish();
                        overridePendingTransition(R.anim.left_in, R.anim.right_out);
                    }
                });
        alertDialog.show();
    }

    public void setNoDataFoundError() {

        barProgressDialog.dismiss();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                SearchReportList.this);
        alertDialog.setTitle("Error");
        alertDialog.setMessage("No Record Found !");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog,
                                        int which) {
                        finish();
                        overridePendingTransition(R.anim.left_in, R.anim.right_out);
                    }
                });
        alertDialog.show();
    }

    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    private class ReportListAdapter extends ArrayAdapter<Searchlist> {
        List<Searchlist> vlistinternal;

        public ReportListAdapter(List<Searchlist> vList) {
            super(SearchReportList.this, R.layout.searchlistfiltershow, vList);
            vlistinternal = vList;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View vehicleinfoview = convertView;
            Searchlist Searchlist;
            if (vehicleinfoview == null) {

                vehicleinfoview = getLayoutInflater().inflate(R.layout.searchlistfiltershow, parent, false);
            }

            Searchlist = searchlistList.get(position);
            String count = Searchlist.getSrno().toString();

            LinearLayout linearLayout = vehicleinfoview.findViewById(R.id.linearLayoutsearchlistfiltershowone);

            Double stringTotRunKm = Double.parseDouble(Searchlist.getTotalrunningkms());

            TextView textViewsrno = vehicleinfoview.findViewById(R.id.textView2);
            TextView textViewdate = vehicleinfoview.findViewById(R.id.textView4);
            TextView textViewTotalRunningKms = vehicleinfoview.findViewById(R.id.textView5);
            TextView textViewTotalRunningTimes = vehicleinfoview.findViewById(R.id.textView6);
            TextView textViewTotalStopageTime = vehicleinfoview.findViewById(R.id.textView7);

            if (position % 2 == 0) {
                vehicleinfoview.setBackgroundResource(R.color.light_gray);
                textViewsrno.setTextColor(getResources().getColor(R.color.black_color));
                textViewdate.setTextColor(getResources().getColor(R.color.black_color));
                textViewTotalRunningKms.setTextColor(getResources().getColor(R.color.black_color));
                textViewTotalRunningTimes.setTextColor(getResources().getColor(R.color.black_color));
                textViewTotalStopageTime.setTextColor(getResources().getColor(R.color.black_color));
            } else {
                vehicleinfoview.setBackgroundResource(R.color.dark_gray);
                textViewsrno.setTextColor(getResources().getColor(R.color.black_color));
                textViewdate.setTextColor(getResources().getColor(R.color.black_color));
                textViewTotalRunningKms.setTextColor(getResources().getColor(R.color.black_color));
                textViewTotalRunningTimes.setTextColor(getResources().getColor(R.color.black_color));
                textViewTotalStopageTime.setTextColor(getResources().getColor(R.color.black_color));
            }

            textViewsrno.setText(Searchlist.getSrno());
            textViewdate.setText(Searchlist.getdate());

            if (stringTotRunKm >= 0.00) {
                textViewTotalRunningKms.setText(stringTotRunKm.toString());
            } else {
                textViewTotalRunningKms.setText("");
            }

            textViewTotalRunningTimes.setText(Searchlist.getTotalrunningtime());
            textViewTotalStopageTime.setText(Searchlist.getTotalstopagetime());

            return vehicleinfoview;
        }
    }
}

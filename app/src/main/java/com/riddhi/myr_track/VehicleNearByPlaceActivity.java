package com.riddhi.myr_track;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import model.NearByVehicleList;
import services.AsyncNearByVehiclePlaceShowReport;
import services.SearchPlaceAsync;
import support.InternetConnectionDetector;
import support.NearByVehicleAdapter;

public class VehicleNearByPlaceActivity extends Activity {
    List<NearByVehicleList> nearByVehicle = new ArrayList<NearByVehicleList>();
    String data = "";
    ProgressDialog barProgressDialog;
    ArrayAdapter<String> adapterDailyVehPara;
    ArrayList<String> f;
    String[] vehiclePlaceList = {}, vehicleNamecomplete = {}, vehicleLatLongList = {};
    private int mYear, mMonth, mDay;
    private String searchmode;
    private int selectedPosition;
    private AlphaAnimation alphaAnimation;
    private EditText button;
    private EditText etDistance;
    private ListView lstDailyVehParal;
    private Boolean _isInternetPresent = false;
    private InternetConnectionDetector _clsInternetConDetect;
    private EditText etDailyParaVehSelVehName;
    private LinearLayout dailyvehanaLinearLayoutone;
    private TextView textView3;


    private ListView lstNearByVehicle;
    private NearByVehicleAdapter nearByVehicleAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_vehicle_near_by_place);

        String customFont = "fonts/arlrdbd.ttf";
        Typeface typeface = Typeface.createFromAsset(getAssets(), customFont);
        textView3 = findViewById(R.id.textView3);
        textView3.setTypeface(typeface);

        searchmode = "normal";

        dailyvehanaLinearLayoutone = findViewById(R.id.dailyvehanaLinearLayoutone);
        lstDailyVehParal = findViewById(R.id.lstDailyVehParaVehName);
        etDailyParaVehSelVehName = findViewById(R.id.etDailyVehParaSelVeh);
        etDistance = findViewById(R.id.etDistance);
        etDistance.setText("10");
        lstNearByVehicle = findViewById(R.id.lstNearByVehicle);

        _clsInternetConDetect = new InternetConnectionDetector(getApplicationContext());

        checkGpsAndInternetConnection();
        alphaAnimation = new AlphaAnimation(3.0F, 0.4F);
        lstDailyVehParal.setVisibility(View.GONE);

        etDailyParaVehSelVehName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    etDailyParaVehSelVehName.setError(null);
                    lstDailyVehParal.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Exp - " + e.toString());
                }
            }
        });

        etDailyParaVehSelVehName.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                etDailyParaVehSelVehName.setError(null);
                lstDailyVehParal.setVisibility(View.VISIBLE);

                if (cs != null && cs.toString().length() > 2) {
                    searchmode = "search";
                    //myfilter(cs);
                    if (_isInternetPresent) {
                        SearchPlaceAsync async = new SearchPlaceAsync();
                        async.execute(Login.companyid, "" + etDailyParaVehSelVehName.getText().toString(), VehicleNearByPlaceActivity.this, dailyvehanaLinearLayoutone);
                    } else {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                                VehicleNearByPlaceActivity.this);
                        alertDialog.setTitle("Error");
                        alertDialog.setMessage("Internet Connection Problem");
                        alertDialog.setIcon(R.drawable.failtwo);
                        alertDialog.setCancelable(false);
                        alertDialog.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        finish();
                                        overridePendingTransition(R.anim.left_in, R.anim.right_out);
                                    }
                                });
                        alertDialog.show();
                    }

                } else {
                    searchmode = "normal";
                    vehiclePlaceList = null;
                    vehiclePlaceList = new String[0];
                    listShow();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });

        etDailyParaVehSelVehName.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (etDailyParaVehSelVehName.getRight() - etDailyParaVehSelVehName.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
                        etDailyParaVehSelVehName.setText("");

                        return true;
                    }
                }
                return false;
            }
        });


    }

    private void checkGpsAndInternetConnection() {

        _isInternetPresent = _clsInternetConDetect.isConnectingToInternet();

    }

    public void onBackPressed() {
        if (lstDailyVehParal.getVisibility() == View.VISIBLE) {
            lstDailyVehParal.setVisibility(View.GONE);
        } else {
            finish();
            overridePendingTransition(R.anim.left_in, R.anim.right_out);
        }

    }

    public void setNullError() {

        barProgressDialog.dismiss();

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                VehicleNearByPlaceActivity.this);
        alertDialog.setTitle("Error");
        alertDialog.setMessage("Internet Connection Problem !");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog,
                                        int which) {
                        nearByVehicleAdapter = new NearByVehicleAdapter(VehicleNearByPlaceActivity.this, nearByVehicle);
                        // Assign adapter to ListView
                        lstNearByVehicle.setAdapter(nearByVehicleAdapter);
                        nearByVehicleAdapter.notifyDataSetChanged();
                    }
                });
        alertDialog.show();

    }

    public void setNoDataFoundError() {

        barProgressDialog.dismiss();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                VehicleNearByPlaceActivity.this);
        alertDialog.setTitle("Error");
        alertDialog.setMessage("No Record Found");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog,
                                        int which) {
                        nearByVehicleAdapter = new NearByVehicleAdapter(VehicleNearByPlaceActivity.this, nearByVehicle);
                        // Assign adapter to ListView
                        lstNearByVehicle.setAdapter(nearByVehicleAdapter);
                        nearByVehicleAdapter.notifyDataSetChanged();
                    }
                });
        alertDialog.show();

    }

    private void listShow() {
        if (searchmode == "search") {
            adapterDailyVehPara = new ArrayAdapter<String>(this, R.layout.vehiclelist, R.id.tvVehicleListVehitem, f);
            lstDailyVehParal.setAdapter(adapterDailyVehPara);
            adapterDailyVehPara.notifyDataSetChanged();
        } else {
           /* if(vehiclePlaceList == null)
            {
                vehiclePlaceList = new String[vehicleNamecomplete.length];
            }*/
            adapterDailyVehPara = new ArrayAdapter<String>(this, R.layout.vehiclelist, R.id.tvVehicleListVehitem, vehiclePlaceList);
            lstDailyVehParal.setAdapter(adapterDailyVehPara);
            adapterDailyVehPara.notifyDataSetChanged();
        }


        // Set Data From List View On click of List
        lstDailyVehParal.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedPosition = position;
                etDailyParaVehSelVehName.setText(adapterDailyVehPara.getItem(position));

                int textLength = etDailyParaVehSelVehName.getText().length();
                etDailyParaVehSelVehName.setSelection(textLength, textLength);
                lstDailyVehParal.setVisibility(View.GONE);
                String[] vehicleArray = new String[2];
                if (searchmode == "search" && f.size() > 0) {
                    Log.e("onItemClick", "onItemClick---->" + f.get(0).toString());
                    for (int k = 0; k < vehiclePlaceList.length; k++) {
                        if (f.get(0).equalsIgnoreCase("" + vehiclePlaceList[k].toString())) {
                            selectedPosition = k;
                            String tempLat = "", tempLong = "";
                            vehicleArray = null;
                            vehicleArray = vehicleLatLongList[selectedPosition].split(",");
                            tempLat = vehicleArray[0].toString();
                            tempLong = vehicleArray[1].toString();


                            if (_isInternetPresent) {
                                barProgressDialog = ProgressDialog.show(VehicleNearByPlaceActivity.this, "Please wait ...", "Downloading...", true);
                                barProgressDialog.setCancelable(false);

                                AsyncNearByVehiclePlaceShowReport async = new AsyncNearByVehiclePlaceShowReport();
                                async.execute(tempLat, tempLong, "" + etDistance.getText().toString(), Login.userid, Login.companyid, VehicleNearByPlaceActivity.this);
                            } else {
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                                        VehicleNearByPlaceActivity.this);
                                alertDialog.setTitle("Error");
                                alertDialog.setMessage("Internet Connection Problem");
                                alertDialog.setIcon(R.drawable.failtwo);
                                alertDialog.setCancelable(false);
                                alertDialog.setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {

                                            public void onClick(DialogInterface dialog, int which) {
                                                finish();
                                                overridePendingTransition(R.anim.left_in, R.anim.right_out);
                                            }
                                        });
                                alertDialog.show();
                            }
                        }
                    }
                }

            }
        });
    }

    public void bindglobalarray(String[] vehicles) {
        String placeName, latLong;
        vehicleNamecomplete = null;
        vehicleNamecomplete = vehicles;

        String[] vehicleArray = new String[2];

        vehiclePlaceList = null;
        vehiclePlaceList = new String[vehicleNamecomplete.length];
        vehicleLatLongList = null;
        vehicleLatLongList = new String[vehicleNamecomplete.length];

        f = new ArrayList<String>();
        f.clear();

        for (int i = 0; i <= vehicleNamecomplete.length - 1; i++) {
            vehicleArray = null;
            vehicleArray = vehicleNamecomplete[i].split("\\?+");
            placeName = vehicleArray[0].toString();
            latLong = vehicleArray[1].toString();
            vehiclePlaceList[i] = placeName;
            vehicleLatLongList[i] = latLong;
            f.add("" + placeName);
        }

        listShow();
    }

    public void setNearByVehicleData(List<NearByVehicleList> _myNearByVehicleList) {
        this.nearByVehicle = _myNearByVehicleList;
        barProgressDialog.dismiss();
        nearByVehicleAdapter = new NearByVehicleAdapter(VehicleNearByPlaceActivity.this, nearByVehicle);
        // Assign adapter to ListView
        lstNearByVehicle.setAdapter(nearByVehicleAdapter);
        nearByVehicleAdapter.notifyDataSetChanged();

        Log.e("nearByVehicle_size", "---->" + nearByVehicle.size());
    }

}

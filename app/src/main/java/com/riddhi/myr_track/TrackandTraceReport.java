package com.riddhi.myr_track;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.Time;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import services.AsyncTrackandTraceReport;
import support.FontChangeCrawler;
import support.InternetConnectionDetector;


public class TrackandTraceReport extends Activity implements OnClickListener {

    ArrayAdapter<String> adapterDailyVehPara;
    ArrayList<String> f;
    ProgressDialog barProgressDialog;
    FontChangeCrawler fontChanger;
    Spinner durationSpinner;
    String[] durationArray = {"Select Duration", "Today", "Day", "Week", "Month"};
    private EditText etVehicleNo;
    private ListView lvVehicleNo;
    private EditText btTrackandTraceRptDateFrom;
    private EditText btTrackandTraceRptDateTo;
    private Button btTrackandTraceRptSubmit;
    private LinearLayout linearLayoutTraceandTraceRptFour;
    private LinearLayout linearLayoutTrackandTraceReport;
    private int mYear, mMonth, mDay;
    private String[] vehicleName, vehicleNamecomplete;
    private int selectedPosition;
    private String searchmode;
    private int intHour;
    private int intMinute;
    private String stringTimeZone;
    private EditText btSelctTimeTo;
    private EditText btSelectTimeFrom;
    private AlphaAnimation alphaAnimation;
    private Date datecurrent = new Date();
    private Date dateButtonFrom = new Date();
    private Date dateButtonTo = new Date();
    private String data = "";
    private Boolean _isInternetPresent = false;
    private InternetConnectionDetector _clsInternetConDetect;
    private TextView textView3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_trackand_trace_report);
        //getActionBar().hide();

        String customFont = "fonts/arlrdbd.ttf";
        Typeface typeface = Typeface.createFromAsset(getAssets(), customFont);
        textView3 = findViewById(R.id.textView3);
        textView3.setTypeface(typeface);

        alphaAnimation = new AlphaAnimation(3.0F, 0.4F);

        allInitialization();

        ArrayAdapter<String> durationAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, durationArray);
        durationAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        durationSpinner.setAdapter(durationAdapter);

        durationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (durationSpinner.getSelectedItemPosition() != 0) {
                    String durationString = durationArray[position];
                    setFrom_To_Date(durationString);
                    setDuration_Time();
                } else {
                    btTrackandTraceRptDateTo.setText(" Date");
                    btTrackandTraceRptDateFrom.setText(" Date");
                    btSelctTimeTo.setText(" Time");
                    btSelectTimeFrom.setText(" Time");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        checkGpsAndInternetConnection();
        barProgressDialog = ProgressDialog.show(TrackandTraceReport.this, "Please wait ...", "Downloading...", true);
        barProgressDialog.setCancelable(false);

        searchmode = "normal";
        //linearLayoutTrackandTraceReport.setVisibility(View.GONE);

        if (_isInternetPresent) {

            AsyncTrackandTraceReport async = new AsyncTrackandTraceReport();
            async.execute(Login.companyid, Login.userid, this, linearLayoutTrackandTraceReport);
        }

        etVehicleNo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                etVehicleNo.setError(null);
                lvVehicleNo.setVisibility(View.VISIBLE);
                //linearLayoutTraceandTraceRptFour.setVisibility(View.GONE);
            }
        });

        etVehicleNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence cs, int start, int count, int after) {

                etVehicleNo.setError(null);
                lvVehicleNo.setVisibility(View.VISIBLE);
                //linearLayoutTraceandTraceRptFour.setVisibility(View.GONE);

                if (cs != null && cs.toString().length() > 0) {
                    searchmode = "search";
                    myfilter(cs);
                } else {
                    searchmode = "normal";
                    listShow();

                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void checkGpsAndInternetConnection() {

        _isInternetPresent = _clsInternetConDetect.isConnectingToInternet();

    }

    public void setNullError() {

        barProgressDialog.dismiss();

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                TrackandTraceReport.this);
        alertDialog.setTitle("Error");
        alertDialog.setMessage("Internet Connection Problem !");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog,
                                        int which) {
                        finish();
                        overridePendingTransition(R.anim.left_in, R.anim.right_out);
                    }
                });
        alertDialog.show();

    }

    public void setNoDataFoundError() {

        barProgressDialog.dismiss();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                TrackandTraceReport.this);
        alertDialog.setTitle("Error");
        alertDialog.setMessage("No Record Found");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog,
                                        int which) {
                        finish();

                        overridePendingTransition(R.anim.left_in, R.anim.right_out);
                    }
                });
        alertDialog.show();

    }

    private void listShow() {

        if (searchmode == "search") {
            adapterDailyVehPara = new ArrayAdapter<String>(this, R.layout.vehiclelist, R.id.tvVehicleListVehitem, f);

        } else {
            adapterDailyVehPara = new ArrayAdapter<String>(this, R.layout.vehiclelist, R.id.tvVehicleListVehitem, vehicleName);
        }

        lvVehicleNo.setAdapter(adapterDailyVehPara);

        // Set Data From List View On click of List
        lvVehicleNo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                selectedPosition = position;

                etVehicleNo.setText(adapterDailyVehPara.getItem(position));
                int textLength = etVehicleNo.getText().length();
                etVehicleNo.setSelection(textLength, textLength);
                lvVehicleNo.setVisibility(View.GONE);
                //linearLayoutTraceandTraceRptFour.setVisibility(View.VISIBLE);
            }
        });
        /*
         * All Click Listener
         *
         * */

        btTrackandTraceRptDateFrom.setOnClickListener(this);
        btTrackandTraceRptDateTo.setOnClickListener(this);

        btSelctTimeTo.setOnClickListener(this);
        btSelectTimeFrom.setOnClickListener(this);


        btTrackandTraceRptSubmit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                v.startAnimation(alphaAnimation);

                if (etVehicleNo.getText().toString().trim().equals("")) {
                    etVehicleNo.setError("Field can not be blank!");

                    Toast.makeText(getApplicationContext(), "Vehicle Number Can not be Blank!", Toast.LENGTH_SHORT).show();
                } else if (btTrackandTraceRptDateFrom.getText().toString().trim().equals("Click to Select Date")) {

                    btTrackandTraceRptDateFrom.setError("Please Select Date!");
                    Toast.makeText(getApplicationContext(), "Please Select Date!", Toast.LENGTH_SHORT).show();
                } else if (btTrackandTraceRptDateTo.getText().toString().trim().equals("Click to Select Date")) {
                    btTrackandTraceRptDateTo.setError("Please Select Date!");
                    Toast.makeText(getApplicationContext(), "Please Select Date!", Toast.LENGTH_SHORT).show();
                } else if (dateButtonFrom.after(datecurrent)) {
                    Toast.makeText(getApplicationContext(), "You Are selected From Date is Future Date!", Toast.LENGTH_SHORT).show();
                } else if (dateButtonTo.after(datecurrent)) {
                    Toast.makeText(getApplicationContext(), "You Are selected To Date is Future Date!", Toast.LENGTH_SHORT).show();
                } else if (dateButtonTo.before(dateButtonFrom)) {
                    Toast.makeText(getApplicationContext(), "Date To Should not be before! " + btTrackandTraceRptDateFrom.getText().toString().trim(), Toast.LENGTH_SHORT).show();
                } else {


                    String[] vehicleDetails;
                    String searchString;

                    searchString = etVehicleNo.getText().toString().trim();
                    for (int i = 0; i < vehicleNamecomplete.length - 1; i++) {
                        if (vehicleNamecomplete[i].contains(searchString)) {
                            selectedPosition = i;
                        }
                    }
                    vehicleDetails = vehicleNamecomplete[selectedPosition].split(",");
                    String vehicleno = vehicleDetails[0];

                    String vehicleid = vehicleDetails[1];

                    String datetimefrom = btTrackandTraceRptDateFrom.getText().toString().trim() + " " + btSelectTimeFrom.getText().toString();
                    String datetimeto = btTrackandTraceRptDateTo.getText().toString().trim() + " " + btSelctTimeTo.getText().toString();

                    String unitno = vehicleDetails[2];
                    Intent i = new Intent(getApplicationContext(), TrackAndTrace.class);
                    i.putExtra("vehicleno", vehicleno.toString());
                    i.putExtra("vehicleid", vehicleid.toString());
                    i.putExtra("unitno", unitno.toString());
                    i.putExtra("fromdate", datetimefrom.toString());
                    i.putExtra("todate", datetimeto.toString());
                    i.putExtra("fromTime", btSelectTimeFrom.getText().toString());
                    i.putExtra("toTime", btSelctTimeTo.getText().toString());

                    startActivity(i);
                    overridePendingTransition(R.anim.right_in, R.anim.left_out);

                }
            }
        });
    }

    private void myfilter(CharSequence cs) {

        cs = cs.toString().toLowerCase();

        f = new ArrayList<String>();

        if (cs != null && cs.toString().length() > 0) {
            for (int i = 0; i < vehicleName.length; i++) {
                String product = vehicleName[i];
                if (product.toLowerCase().contains(cs))

                    f.add(product);
            }
        }
        listShow();
    }

    private void allInitialization() {

        etVehicleNo = findViewById(R.id.etTrackandTraceReportVehicleNo);
        lvVehicleNo = findViewById(R.id.lstTrackandTraceReport);
        btTrackandTraceRptDateTo = findViewById(R.id.btTrackandTraceReportDateTo);
        btTrackandTraceRptDateFrom = findViewById(R.id.btTrackandTraceReportDateFrom);
        btTrackandTraceRptSubmit = findViewById(R.id.btTrackandTraceReportSubmit);
        linearLayoutTraceandTraceRptFour = findViewById(R.id.linearLayoutTrackandTraceReportFour);
        linearLayoutTrackandTraceReport = findViewById(R.id.linearLayoutTrackandTraceReport);
        btSelctTimeTo = findViewById(R.id.btSearchReportTimeto);
        btSelectTimeFrom = findViewById(R.id.btSearchReportTimeFrom);
        _clsInternetConDetect = new InternetConnectionDetector(getApplicationContext());
        durationSpinner = findViewById(R.id.durationSpinner);
    }

    public void bindglobalarray(String[] searchArray) {

        barProgressDialog.dismiss();
        String vehicleno;
        vehicleNamecomplete = searchArray;
        vehicleName = new String[vehicleNamecomplete.length];

        for (int i = 0; i <= vehicleNamecomplete.length - 1; i++) {
            vehicleno = vehicleNamecomplete[i].substring(0, vehicleNamecomplete[i].indexOf(","));
            vehicleName[i] = vehicleno;
        }
        listShow();
    }

    public void setDuration_Time() {
        Time today = new Time(Time.getCurrentTimezone());
        today.setToNow();

        btSelectTimeFrom.setText("00" + ":" + "01" + " ");
        btSelctTimeTo.setText(today.format("%k:%M"));
    }

    public void setFrom_To_Date(String duration) {
        if (duration.equals("Today")) {
            Calendar calendar = Calendar.getInstance();
            //calendar.add(Calendar.DATE, -20);
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
            String dateString = dateFormat.format(calendar.getTime());

            btTrackandTraceRptDateFrom.setText(dateString);
            btTrackandTraceRptDateTo.setText(dateString);

        } else if (duration.equals("Day")) {
            Time today = new Time(Time.getCurrentTimezone());
            today.setToNow();
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, -1);
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
            String dateString = dateFormat.format(calendar.getTime());
            String currentDate = String.valueOf(today.monthDay + "-" + (today.month + 1) + "-" + today.year);

            btTrackandTraceRptDateFrom.setText(dateString);
            btTrackandTraceRptDateTo.setText(currentDate);
        } else if (duration.equals("Week")) {
            Time today = new Time(Time.getCurrentTimezone());
            today.setToNow();
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, -7);
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
            String dateString = dateFormat.format(calendar.getTime());
            String currentDate = String.valueOf(today.monthDay + "-" + (today.month + 1) + "-" + today.year);

            btTrackandTraceRptDateFrom.setText(dateString);
            btTrackandTraceRptDateTo.setText(currentDate);
        } else if (duration.equals("Month")) {
            Time today = new Time(Time.getCurrentTimezone());
            today.setToNow();
            int month;
            int daysInMonth;
            Calendar calendar = Calendar.getInstance();
            boolean isLeapYear = ((Calendar.YEAR % 4 == 0) && (Calendar.YEAR % 100 != 0) || (Calendar.YEAR % 400 == 0));
            month = calendar.get(Calendar.MONTH);
            month = month + 1;
            String currentDate = String.valueOf(today.monthDay + "-" + (today.month + 1) + "-" + today.year);

            if (month == 4 || month == 6 || month == 9 || month == 11) {
                daysInMonth = 30;
            } else if (month == 2) {

                daysInMonth = (isLeapYear) ? 29 : 28;
            } else
                daysInMonth = 31;

            calendar.add(Calendar.DATE, -daysInMonth);
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
            String dateString = dateFormat.format(calendar.getTime());

            btTrackandTraceRptDateFrom.setText(dateString);
            btTrackandTraceRptDateTo.setText(currentDate);
        }
    }

    public void onBackPressed() {
        if (lvVehicleNo.getVisibility() == View.VISIBLE) {
            lvVehicleNo.setVisibility(View.GONE);
        } else {
            finish();
            overridePendingTransition(R.anim.left_in, R.anim.right_out);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == btTrackandTraceRptDateFrom) {
            // Process to get Current Date
            btTrackandTraceRptDateFrom.setError(null);
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            // Launch Date Picker Dialog
            DatePickerDialog dpd = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            // Display Selected date in textbox
                            btTrackandTraceRptDateFrom.setText(year + "-"
                                    + (monthOfYear + 1) + "-" + dayOfMonth);

                            String sButtonDateFrom = btTrackandTraceRptDateFrom.getText().toString();

                            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                            data = dateFormat.format(c.getTime());

                            try {
                                datecurrent = dateFormat.parse(data.toString().trim());
                                dateButtonFrom = dateFormat.parse(sButtonDateFrom.toString().trim());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                    }, mYear, mMonth, mDay);

            dpd.show();
        }

        if (v == btTrackandTraceRptDateTo) {
            // Process to get Current Date
            btTrackandTraceRptDateTo.setError(null);
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            // Launch Date Picker Dialog
            DatePickerDialog dpd = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            // Display Selected date in textbox
                            btTrackandTraceRptDateTo.setText(year + "-"
                                    + (monthOfYear + 1) + "-" + dayOfMonth);

                            String sButtonDateTo = btTrackandTraceRptDateTo.getText().toString();

                            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                            data = dateFormat.format(c.getTime());

                            try {
                                datecurrent = dateFormat.parse(data.toString().trim());
                                dateButtonTo = dateFormat.parse(sButtonDateTo.toString().trim());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                    }, mYear, mMonth, mDay);

            dpd.show();
        }

        if (v == btSelctTimeTo) {
            final Calendar calendar = Calendar.getInstance();
            intHour = calendar.get(Calendar.HOUR_OF_DAY);
            intMinute = calendar.get(Calendar.MINUTE);
            TimePickerDialog tpd = new TimePickerDialog(this,
                    new TimePickerDialog.OnTimeSetListener() {
                        public void onTimeSet(TimePicker view, int hour,
                                              int minute) {
                            if (hour == 00) {
                                hour += 00;
//                                stringTimeZone = "AM";
                            }
                            /*else if (hour == 12) {
                                stringTimeZone = "PM";
                            } else if (hour > 12) {
                                hour -= 12;
                                stringTimeZone = "PM";
                            } else {
                                stringTimeZone = "AM";
                            }*/
                            btSelctTimeTo.setText(hour + ":" + minute + " ");
                        }
                    }, intHour, intMinute, false);
            tpd.show();
        }

        if (v == btSelectTimeFrom) {
            final Calendar calendar = Calendar.getInstance();
            intHour = calendar.get(Calendar.HOUR);
            intMinute = calendar.get(Calendar.MINUTE);
            TimePickerDialog tpd = new TimePickerDialog(this,
                    new TimePickerDialog.OnTimeSetListener() {
                        public void onTimeSet(TimePicker view, int hour,
                                              int minute) {
                            if (hour == 0) {
                                hour += 00;
//                                stringTimeZone = "AM";
                            }
                          /*  else if (hour == 12) {
                                stringTimeZone = "PM";
                            } else if (hour > 12) {
                                hour -= 12;
                                stringTimeZone = "PM";
                            } else {
                                stringTimeZone = "AM";
                            }*/
                            btSelectTimeFrom.setText(hour + ":" + minute + " ");
                        }
                    }, intHour, intMinute, false);
            tpd.show();
        }
    }
}
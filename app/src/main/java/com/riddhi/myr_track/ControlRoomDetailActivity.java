package com.riddhi.myr_track;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import Fragment.VehicleFragment;
import model.vehiclelist;
import services.AsyncControlRoomDetailVehicleList;
import services.ListVehicleAsyncNew;
import support.ControlRoomDetailAdapter;
import support.CustomVehicleAdapter;

public class ControlRoomDetailActivity extends AppCompatActivity {

    private ControlRoomDetailAdapter adapter;
    private ListView listView;
    public List<vehiclelist> myvehiclelist = new ArrayList<vehiclelist>();
    Context context;
    RelativeLayout center_relative;
    private TextView textView3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control_room_detail);
        context = this;
        String customFont = "fonts/arlrdbd.ttf";
        Typeface typeface = Typeface.createFromAsset(this.getAssets(), customFont);
        textView3 = (TextView) findViewById(R.id.textView3);
        textView3.setTypeface(typeface);

        center_relative = (RelativeLayout) findViewById(R.id.center_relative);
        listView = (ListView) findViewById(R.id.vehicle_recycler_view);
        String Type = "",TextStr = "";
        Intent intent = getIntent();
        Type = intent.getStringExtra("Type");
        Log.e("ControlRoomDetail","==>"+Type);
        TextStr = intent.getStringExtra("Text");
        Log.e("ControlRoomDetail","==>"+TextStr);
        textView3.setText(""+TextStr);
        populatevehiclelist(Type);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Intent intent = new Intent(this,);
            }
        });
    }

    public void populatevehiclelistview(List<vehiclelist> VList)
    {
        center_relative.setVisibility(View.GONE);
        if(VList.size()>0)
        {
            adapter = new ControlRoomDetailAdapter((ArrayList<vehiclelist>) VList, context);
            listView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
        else
        {
            Toast.makeText(ControlRoomDetailActivity.this,"Data Not Found",Toast.LENGTH_LONG).show();
        }
    }

    private void populatevehiclelist(String type)
    {
        Log.e("myvehiclelist size","==>"+myvehiclelist.size());
        myvehiclelist.clear();
        AsyncControlRoomDetailVehicleList lvAsysnc = new AsyncControlRoomDetailVehicleList();
        lvAsysnc.execute(Login.companyid,Login.userid,type, ControlRoomDetailActivity.this);
    }
}
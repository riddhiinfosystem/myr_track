package services;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.riddhi.myr_track.Login;
import com.riddhi.myr_track.StatusReport;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Vinayak on 11/30/2016.
 */

public class StatusReportAsync extends AsyncTask {

    StatusReport report;
    String effectiveDate;
    Context context;

    @Override
    protected Object doInBackground(Object[] params) {

        effectiveDate = (String) params[0];
        report = (StatusReport) params[1];
        context = (Context) params[2];

        Uri uri;
        HttpHandler service = new HttpHandler();

        uri = new Uri.Builder()
                .scheme("http")
                .authority("rtracksystem.co.in")
                .path("Android/FrmVehicleStatusChart.aspx")
                .appendQueryParameter("EffectiveDate", String.valueOf(effectiveDate))
                .appendQueryParameter("CompanyID", String.valueOf(Login.companyid))
                .build();

        String response = service.makeServiceCall(uri.toString());
        System.out.println("Response--- " + response);

        if (response != null) {
            try {
                JSONArray vehiclesReportArray = null;
                JSONObject jsonObj = new JSONObject(response);
                vehiclesReportArray = jsonObj.getJSONArray("Vehicles");
                JSONObject obj;

                for (int i = 0; i < vehiclesReportArray.length(); i++) {
                    obj = vehiclesReportArray.getJSONObject(i);
                    report.nameList.add(obj.getString("Status"));
                    report.valueList.add(obj.getInt("VehicleCount"));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            report.reportLayout.setVisibility(View.VISIBLE);
            Log.e("HTTPHandler ", "couldn't get any data from the url");
            Toast.makeText(context, "No status report available there", Toast.LENGTH_SHORT).show();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Object o) {

        report.progressBar.setVisibility(View.GONE);
        //report.reportLayout.setVisibility(View.GONE);

        if (report.nameList.size() > 0 && report.valueList.size() > 0) {
            report.showPieChart(report);
        } else {
            Toast.makeText(context, "No status report available there", Toast.LENGTH_SHORT).show();
            report.reportLayout.setVisibility(View.VISIBLE);
            report.dateET.setText(" Click To Select Date");
        }
    }
}

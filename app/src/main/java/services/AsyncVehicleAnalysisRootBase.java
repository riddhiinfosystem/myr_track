package services;

import android.net.Uri;
import android.os.AsyncTask;

import com.riddhi.myr_track.DailyAnalysisRootBaseReportList;
import com.riddhi.myr_track.Login;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import model.Searchlist;

public class AsyncVehicleAnalysisRootBase extends AsyncTask {
    String vehicleid;
    String unitid;
    String datefrom;
    String dateto;
    String[] searchArray;
    DailyAnalysisRootBaseReportList searchReportList;
    List<Searchlist> _mysearchlist;
    ArrayList<String> vehiclenolist;
    ArrayList<String> vehicleIdlist;
    ArrayList<String> unitnolist;
    JSONArray vehicles = new JSONArray();
    int condition = 0;
    String fromTime, toTime;

    protected Object doInBackground(Object[] params) {
        vehicleid = String.valueOf(params[0].toString());
        unitid = String.valueOf(params[1].toString());
        datefrom = String.valueOf(params[2].toString());
        dateto = String.valueOf(params[3].toString());
        _mysearchlist = (List) params[4];
        searchReportList = (DailyAnalysisRootBaseReportList) params[5];


        Uri uri;
        if (vehicleid != null) {
            HttpHandler service = new HttpHandler();

            if (Login.server == 50) {

                uri = new Uri.Builder()
                        .scheme("http")
                        .authority("rtracksystem.co.in")
                        .path("android/FrmVehicleAnalysisRootBase.aspx")
                        .appendQueryParameter("VehicleID", String.valueOf(vehicleid))
                        .appendQueryParameter("FromDate", String.valueOf(datefrom))
                        .appendQueryParameter("ToDate", String.valueOf(dateto))
                        .appendQueryParameter("CompanyID", String.valueOf(Login.companyid))
                        .appendQueryParameter("UserID", String.valueOf(Login.userid))
                        .build();
            } else {
                uri = new Uri.Builder()
                        .scheme("http")
                        .authority("rtracksystem.co.in")
                        .path("android/FrmVehicleAnalysisRootBase.aspx")
                        .appendQueryParameter("VehicleID", String.valueOf(vehicleid))
                        .appendQueryParameter("FromDate", String.valueOf(datefrom))
                        .appendQueryParameter("ToDate", String.valueOf(dateto))
                        .appendQueryParameter("CompanyID", String.valueOf(Login.companyid))
                        .appendQueryParameter("UserID", String.valueOf(Login.userid))
                        .build();
            }

            String response = service.makeServiceCall(uri.toString());
            response = android.text.Html.fromHtml(response).toString();
            if (response != null) {
                try {
                    condition = 1;
                    int srCount = 0;
                    JSONObject jsonObj = new JSONObject(response);
                    vehicles = jsonObj.getJSONArray("Vehicles");
                    JSONObject c;
                    String _vehicleno, _ActivityType, _FromDate, _ToDate, _Time, _StartLocation, _EndLocation, _Kms;

                    vehicleIdlist = new ArrayList<String>();
                    vehiclenolist = new ArrayList<String>();
                    unitnolist = new ArrayList<String>();

                    searchArray = new String[vehicles.length()];
                    for (int i = 0; i < vehicles.length(); i++) {
                        c = vehicles.getJSONObject(i);

                        _vehicleno = c.getString("Vehicle No");
                        _ActivityType = c.getString("Activity Type ");
                        _FromDate = c.getString("From Date");
                        _ToDate = c.getString("To Date");
                        _Time = c.getString("Time");
                        _StartLocation = c.getString("Start Location");
                        _EndLocation = c.getString("End Location");
                        _Kms = c.getString("Kms");

                        _mysearchlist.add(new Searchlist(_vehicleno, _ActivityType, _FromDate, _ToDate, _Time, _StartLocation, _EndLocation, _Kms));

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if (response == null) {
                condition = 2;
            } else {
                if (vehicles.length() == 0) {
                    condition = 3;
                }
            }
        }
        return null;
    }

    protected void onPostExecute(Object result) {
        if (condition == 1) {
            searchReportList.populatevehiclelistview(_mysearchlist);
        }
        if (condition == 2) {
            searchReportList.setNullError();
        }
        if (condition == 3) {
            searchReportList.setNoDataFoundError();
        }
    }
}
package services;

import android.net.Uri;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ProgressBar;

import com.riddhi.myr_track.Login;
import com.riddhi.myr_track.MarkLocationNew;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by abc on 4/5/2015.
 */
public class AsyncMarkLocGetAddress extends AsyncTask {

    MarkLocationNew gpsLocation;
    int condition = 0;
    JSONArray vehicles = new JSONArray();
    String[] stringsLocation;
    ProgressBar pbMarkLocGetAddresss;
    private Double _asyncMarkLocGetAddLat;
    private Double _asyncMarkLocGetAddLong;

    protected Object doInBackground(Object[] params) {

        gpsLocation = (MarkLocationNew) params[0];
        _asyncMarkLocGetAddLat = Double.valueOf(params[1].toString());
        _asyncMarkLocGetAddLong = Double.valueOf(params[2].toString());
        pbMarkLocGetAddresss = (ProgressBar) params[3];

        Uri uri;
        HttpHandler service = new HttpHandler();

        if (Login.server == 50) {
            uri = new Uri.Builder()
                    .scheme("http")
                    .authority("rtracksystem.co.in")
                    .path("android/FrmLatLngwiseLocation.aspx")
                    .appendQueryParameter("Lat", String.valueOf(_asyncMarkLocGetAddLat))
                    .appendQueryParameter("Lng", String.valueOf(_asyncMarkLocGetAddLong))
                    .appendQueryParameter("CompanyID", String.valueOf(Login.companyid))
                    .build();
        } else {
            uri = new Uri.Builder()
                    .scheme("http")
                    .authority("rtracksystem.co.in")
                    .path("android/FrmLatLngwiseLocation.aspx")
                    .appendQueryParameter("Lat", String.valueOf(_asyncMarkLocGetAddLat))
                    .appendQueryParameter("Lng", String.valueOf(_asyncMarkLocGetAddLong))
                    .appendQueryParameter("CompanyID", String.valueOf(Login.companyid))
                    .build();
        }
        String response = service.makeServiceCall(uri.toString());


        if (response != null) {
            try {
                condition = 1;

                JSONObject jsonObj = new JSONObject(response);
                vehicles = jsonObj.getJSONArray("Vehicles");
                JSONObject c;
                String location = "";

                stringsLocation = new String[vehicles.length()];

                for (int i = 0; i < vehicles.length(); i++) {
                    c = vehicles.getJSONObject(i);
                    location = c.getString("Location");

                    stringsLocation[i] = location;
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (response == null) {
            condition = 2;
        } else {
            if (vehicles.length() == 0) {
                condition = 3;
            }
        }

        return null;
    }

    protected void onPostExecute(Object result) {

        pbMarkLocGetAddresss.setVisibility(View.GONE);

        if (condition == 1) {
            gpsLocation.showLocationName(stringsLocation);
        }
        if (condition == 2) {
            gpsLocation.setNullError();
        }
        if (condition == 3) {
            gpsLocation.setNoDataFoundError();
        }
    }
}
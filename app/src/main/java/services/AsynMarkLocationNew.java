package services;

import android.net.Uri;
import android.os.AsyncTask;

import com.riddhi.myr_track.GpsLocationTwo;
import com.riddhi.myr_track.Login;
import com.riddhi.myr_track.MarkLocationNew;
import com.riddhi.myr_track.SpinnerList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by abc on 3/30/2015.
 */
public class AsynMarkLocationNew extends AsyncTask {

    MarkLocationNew gpsLocation;
    String locationTypeId, locationType;

    int condition = 0;


    SpinnerList spinnerList;

    JSONArray vehicles = new JSONArray();
    String[] stringsLocationTypeId;
    String[] stringsLocationType;
    List<SpinnerList> spinnerListList;
    String asyncLocType = String.valueOf(GpsLocationTwo.IsLocType);

    protected Object doInBackground(Object[] params) {

        gpsLocation = (MarkLocationNew) params[0];
        spinnerListList = (List) params[1];

        Uri uri;
        HttpHandler service = new HttpHandler();

        if (Login.server == 50) {
            uri = new Uri.Builder()
                    .scheme("http")
                    .authority("rtracksystem.co.in")
                    .path("android/FrmMarkLocation.aspx")
                    .appendQueryParameter("IsLocType", asyncLocType)

                    .build();
        } else {
            uri = new Uri.Builder()
                    .scheme("http")
                    .authority("rtracksystem.co.in")
                    .path("android/FrmMarkLocation.aspx")
                    .appendQueryParameter("IsLocType", asyncLocType)
                    .build();
        }
        String response = service.makeServiceCall(uri.toString());


        if (response != null) {
            try {
                condition = 1;

                JSONObject jsonObj = new JSONObject(response);
                vehicles = jsonObj.getJSONArray("Vehicles");
                JSONObject c;

                stringsLocationTypeId = new String[vehicles.length()];
                stringsLocationType = new String[vehicles.length()];


                for (int i = 0; i < vehicles.length(); i++) {
                    c = vehicles.getJSONObject(i);
                    locationTypeId = c.getString("Location_Type_ID");
                    locationType = c.getString("Location_Type");

                    stringsLocationTypeId[i] = locationTypeId;
                    stringsLocationType[i] = locationType;
                    int locid = Integer.parseInt(locationTypeId);
                    spinnerListList.add(new SpinnerList(locid, locationType));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (response == null) {
            condition = 2;
        } else {
            if (vehicles.length() == 0) {
                condition = 3;
            }
        }

        return null;
    }

    protected void onPostExecute(Object result) {

        if (condition == 1) {
            gpsLocation.locationArrayAsync(stringsLocationTypeId, stringsLocationType);

        }
        if (condition == 2) {
            gpsLocation.setNullError();
        }
        if (condition == 3) {
            gpsLocation.setNoDataFoundError();
        }
    }
}
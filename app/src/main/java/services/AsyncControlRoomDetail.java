package services;

import android.net.Uri;
import android.os.AsyncTask;
import android.widget.RelativeLayout;

import com.riddhi.myr_track.ControlRoomActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import Fragment.HomeFragment;

public class AsyncControlRoomDetail extends AsyncTask {

    RelativeLayout sp;
    Integer _companyId, _userid;
    String continue_stoppage_24_value, continue_stoppage_48_value, continue_stoppage_72_value,continue_driving_4_value, continue_driving_8_value, over_speed_value,night_driving_value, power_disconnect_value, inactive_vehicle_value;
    ControlRoomActivity homeFragment;
    int condition = 0;
    JSONArray controllingBranchList = new JSONArray();

    @Override
    protected Object doInBackground(Object[] params) {
        _companyId = Integer.valueOf(params[0].toString());
        _userid = Integer.valueOf(params[1].toString());
        sp = (RelativeLayout) params[2];
        homeFragment = (ControlRoomActivity) params[3];


        Uri uri;
        if (_companyId != 0) {
            HttpHandler service = new HttpHandler();
            uri = new Uri.Builder()
                    .scheme("http")
                    .authority("rtracksystem.co.in")
                    .path("android/FrmControlRoom.aspx")
                    .appendQueryParameter("CompanyID", String.valueOf(_companyId))
                    .appendQueryParameter("UserID", String.valueOf(_userid))
                    .build();

            String response = service.makeServiceCall(uri.toString());

            if (response != null) {
                try {
                    condition = 1;
                    JSONObject jsonObj = new JSONObject(response);

                    controllingBranchList = jsonObj.getJSONArray("JsonArray");
                    JSONObject c;
                    c = controllingBranchList.getJSONObject(0);

                        for (int k = 0; k < 1; k++) {

                                continue_stoppage_24_value = c.getString("Continue Stoppage(24 Hrs)");

                                continue_stoppage_48_value = c.getString("Continue Stoppage(48 Hrs)");

                                continue_stoppage_72_value = c.getString("Continue Stoppage(72 Hrs)");

                                continue_driving_4_value = c.getString("Continue Driving(4 Hrs)");

                                continue_driving_8_value = c.getString("Continue Driving(8 Hrs)");

                                over_speed_value = c.getString("Over Speed");

                                night_driving_value = c.getString("Night Driving(11PM to 5AM)");

                                power_disconnect_value = c.getString("Power Disconnect");

                                inactive_vehicle_value = c.getString("Inactive Vehicle");

                        }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (response == null) {
                condition = 2;
            } else {
                if (controllingBranchList.length() == 0) {
                    condition = 3;
                }
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object result) {

        if (condition == 1) {
            homeFragment.setDashboardDataOnView(continue_stoppage_24_value, continue_stoppage_48_value, continue_stoppage_72_value,continue_driving_4_value, continue_driving_8_value, over_speed_value,night_driving_value, power_disconnect_value, inactive_vehicle_value);

        }


    }
}
package services;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.riddhi.myr_track.VehicleNearByPlaceActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import model.NearByVehicleList;

/**
 * Created by Vinayak on 6/19/2018.
 */

public class AsyncNearByVehiclePlaceShowReport extends AsyncTask {

    Integer _companyId, _userid, vehicleUnitNo;
    String[] searchArray;
    VehicleNearByPlaceActivity vehicleNearByActivity;
    int condition = 0;
    String classPosition = "1";
    String tempLat = "", tempLong = "";
    JSONArray vehicles = new JSONArray();
    int Distance = 10;

    String[] vehicleArray;
    ArrayList<String> vehiclenolist;
    ArrayList<String> vehicleIdlist;
    ArrayList<String> unitnolist;
    List<NearByVehicleList> nearByVehicle = new ArrayList<NearByVehicleList>();

    protected Object doInBackground(Object[] params) {
        tempLat = params[0].toString();
        tempLong = params[1].toString();
        Distance = Integer.valueOf(params[2].toString());
        _userid = Integer.valueOf(params[3].toString());
        _companyId = Integer.valueOf(params[4].toString());
        vehicleNearByActivity = (VehicleNearByPlaceActivity) params[5];

        Uri uri;
        if (_companyId != 0) {
            HttpHandler service = new HttpHandler();

            uri = new Uri.Builder()
                    .scheme("http")
                    .authority("rtracksystem.co.in")
                    .path("android/FrmNearByVehiclePlaceShowReport.aspx")
                    .appendQueryParameter("Latitude", tempLat)
                    .appendQueryParameter("Logitude", tempLong)
                    .appendQueryParameter("Distance", String.valueOf(Distance))
                    .appendQueryParameter("UserId", String.valueOf(_userid))
                    .appendQueryParameter("CompanyID", String.valueOf(_companyId))
                    .build();

            String response = service.makeServiceCall(uri.toString());

            if (response != null) {
                try {
                    condition = 1;

                    JSONObject jsonObj = new JSONObject(response);
                    vehicles = jsonObj.getJSONArray("Vehicles");
                    JSONObject c;
                    String vehicleno = null, vehicleid = null, unitno = null;

                    vehicleIdlist = new ArrayList<String>();
                    vehiclenolist = new ArrayList<String>();
                    unitnolist = new ArrayList<String>();

                    searchArray = new String[vehicles.length()];
                    for (int i = 0; i < vehicles.length(); i++) {
                        c = vehicles.getJSONObject(i);

                        nearByVehicle.add(new NearByVehicleList("" + c.getString("Vehicle No"), "" + c.getString("Location"), "" + c.getString("Date Time"), "" + c.getString("Distance")));

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (response == null) {
                condition = 2;
            } else {
                if (vehicles.length() == 0) {
                    condition = 3;
                }
            }
        }
        return null;
    }


    protected void onPostExecute(Object result) {
        if (condition == 1) {
            Log.e("Data received", "successfully------>");
            vehicleNearByActivity.setNearByVehicleData(nearByVehicle);
        }
        if (condition == 2) {

            vehicleNearByActivity.setNullError();


        }
        if (condition == 3) {

            vehicleNearByActivity.setNoDataFoundError();


        }

    }
}


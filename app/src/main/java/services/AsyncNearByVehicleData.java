package services;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.riddhi.myr_track.VehicleNearByActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import model.NearByVehicleList;

/**
 * Created by Vinayak on 6/13/2018.
 */

public class AsyncNearByVehicleData extends AsyncTask {

    Integer _companyId, _userid, vehicleUnitNo;
    String[] searchArray;
    VehicleNearByActivity vehicleNearByActivity;
    int condition = 0;
    String classPosition = "1";
    JSONArray vehicles = new JSONArray();
    int Distance = 10;

    String[] vehicleArray;
    ArrayList<String> vehiclenolist;
    ArrayList<String> vehicleIdlist;
    ArrayList<String> unitnolist;
    List<NearByVehicleList> nearByVehicle = new ArrayList<NearByVehicleList>();

    protected Object doInBackground(Object[] params) {
        _companyId = Integer.valueOf(params[0].toString());
        vehicleUnitNo = Integer.valueOf(params[1].toString());
        vehicleNearByActivity = (VehicleNearByActivity) params[3];
        Distance = Integer.valueOf(params[2].toString());
        Uri uri;
        if (_companyId != 0) {
            HttpHandler service = new HttpHandler();
            uri = new Uri.Builder()
                    .scheme("http")
                    .authority("rtracksystem.co.in")
                    .path("android/FrmNearByVehicleReport.aspx")
                    .appendQueryParameter("CompanyID", String.valueOf(_companyId))
                    .appendQueryParameter("UnitNo", String.valueOf(vehicleUnitNo))
                    .appendQueryParameter("Distance", String.valueOf(Distance))

                    .build();


            String response = service.makeServiceCall(uri.toString());

            if (response != null) {
                try {
                    condition = 1;

                    JSONObject jsonObj = new JSONObject(response);
                    vehicles = jsonObj.getJSONArray("Vehicles");
                    JSONObject c;
                    String vehicleno = null, vehicleid = null, unitno = null;

                    vehicleIdlist = new ArrayList<String>();
                    vehiclenolist = new ArrayList<String>();
                    unitnolist = new ArrayList<String>();

                    searchArray = new String[vehicles.length()];
                    for (int i = 0; i < vehicles.length(); i++) {
                        c = vehicles.getJSONObject(i);
                        /*vehicleno = c.getString("Vehicle_No");
                        vehicleid = c.getString("Vehicle_ID");
                        unitno = c.getString("UnitNo");*/

                        nearByVehicle.add(new NearByVehicleList("" + c.getString("Vehicle No"), "" + c.getString("Location"), "" + c.getString("Date Time"), "" + c.getString("Distance")));

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (response == null) {
                condition = 2;
            } else {
                if (vehicles.length() == 0) {
                    condition = 3;
                }
            }
        }
        return null;
    }


    protected void onPostExecute(Object result) {
        if (condition == 1) {
            Log.e("Data received", "successfully------>");
            vehicleNearByActivity.setNearByVehicleData(nearByVehicle);
        }
        if (condition == 2) {

            vehicleNearByActivity.setNullError();


        }
        if (condition == 3) {

            vehicleNearByActivity.setNoDataFoundError();


        }

    }
}


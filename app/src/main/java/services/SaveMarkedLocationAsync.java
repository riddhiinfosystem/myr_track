package services;

import android.net.Uri;
import android.os.AsyncTask;
import android.view.View;
import android.widget.Toast;

import com.riddhi.myr_track.Login;
import com.riddhi.myr_track.trackvehicle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Vinayak on 11/11/2016.
 */

public class SaveMarkedLocationAsync extends AsyncTask {

    String locationType;
    String locationName;
    trackvehicle _trackVehicle;
    String latitude, longitude;
    String message, msgid;
    String responceString;

    @Override
    protected Object doInBackground(Object[] params) {

        _trackVehicle = (trackvehicle) params[0];
        locationType = (String) params[1];
        locationName = (String) params[2];
        latitude = (String) params[3];
        longitude = (String) params[4];

        Uri uri;
        HttpHandler service = new HttpHandler();

        if (Login.server == 50) {
            uri = new Uri.Builder()
                    .scheme("http")
//                        .authority("track.rtracksystem.com")
                    .authority("rtracksystem.co.in")
                    .path("android/FrmMarkLocation.aspx")
                    .appendQueryParameter("PlaceName", locationName)
                    .appendQueryParameter("Latitude", latitude)
                    .appendQueryParameter("Longitude", longitude)
                    .appendQueryParameter("City", "")
                    .appendQueryParameter("State", "")
                    .appendQueryParameter("LocationTypeID", String.valueOf(locationType))
                    .appendQueryParameter("CompanyID", String.valueOf(Login.companyid))
                    .appendQueryParameter("UserID", String.valueOf(Login.userid))
                    .build();
        } else {
            uri = new Uri.Builder()
                    .scheme("http")
//                        .authority("track.rtracksystem.com")
                    .authority("rtracksystem.co.in")
                    .path("android/FrmMarkLocation.aspx")
                    .appendQueryParameter("PlaceName", locationName)
                    .appendQueryParameter("Latitude", latitude)
                    .appendQueryParameter("Longitude", longitude)
                    .appendQueryParameter("City", "")
                    .appendQueryParameter("State", "")
                    .appendQueryParameter("LocationTypeID", String.valueOf(locationType))
                    .appendQueryParameter("CompanyID", String.valueOf(Login.companyid))
                    .appendQueryParameter("UserID", String.valueOf(Login.userid))
                    .build();
        }

        String response = service.makeServiceCall(uri.toString());
        responceString = response;

        if (response != null) {
            try {
                JSONArray vehicles = new JSONArray();
                JSONObject jsonObj = new JSONObject(response);
                vehicles = jsonObj.getJSONArray("Vehicles");
                JSONObject c;

                for (int i = 0; i < vehicles.length(); i++) {
                    c = vehicles.getJSONObject(i);
                    message = c.getString("Message");
                    msgid = c.getString("Msg_ID");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    @Override
    protected void onPostExecute(Object o) {

        if (responceString == null) {
            System.out.println("Responce--- " + responceString);
            System.out.println("Location can not be marked");
        } else {
            _trackVehicle.progressBar.setVisibility(View.INVISIBLE);
            if (msgid.equals("0")) {
                Toast.makeText(_trackVehicle, "Location marked successfully", Toast.LENGTH_SHORT).show();
                System.out.println("Location Type -" + locationType);
                System.out.println("Location marked successfully");
            } else {
                Toast.makeText(_trackVehicle, message, Toast.LENGTH_SHORT).show();
                System.out.println("Location can not be marked");
            }
        }
    }
}

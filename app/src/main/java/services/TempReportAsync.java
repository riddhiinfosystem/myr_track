package services;

import android.net.Uri;
import android.os.AsyncTask;
import android.view.View;
import android.widget.LinearLayout;

import com.riddhi.myr_track.Login;
import com.riddhi.myr_track.TempratureReport;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by abc on 1/5/2015.
 */
public class TempReportAsync extends AsyncTask {


    Integer _companyId, _userid;
    String[] searchArray;
    TempratureReport tempratureReport;
    LinearLayout _LinearLayout;
    int condition = 0;
    JSONArray vehicles = new JSONArray();

    String[] vehicleArray;
    ArrayList<String> vehiclenolist;
    ArrayList<String> vehicleIdlist;
    ArrayList<String> unitnolist;

    protected Object doInBackground(Object[] params) {
        _companyId = Integer.valueOf(params[0].toString());
        _userid = Integer.valueOf(params[1].toString());
        tempratureReport = (TempratureReport) params[2];
        _LinearLayout = (LinearLayout) params[3];

        Uri uri;
        if (_companyId != 0) {
            HttpHandler service = new HttpHandler();

            if (Login.server == 50) {

                uri = new Uri.Builder()
                        .scheme("http")
                        .authority("rtracksystem.co.in")
//                    .authority("track.rtracksystem.com")
                        .path("android/frmvehiclelist.aspx")
                        .appendQueryParameter("CompanyID", String.valueOf(_companyId))
                        .appendQueryParameter("UserID", String.valueOf(_userid))
                        .build();
            } else {
                uri = new Uri.Builder()
                        .scheme("http")
                        .authority("rtracksystem.co.in")
//                    .authority("track.rtracksystem.com")
                        .path("android/frmvehiclelist.aspx")
                        .appendQueryParameter("CompanyID", String.valueOf(_companyId))
                        .appendQueryParameter("UserID", String.valueOf(_userid))
                        .build();

            }

            String response = service.makeServiceCall(uri.toString());

            if (response != null) {
                try {

                    condition = 1;

                    JSONObject jsonObj = new JSONObject(response);
                    vehicles = jsonObj.getJSONArray("Vehicles");
                    JSONObject c;
                    String vehicleno = null, vehicleid = null, unitno = null;

                    vehicleIdlist = new ArrayList<String>();
                    vehiclenolist = new ArrayList<String>();
                    unitnolist = new ArrayList<String>();

//                    JSONObject c = vehicles.getJSONObject(0);
//                    String vehicleNo = c.getString("Vehicle No");
//                    Log.w("txt", vehicleNo);
                    searchArray = new String[vehicles.length()];
                    for (int i = 0; i < vehicles.length(); i++) {
                        c = vehicles.getJSONObject(i);
                        vehicleno = c.getString("Vehicle_No");
                        vehicleid = c.getString("Vehicle_ID");
                        unitno = c.getString("UnitNo");

                        searchArray[i] = vehicleno + "," + vehicleid + "," + unitno;

//                        _myreportlist.add(new reportlist(vehicleno,vehicleid,unitno));


                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (response == null) {
                condition = 2;
            } else {
                if (vehicles.length() == 0) {
                    condition = 3;
                }

            }

        }
        return null;
    }


    protected void onPostExecute(Object result) {
        //       pr.setVisibility(View.GONE);
        _LinearLayout.setVisibility(View.VISIBLE);


        if (condition == 1) {
            tempratureReport.bindglobalarray(searchArray);

        }
        if (condition == 2) {
            tempratureReport.setNullError();
        }
        if (condition == 3) {
            tempratureReport.setNoDataFoundError();
        }


//        _reportvehiclenew.populateReportVehicle(_myreportlist);

    }
}

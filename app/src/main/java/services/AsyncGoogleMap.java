package services;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.riddhi.myr_track.Login;
import com.riddhi.myr_track.trackvehicle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by abc on 1/19/2015.
 */
public class AsyncGoogleMap extends AsyncTask {

    String unitNo;
    Integer _companyId, _userid;
    String[] vehicleNoArray;
    String[] dateArray;
    String[] latitudeArray;
    String[] logitudeArray;
    String[] locationArray;
    String[] speedArray;
    trackvehicle trackvehicle1;

    protected Object doInBackground(Object[] params) {
        _companyId = Integer.valueOf(params[0].toString());
        unitNo = String.valueOf(params[1]);
        trackvehicle1 = (trackvehicle) params[2];

        Uri uri;
        if (_companyId != 0) {
            HttpHandler service = new HttpHandler();

            if (Login.server == 50) {

                uri = new Uri.Builder()
                        .scheme("http")
                        .authority("rtracksystem.co.in")
//                    .authority("track.rtracksystem.com")
                        .path("android/FrmVehicleLastLocation.aspx")
                        .appendQueryParameter("CompanyID", String.valueOf(_companyId))
                        .appendQueryParameter("UnitNo", String.valueOf(unitNo))
                        .build();
            } else {
                uri = new Uri.Builder()
                        .scheme("http")
                        .authority("rtracksystem.co.in")
                        .path("android/FrmVehicleLastLocation.aspx")
                        .appendQueryParameter("CompanyID", String.valueOf(_companyId))
                        .appendQueryParameter("UnitNo", String.valueOf(unitNo))
                        .appendQueryParameter("UserID", String.valueOf(_userid))
                        .build();

            }

            String response = service.makeServiceCall(uri.toString());

            if (response != null) {
                try {
                    JSONArray vehicles = null;
                    JSONObject jsonObj = new JSONObject(response);
                    vehicles = jsonObj.getJSONArray("Vehicles");
                    JSONObject c;
                    String vehicleNo, date, latitude, longitude, speed, location;


                    vehicleNoArray = new String[vehicles.length()];
                    dateArray = new String[vehicles.length()];
                    latitudeArray = new String[vehicles.length()];
                    logitudeArray = new String[vehicles.length()];
                    locationArray = new String[vehicles.length()];
                    speedArray = new String[vehicles.length()];
                    for (int i = 0; i < vehicles.length(); i++) {
                        c = vehicles.getJSONObject(i);
                        vehicleNo = c.getString("Vehicle_No");
                        date = c.getString("Date");
                        latitude = c.getString("latitude");
                        longitude = c.getString("longitude");
                        location = c.getString("Location");
                        speed = c.getString("Speed");
                        vehicleNoArray[i] = vehicleNo;
                        dateArray[i] = date;
                        latitudeArray[i] = latitude;
                        logitudeArray[i] = longitude;
                        locationArray[i] = location;
                        speedArray[i] = speed;

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("HTTPHandler", "Couldn't get any data from the url");
            }
        }
        return null;
    }


    protected void onPostExecute(Object result) {
        //       pr.setVisibility(View.GONE);
        //trackvehicle1.bindglobalarray(vehicleNoArray,dateArray,latitudeArray,logitudeArray,locationArray,speedArray);

//        _reportvehiclenew.populateReportVehicle(_myreportlist);
    }
}

package services;

import android.net.Uri;
import android.os.AsyncTask;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import model.VehicleStatus;

/**
 * Created by Vinayak on 6/1/2018.
 */

public class AsyncVehicleStatusDetailSave extends AsyncTask {

    Integer _companyId, _userid;
    String _status, _status_Id, _vehicleId, response;
    com.riddhi.myr_track.VehicleStatusActivity _lstVehicleNew;
    int condition = 0;
    List<VehicleStatus> _myvehiclesList;

    JSONArray controllingBranchList = new JSONArray();

    @Override
    protected Object doInBackground(Object[] params) {
        _status = params[0].toString();
        _status_Id = params[1].toString();
        _companyId = Integer.valueOf(params[2].toString());
        _userid = Integer.valueOf(params[3].toString());
        _vehicleId = params[4].toString();
        _lstVehicleNew = (com.riddhi.myr_track.VehicleStatusActivity) params[5];

        _myvehiclesList = new ArrayList<>();

        Uri uri;
        if (_companyId != 0) {
            HttpHandler service = new HttpHandler();
            uri = new Uri.Builder()
                    .scheme("http")
                    .authority("rtracksystem.co.in")
                    .path("android/FrmSaveVehicleStatusDetailNew.aspx")
                    .appendQueryParameter("Status", _status)
                    .appendQueryParameter("Status_Id", _status_Id)
                    .appendQueryParameter("CompanyID", String.valueOf(_companyId))
                    .appendQueryParameter("UserID", String.valueOf(_userid))
                    .appendQueryParameter("VehicleId", _vehicleId)
                    .build();

            response = service.makeServiceCall(uri.toString());

            condition = 1;


            if (response != null) {
                try {

                    JSONObject jsonObj = new JSONObject(response);

                    controllingBranchList = jsonObj.getJSONArray("Vehicles");

                    JSONObject c, d;
                    JSONArray j = new JSONArray();
                    String Controlling_Branch_ID, Vehicle_No, Status;
                    Boolean selected;
                    for (int i = 0; i < controllingBranchList.length(); i++) {
                        c = controllingBranchList.getJSONObject(i);

                        /*Controlling_Branch_ID = c.getString("Controlling_Branch_ID");
                        Vehicle_No = c.getString("Vehicle_No");
                        selected = false;
                        Status = "" + c.getString("Status");
                        if(Status.equalsIgnoreCase("none"))
                        {
                            Status = "";
                        }
                        _myvehiclesList.add(new model.VehicleStatus(Controlling_Branch_ID, Vehicle_No, Status, selected));*/

                        /*Controlling_Branch_ID = c.getString("Msg_ID");
                        if(Controlling_Branch_ID.equalsIgnoreCase("0"))
                        {

                        }*/
                        condition = 1;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object result) {
        //Log.e("onPostExecute","--->Success"+_myvehicles.size()+"--->"+searchArray.length);
        // _lstVehicleNew.
        // _lstVehicleNew.populatevehiclelistview(_myvehicles,);

        if (condition == 1) {
            //_lstVehicleNew.bindarray(_myvehiclesList);
            Toast.makeText(_lstVehicleNew,"Data Saved Successfully",Toast.LENGTH_LONG).show();
            _lstVehicleNew.bindarrayNew();
        }
        if (condition == 2) {
            //_lstVehicleNew.setNullError();
        }
        if (condition == 3) {
            //_lstVehicleNew.setNoDataFoundError();
        }

    }
}
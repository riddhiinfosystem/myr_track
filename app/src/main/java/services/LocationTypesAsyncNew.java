package services;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.riddhi.myr_track.Login;
import com.riddhi.myr_track.trackvehicle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import model.LocationTypeBean;

/**
 * Created by Vinayak on 11/9/2016.
 */

public class LocationTypesAsyncNew extends AsyncTask {

    String[] locationTypes;
    String[] locationTypesIds;
    ProgressBar progressBar;
    Activity activity;
    Context context;
    ArrayList<LocationTypeBean> locationTypeList;
    LocationTypeBean bean;
    trackvehicle _trackVehicle;
    ArrayList<String> locationTypesList;

    @Override
    protected Object doInBackground(Object[] params) {

        activity = (Activity) params[0];
        context = (Context) params[1];
        _trackVehicle = (trackvehicle) params[1];

        locationTypesList = new ArrayList<String>();
        Uri uri;
        HttpHandler service = new HttpHandler();

        if (Login.server == 50) {
            uri = new Uri.Builder()
                    .scheme("http")
                    .authority("rtracksystem.co.in")
                    .path("Android/FrmLocationType.aspx")
                    .build();
        } else {
            uri = new Uri.Builder()
                    .scheme("http")
                    .authority("rtracksystem.co.in")
                    .path("Android/FrmLocationType.aspx")
                    .build();
        }

        String response = service.makeServiceCall(uri.toString());

        if (response != null) {
            locationTypeList = new ArrayList<LocationTypeBean>();

            try {
                JSONArray locationTypeArray = null;
                JSONObject jsonObj = new JSONObject(response);
                locationTypeArray = jsonObj.getJSONArray("Vehicles");
                JSONObject obj;

                locationTypes = new String[locationTypeArray.length()];
                locationTypesIds = new String[locationTypeArray.length()];

                for (int i = 0; i < locationTypeArray.length(); i++) {
                    obj = locationTypeArray.getJSONObject(i);

                    bean = new LocationTypeBean();
                    bean.setLocationTypeId(obj.getInt("Location_Type_ID"));
                    bean.setLocationType(obj.getString("Location_Type"));

                    locationTypesList.add(obj.getString("Location_Type"));
                    locationTypes[i] = obj.getString("Location_Type");
                    locationTypesIds[i] = obj.getString("Location_Type_ID");

                    String location = obj.getString("Location_Type");
                    int id = Integer.parseInt(obj.getString("Location_Type_ID"));

                    locationTypeList.add(new LocationTypeBean(location, id));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Log.e("HTTPHandler", "Couldn't get any data from the url");
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        if (locationTypeList == null) {
            Toast.makeText(context, "There is no location types available", Toast.LENGTH_SHORT).show();
        } else {
            Log.e("Data ", "recieved...!!");
        }

        _trackVehicle.progressBar.setVisibility(View.GONE);
        locationTypesList.add(0, "Select Location Type");
        ArrayAdapter<String> locationAdapter = new ArrayAdapter<String>(_trackVehicle, android.R.layout.simple_spinner_item, locationTypesList);
        locationAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        _trackVehicle.locationTypeSpinner.setAdapter(locationAdapter);
    }
}

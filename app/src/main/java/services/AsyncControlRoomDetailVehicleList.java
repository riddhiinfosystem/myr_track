package services;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.riddhi.myr_track.ControlRoomDetailActivity;
import com.riddhi.myr_track.VehicleNearByActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import model.NearByVehicleList;
import model.vehiclelist;

public class AsyncControlRoomDetailVehicleList extends AsyncTask {

    Integer _companyId, _userid;
    ControlRoomDetailActivity vehicleNearByActivity;
    int condition = 0;
    String type = "";
    JSONArray vehicles = new JSONArray();

    List<vehiclelist> nearByVehicle = new ArrayList<vehiclelist>();

    protected Object doInBackground(Object[] params) {
        _companyId = Integer.valueOf(params[0].toString());
        _userid = Integer.valueOf(params[1].toString());
        type = params[2].toString();
        vehicleNearByActivity = (ControlRoomDetailActivity) params[3];

        Uri uri;
        if (_companyId != 0) {
            HttpHandler service = new HttpHandler();
            uri = new Uri.Builder()
                    .scheme("http")
                    .authority("rtracksystem.co.in")
                    .path("android/FrmControlRoomDetail.aspx")
                    .appendQueryParameter("CompanyID", String.valueOf(_companyId))
                    .appendQueryParameter("UserID", String.valueOf(_userid))
                    .appendQueryParameter("Type", type)
                    .build();


            String response = service.makeServiceCall(uri.toString());

            if (response != null) {
                try {
                    condition = 1;

                    JSONObject jsonObj = new JSONObject(response);
                    vehicles = jsonObj.getJSONArray("Vehicles");
                    JSONObject c;
                    String vehicleno = "", vehiclegroup = "", location = "", speed = "", batteryStatus = "", lastreported = "";

                    for (int i = 0; i < vehicles.length(); i++) {
                        c = vehicles.getJSONObject(i);
                        if(c.has("Vehicle No"))
                        {
                            vehicleno = c.getString("Vehicle No");
                        }
                        else if(c.has("VehicleNo1"))
                        {
                            vehicleno = c.getString("VehicleNo1");
                        }
                        else
                        {
                            vehicleno = "";
                        }

                        if(c.has("Vehicle Group"))
                        {
                            vehiclegroup = c.getString("Vehicle Group");
                        }
                        else if(c.has("VehicleGroup"))
                        {
                            vehiclegroup = c.getString("VehicleGroup");
                        }
                        else
                        {
                            vehiclegroup = "";
                        }

                        if(c.has("LastLocation"))
                        {
                            location = c.getString("LastLocation");
                        }
                        else if(c.has("Location"))
                        {
                            location = c.getString("Location");
                        }
                        else if(c.has("Place"))
                        {
                            location = c.getString("Place");
                        }
                        else
                        {
                            location = "";
                        }

                        if(c.has("speed"))
                        {
                            speed = c.getString("speed");
                        }
                        else
                        {
                            speed = "";
                        }

                        if(c.has("Date"))
                        {
                            lastreported = c.getString("Date");
                        }
                        else if(c.has("Last Reported Date"))
                        {
                            lastreported = c.getString("Last Reported Date");
                        }
                        else
                        {
                            lastreported = "";
                        }

                        if(c.has("Ext. Battery Volt"))
                        {
                            batteryStatus = c.getString("Ext. Battery Volt");
                        }
                        else
                        {
                            batteryStatus = "";
                        }

                        nearByVehicle.add(new vehiclelist(vehicleno, vehiclegroup, location, speed , batteryStatus , lastreported ));

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (response == null) {
                condition = 2;
            } else {
                if (vehicles.length() == 0) {
                    condition = 3;
                }
            }
        }
        return null;
    }


    protected void onPostExecute(Object result) {
        if (condition == 1) {
            Log.e("Data received", "successfully------>");
            vehicleNearByActivity.populatevehiclelistview(nearByVehicle);
        }

    }
}


package services;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.riddhi.myr_track.GpsLocationTwo;
import com.riddhi.myr_track.Login;
import com.riddhi.myr_track.SpinnerList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by abc on 2/13/2015.
 */
public class AsyncMarkLocation extends AsyncTask {

    RelativeLayout asyncRlayoutGpsLocMain;
    GpsLocationTwo gpsLocation;
    ProgressBar asncGpsLocPb;
    String locationType;
    String asyncLocation;
    String asyncLatitude;
    String asyncLongitude;
    String asyncState;
    String asyncCity;
    int asyncCompanyid;
    int asyncUserId;
    int asynclocationTypeId;


    String message;
    String msgid;

    SpinnerList spinnerList;

    String[] stringsLocationTypeId;
    String[] stringsLocationType;
    List<SpinnerList> spinnerListList;

    protected Object doInBackground(Object[] params) {


        asyncRlayoutGpsLocMain = (RelativeLayout) params[0];
        gpsLocation = (GpsLocationTwo) params[1];
        asncGpsLocPb = (ProgressBar) params[2];
        asyncLocation = String.valueOf(params[3]);
        asyncLatitude = String.valueOf(params[4]);
        asyncLongitude = String.valueOf(params[5]);
        asyncCity = String.valueOf(params[6]);
        asyncState = String.valueOf(params[7]);
        asynclocationTypeId = Integer.valueOf(params[8].toString());
        asyncCompanyid = Integer.valueOf(params[9].toString());
        asyncUserId = Integer.valueOf(params[10].toString());

        Uri uri;
        HttpHandler service = new HttpHandler();

        if (Login.server == 50) {
            uri = new Uri.Builder()
                    .scheme("http")
//                        .authority("track.rtracksystem.com")
                    .authority("rtracksystem.co.in")
                    .path("android/FrmMarkLocation.aspx")
                    .appendQueryParameter("PlaceName", asyncLocation)
                    .appendQueryParameter("Latitude", asyncLatitude)
                    .appendQueryParameter("Longitude", asyncLongitude)
                    .appendQueryParameter("City", asyncCity)
                    .appendQueryParameter("State", asyncState)
                    .appendQueryParameter("LocationTypeID", String.valueOf(asynclocationTypeId))
                    .appendQueryParameter("CompanyID", String.valueOf(asyncCompanyid))
                    .appendQueryParameter("UserID", String.valueOf(asyncUserId))

                    .build();
        } else {
            uri = new Uri.Builder()
                    .scheme("http")
//                        .authority("track.rtracksystem.com")
                    .authority("rtracksystem.co.in")
                    .path("android/FrmMarkLocation.aspx")
                    .appendQueryParameter("PlaceName", asyncLocation)
                    .appendQueryParameter("Latitude", asyncLatitude)
                    .appendQueryParameter("Longitude", asyncLongitude)
                    .appendQueryParameter("City", asyncCity)
                    .appendQueryParameter("State", asyncState)
                    .appendQueryParameter("LocationTypeID", String.valueOf(asynclocationTypeId))
                    .appendQueryParameter("CompanyID", String.valueOf(asyncCompanyid))
                    .appendQueryParameter("UserID", String.valueOf(asyncUserId))

                    .build();
        }
        String response = service.makeServiceCall(uri.toString());


        if (response != null) {
            try {
                JSONArray vehicles = null;
                JSONObject jsonObj = new JSONObject(response);
                vehicles = jsonObj.getJSONArray("Vehicles");
                JSONObject c;

                stringsLocationTypeId = new String[vehicles.length()];
                stringsLocationType = new String[vehicles.length()];


                for (int i = 0; i < vehicles.length(); i++) {
                    c = vehicles.getJSONObject(i);
                    message = c.getString("Message");
                    msgid = c.getString("Msg_ID");


                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Log.e("HTTPHandler", "Couldn't get any data from the url");
        }

        return null;
    }


    protected void onPostExecute(Object result) {
        //       pr.setVisibility(View.GONE);
        if (stringsLocationTypeId == null) {
            gpsLocation.nullException();
        }
        asncGpsLocPb.setVisibility(View.GONE);
        asyncRlayoutGpsLocMain.setVisibility(View.VISIBLE);
        gpsLocation.markLocationMessage(message, msgid);
    }
}

package services;

import android.net.Uri;
import android.os.AsyncTask;

import com.riddhi.myr_track.Login;
import com.riddhi.myr_track.SearchReportList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import model.Searchlist;

public class DateWiseReportDataAsync extends AsyncTask {
    String vehicleid;
    String unitid;
    String datefrom;
    String dateto;
    String[] searchArray;
    SearchReportList searchReportList;
    List<Searchlist> _mysearchlist;
    ArrayList<String> vehiclenolist;
    ArrayList<String> vehicleIdlist;
    ArrayList<String> unitnolist;
    JSONArray vehicles = new JSONArray();
    int condition = 0;
    String fromTime, toTime;

    protected Object doInBackground(Object[] params) {
        vehicleid = String.valueOf(params[0].toString());
        unitid = String.valueOf(params[1].toString());
        datefrom = String.valueOf(params[2].toString());
        dateto = String.valueOf(params[3].toString());
        _mysearchlist = (List) params[4];
        searchReportList = (SearchReportList) params[5];
        fromTime = String.valueOf(params[6]);
        toTime = String.valueOf(params[7]);

        searchReportList = (SearchReportList) params[5];
        Uri uri;
        if (vehicleid != null) {
            HttpHandler service = new HttpHandler();

            if (Login.server == 50) {

                uri = new Uri.Builder()
                        .scheme("http")
                        .authority("rtracksystem.co.in")
                        .path("android/FrmVehicleAnalysisReport.aspx")
                        .appendQueryParameter("VehicleID", vehicleid)
                        .appendQueryParameter("UnitNo", unitid)
                        .appendQueryParameter("FromDate", datefrom)
                        .appendQueryParameter("ToDate", dateto)
                        .appendQueryParameter("FromTime", fromTime)
                        .appendQueryParameter("ToTime", toTime)
                        .build();
            } else {
                uri = new Uri.Builder()
                        .scheme("http")

                        .authority("rtracksystem.co.in")
                        .path("android/FrmVehicleAnalysisReport.aspx")
                        .appendQueryParameter("VehicleID", String.valueOf(vehicleid))
                        .appendQueryParameter("UnitNo", String.valueOf(unitid))
                        .appendQueryParameter("FromDate", String.valueOf(datefrom))
                        .appendQueryParameter("ToDate", String.valueOf(dateto))
                        .appendQueryParameter("FromTime", fromTime)
                        .appendQueryParameter("ToTime", toTime)
                        .build();
            }

            String response = service.makeServiceCall(uri.toString());

            if (response != null) {
                try {
                    condition = 1;

                    JSONObject jsonObj = new JSONObject(response);
                    vehicles = jsonObj.getJSONArray("Vehicles");
                    JSONObject c;
                    String srno, vehicleno, date, totalrunningkms, totalrunningtime, totalstopagetime;

                    vehicleIdlist = new ArrayList<String>();
                    vehiclenolist = new ArrayList<String>();
                    unitnolist = new ArrayList<String>();

                    searchArray = new String[vehicles.length()];
                    for (int i = 0; i < vehicles.length(); i++) {
                        c = vehicles.getJSONObject(i);

                        srno = c.getString("SrNo");
                        vehicleno = c.getString("Vehicle_No");
                        date = c.getString("Date");
                        totalrunningkms = c.getString("TotalRunningKMS");
                        totalrunningtime = c.getString("TotalRunningTime");
                        totalstopagetime = c.getString("TotalStopageTime");

                        _mysearchlist.add(new Searchlist(srno, vehicleno, date, totalrunningkms, totalrunningtime, totalstopagetime));

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if (response == null) {
                condition = 2;
            } else {
                if (vehicles.length() == 0) {
                    condition = 3;
                }
            }
        }
        return null;
    }

    protected void onPostExecute(Object result) {
        if (condition == 1) {
            searchReportList.populatevehiclelistview(_mysearchlist);
        }
        if (condition == 2) {
            searchReportList.setNullError();
        }
        if (condition == 3) {
            searchReportList.setNoDataFoundError();
        }
    }
}
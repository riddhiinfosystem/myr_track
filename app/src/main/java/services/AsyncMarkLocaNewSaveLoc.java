package services;

import android.net.Uri;
import android.os.AsyncTask;

import com.riddhi.myr_track.Login;
import com.riddhi.myr_track.MarkLocationNew;
import com.riddhi.myr_track.SpinnerList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by abc on 4/2/2015.
 */
public class AsyncMarkLocaNewSaveLoc extends AsyncTask {

    MarkLocationNew gpsLocation;
    String locationType;
    String asyncLocation;
    String asyncLatitude;
    String asyncLongitude;
    String asyncState;
    String asyncCity;
    int asyncCompanyid;
    int asyncUserId;
    int asynclocationTypeId;
    int condition = 0;
    String message;
    String msgid;
    SpinnerList spinnerList;

    String[] stringsLocationTypeId;
    String[] stringsLocationType;
    List<SpinnerList> spinnerListList;
    JSONArray vehicles = new JSONArray();

    protected Object doInBackground(Object[] params) {

        gpsLocation = (MarkLocationNew) params[0];
        asyncLocation = String.valueOf(params[1]);
        asyncLatitude = String.valueOf(params[2]);
        asyncLongitude = String.valueOf(params[3]);
        asyncCity = String.valueOf(params[4]);
        asyncState = String.valueOf(params[5]);
        asynclocationTypeId = Integer.valueOf(params[6].toString());
        asyncCompanyid = Integer.valueOf(params[7].toString());
        asyncUserId = Integer.valueOf(params[8].toString());

        Uri uri;
        HttpHandler service = new HttpHandler();

        if (Login.server == 50) {
            uri = new Uri.Builder()
                    .scheme("http")
                    .authority("rtracksystem.co.in")
                    .path("android/FrmMarkLocation.aspx")
                    .appendQueryParameter("PlaceName", asyncLocation)
                    .appendQueryParameter("Latitude", asyncLatitude)
                    .appendQueryParameter("Longitude", asyncLongitude)
                    .appendQueryParameter("City", asyncCity)
                    .appendQueryParameter("State", asyncState)
                    .appendQueryParameter("LocationTypeID", String.valueOf(asynclocationTypeId))
                    .appendQueryParameter("CompanyID", String.valueOf(asyncCompanyid))
                    .appendQueryParameter("UserID", String.valueOf(asyncUserId))

                    .build();
        } else {
            uri = new Uri.Builder()
                    .scheme("http")
                    .authority("rtracksystem.co.in")
                    .path("android/FrmMarkLocation.aspx")
                    .appendQueryParameter("PlaceName", asyncLocation)
                    .appendQueryParameter("Latitude", asyncLatitude)
                    .appendQueryParameter("Longitude", asyncLongitude)
                    .appendQueryParameter("City", asyncCity)
                    .appendQueryParameter("State", asyncState)
                    .appendQueryParameter("LocationTypeID", String.valueOf(asynclocationTypeId))
                    .appendQueryParameter("CompanyID", String.valueOf(asyncCompanyid))
                    .appendQueryParameter("UserID", String.valueOf(asyncUserId))

                    .build();
        }
        String response = service.makeServiceCall(uri.toString());

        if (response != null) {
            try {
                condition = 1;

                JSONObject jsonObj = new JSONObject(response);
                vehicles = jsonObj.getJSONArray("Vehicles");
                JSONObject c;

                stringsLocationTypeId = new String[vehicles.length()];
                stringsLocationType = new String[vehicles.length()];

                for (int i = 0; i < vehicles.length(); i++) {
                    c = vehicles.getJSONObject(i);
                    message = c.getString("Message");
                    msgid = c.getString("Msg_ID");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (response == null) {
            condition = 2;
        } else {

            if (vehicles.length() == 0) {
                condition = 3;
            }
        }
        return null;
    }

    protected void onPostExecute(Object result) {

        if (condition == 1) {
            gpsLocation.markLocationMessage(message, msgid);
        }
        if (condition == 2) {
            gpsLocation.setNullError();
        }
        if (condition == 3) {
            gpsLocation.setNoDataFoundError();
        }
    }
}

package services;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.riddhi.myr_track.Login;
import com.riddhi.myr_track.VehicleDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vinayak on 4/28/2018.
 */

public class AsyncVehicleDetails extends AsyncTask {

    Integer _companyId, _userid;
    List<model.VehicleDetails> _myvehicles;
    VehicleDetails _lstVehicleNew;
    String[] searchArray;
    LinearLayout linearLayoutdailyvehana;
    int condition = 0;
    JSONArray vehicles = new JSONArray();

    @Override
    protected Object doInBackground(Object[] params) {
        _companyId = Integer.valueOf(params[0].toString());
        _userid = Integer.valueOf(params[1].toString());
        _myvehicles = new ArrayList<>();
        _lstVehicleNew = (VehicleDetails) params[2];
        linearLayoutdailyvehana = (LinearLayout) params[3];

        Uri uri;
        if (_companyId != 0) {
            HttpHandler service = new HttpHandler();

            if (Login.server == 50) {
                uri = new Uri.Builder()
                        .scheme("http")
                        .authority("rtracksystem.co.in")
                        .path("android/frmVehicleDetails.aspx")
                        .appendQueryParameter("CompanyID", String.valueOf(_companyId))
                        .appendQueryParameter("UserID", String.valueOf(_userid))
                        .build();
            } else {
                uri = new Uri.Builder()
                        .scheme("http")
                        .authority("rtracksystem.co.in")
                        .path("android/frmVehicleDetails.aspx")
                        .appendQueryParameter("CompanyID", String.valueOf(_companyId))
                        .appendQueryParameter("UserID", String.valueOf(_userid))
                        .build();
            }

            String response = service.makeServiceCall(uri.toString());

            if (response != null) {
                try {
                    condition = 1;
                    JSONObject jsonObj = new JSONObject(response);
                    vehicles = jsonObj.getJSONArray("Vehicles");
                    JSONObject c;

                    String Vehicle_No, UnitNo, utcToLocalDateTime, Installation_Date, Power_Satatus;
                    String Remarks, LastLocation, Vehicle_Controlling_Branch, Vehicle_Group, Vehicle_Type, VehicleWorking;
                    searchArray = new String[vehicles.length()];
                    for (int i = 0; i < vehicles.length(); i++) {
                        c = vehicles.getJSONObject(i);
                        Vehicle_No = "" + c.getString("Vehicle_No");
                        UnitNo = "" + c.getString("UnitNo");
                        utcToLocalDateTime = "" + c.getString("utcToLocalDateTime");
                        Installation_Date = "" + c.getString("Installation_Date");
                        Power_Satatus = "" + c.getString("Power_Satatus");
                        Remarks = "" + c.getString("Remarks");
                        LastLocation = "" + c.getString("LastLocation");
                        Vehicle_Controlling_Branch = "" + c.getString("Vehicle_Controlling_Branch");
                        Vehicle_Group = "" + c.getString("Vehicle_Group");
                        Vehicle_Type = "" + c.getString("Vehicle_Type");
                        VehicleWorking = "" + c.getString("VehicleWorking");

                        searchArray[i] = Vehicle_No;
                        _myvehicles.add(new model.VehicleDetails(Vehicle_No, UnitNo, utcToLocalDateTime, Installation_Date, Power_Satatus, Remarks, LastLocation, Vehicle_Controlling_Branch, Vehicle_Group, Vehicle_Type, VehicleWorking));

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (response == null) {
                condition = 2;
            } else {
                if (vehicles.length() == 0) {
                    condition = 3;
                }
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object result) {
        Log.e("onPostExecute", "--->Success" + _myvehicles.size() + "--->" + searchArray.length);
        // _lstVehicleNew.
        // _lstVehicleNew.populatevehiclelistview(_myvehicles,);

        linearLayoutdailyvehana.setVisibility(View.VISIBLE);

        if (condition == 1) {
            _lstVehicleNew.bindglobalarray(searchArray, _myvehicles);
        }
        if (condition == 2) {
            _lstVehicleNew.setNullError();
        }
        if (condition == 3) {
            _lstVehicleNew.setNoDataFoundError();
        }

    }
}
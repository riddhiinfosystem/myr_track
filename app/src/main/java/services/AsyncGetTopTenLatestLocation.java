package services;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.riddhi.myr_track.LiveTrackTraceSingleVehicleActivity;
import com.riddhi.myr_track.Login;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import Fragment.VehicleFragment;
import model.vehiclelist;

public class AsyncGetTopTenLatestLocation extends AsyncTask {

    String UnitNo;
    List<vehiclelist> _myvehicles;
    public ProgressBar sp;
    LiveTrackTraceSingleVehicleActivity _lstVehicleNew;


    @Override
    protected Object doInBackground(Object[] params) {
        UnitNo = params[0].toString();
        _myvehicles = (List) params[1];
        sp = (ProgressBar) params[2];
        _lstVehicleNew = (LiveTrackTraceSingleVehicleActivity) params[3];

        Uri uri;
        if (!UnitNo.equalsIgnoreCase("")) {
            HttpHandler service = new HttpHandler();
            //http://rtracksystem.co.in/Track/FrmVehicleTracingAjaxCallBack.aspx?UnitNo=9346
            if (Login.server == 50) {
                uri = new Uri.Builder()
                        .scheme("http")
                        .authority("rtracksystem.co.in")
                        .path("Track/FrmVehicleTracingAjaxCallBack.aspx")
                        .appendQueryParameter("UnitNo", UnitNo)
                        .build();
            } else {
                uri = new Uri.Builder()
                        .scheme("http")
                        .authority("rtracksystem.co.in")
                        .path("Track/FrmVehicleTracingAjaxCallBack.aspx")
                        .appendQueryParameter("UnitNo",UnitNo)
                        .build();
            }

            String response = service.makeServiceCall(uri.toString());

            if (response != null) {
                try {
                    _myvehicles.clear();
                    JSONArray vehicles = null;
                    JSONObject jsonObj = new JSONObject(response);
                    vehicles = jsonObj.getJSONArray("Vehicles");
                    JSONObject c;
                    String latitude, longitude,speed,reporteddatetime,location;

                    for (int i = 0; i < vehicles.length(); i++) {
                        c = vehicles.getJSONObject(i);
                        latitude = c.getString("latitude");
                        longitude = c.getString("longitude");
                        speed = c.getString("speed");
                        reporteddatetime = c.getString("utcToLocalDateTime");
                        location = c.getString("Location");


                        _myvehicles.add(new vehiclelist(latitude, longitude,speed,reporteddatetime,location));

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("HTTPHandler", "Couldn't get any data from the url");
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object result) {
        sp.setVisibility(View.GONE);
        _lstVehicleNew.populatevehiclelistview(_myvehicles);
    }
}
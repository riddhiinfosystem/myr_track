package services;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import com.riddhi.myr_track.Login;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import Fragment.VehicleFragment;
import model.vehiclelist;

/**
 * Created by Narendra on 11/7/2014.
 */
public class ListVehicleAsyncNew extends AsyncTask {

    Integer _companyId, _userid;
    RelativeLayout sp;
    List<vehiclelist> _myvehicles;
    VehicleFragment _lstVehicleNew;


    @Override
    protected Object doInBackground(Object[] params) {
        _companyId = Integer.valueOf(params[0].toString());
        _myvehicles = (List) params[1];
        sp = (RelativeLayout) params[2];
        _lstVehicleNew = (VehicleFragment) params[3];
        _userid = Integer.valueOf(params[4].toString());

        Uri uri;
        if (_companyId != 0)
        {
            HttpHandler service = new HttpHandler();

            if (Login.server == 50) {
                uri = new Uri.Builder()
                        .scheme("http")
                        .authority("rtracksystem.co.in")
                        .path("android/FrmVehicleCurrentStatus.aspx")
                        .appendQueryParameter("CompanyID", String.valueOf(_companyId))
                        .appendQueryParameter("UserID", String.valueOf(_userid))
                        .build();
            } else {
                uri = new Uri.Builder()
                        .scheme("http")
                        .authority("rtracksystem.co.in")
                        .path("android/FrmVehicleCurrentStatus.aspx")
                        .appendQueryParameter("CompanyID", String.valueOf(_companyId))
                        .appendQueryParameter("UserID", String.valueOf(_userid))
                        .build();
            }

            String response = service.makeServiceCall(uri.toString());

            if (response != null) {
                try {
                    _myvehicles.clear();
                    JSONArray vehicles = null;
                    JSONObject jsonObj = new JSONObject(response);
                    vehicles = jsonObj.getJSONArray("Vehicles");
                    JSONObject c;
                    String vehicleNo, name, latitude, longitude, speed, lastreported, unitNo, BatteryStatus,Status,Tracking_Date_Time;
                    String analog1, digital1, digital2,digital3, onewiredata1, onewiredata2, onewiredata3;
                    String SensorIDonAnalogOne, SensorIDonDigitalIO1, SensorIDonDigitalIO2, SensorIDonDigitalIO3;
                    String SensorIDonOneWireData1, SensorIDonOneWireData2, SensorIDonOneWireData3,ignition;


                    for (int i = 0; i < vehicles.length(); i++) {
                        c = vehicles.getJSONObject(i);
                        vehicleNo = c.getString("Vehicle No");
                        name = c.getString("Location");
                        latitude = c.getString("latitude");
                        longitude = c.getString("longitude");
                        speed = c.getString("speed");
                        lastreported = c.getString("lastreported");
                        unitNo = c.getString("UnitNo");
                        BatteryStatus = c.getString("Ext_Battery_Volt");
                        ignition = ""+c.getString("ignition");
                        Status = ""+c.getString("Status");
                        Tracking_Date_Time = ""+c.getString("Tracking_Date_Time");

                        if (c.has("AnalogOne")) {
                            analog1 = c.getString("AnalogOne");
                            digital1 = c.getString("DigitalIO1");
                            digital2 = c.getString("DigitalIO2");
                            digital3 = ""+c.getString("DigitalIO3");
                            onewiredata1 = c.getString("OneWireData1");
                            onewiredata2 = c.getString("OneWireData2");
                            onewiredata3 = c.getString("OneWireData3");
                            SensorIDonAnalogOne = c.getString("SensorIDonAnalogOne");
                            SensorIDonDigitalIO1 = c.getString("SensorIDonDigitalIO1");
                            SensorIDonDigitalIO2 = c.getString("SensorIDonDigitalIO2");
                            SensorIDonDigitalIO3 = c.getString("SensorIDonDigitalIO3");
                            SensorIDonOneWireData1 = c.getString("SensorIDonOneWireData1");
                            SensorIDonOneWireData2 = c.getString("SensorIDonOneWireData2");
                            SensorIDonOneWireData3 = c.getString("SensorIDonOneWireData3");
                        } else {
                            analog1 = "";
                            digital1 = "";
                            digital2 = "";
                            digital3 = "";
                            onewiredata1 = "";
                            onewiredata2 = "";
                            onewiredata3 = "";
                            SensorIDonAnalogOne = "0";
                            SensorIDonDigitalIO1 = "0";
                            SensorIDonDigitalIO2 = "0";
                            SensorIDonDigitalIO3 = "0";
                            SensorIDonOneWireData1 = "0";
                            SensorIDonOneWireData2 = "0";
                            SensorIDonOneWireData3 = "0";
                        }

                        _myvehicles.add(new vehiclelist(vehicleNo, name, latitude, longitude, speed, lastreported, analog1, digital1, digital2,digital3, onewiredata1, onewiredata2, onewiredata3, SensorIDonAnalogOne, SensorIDonDigitalIO1, SensorIDonDigitalIO2, SensorIDonDigitalIO3, SensorIDonOneWireData1, SensorIDonOneWireData2, SensorIDonOneWireData3, unitNo, BatteryStatus,ignition,Status,Tracking_Date_Time));

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("HTTPHandler", "Couldn't get any data from the url");
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object result) {
        sp.setVisibility(View.GONE);
        _lstVehicleNew.populatevehiclelistview(_myvehicles);
    }
}
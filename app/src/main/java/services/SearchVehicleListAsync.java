package services;

import android.net.Uri;
import android.os.AsyncTask;
import android.view.View;
import android.widget.LinearLayout;

import com.riddhi.myr_track.DailyAnlysisTimeBaseActivity;
import com.riddhi.myr_track.Login;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Vinayak on 8/13/2018.
 */

public class SearchVehicleListAsync extends AsyncTask {

    Integer _companyId, _userid;
    String[] searchArray;
    DailyAnlysisTimeBaseActivity dailyAnlysisTimeBaseActivity;
    int condition = 0;
    String classPosition = "1";
    JSONArray vehicles = new JSONArray();

    String[] vehicleArray;
    ArrayList<String> vehiclenolist;
    ArrayList<String> vehicleIdlist;
    ArrayList<String> unitnolist;
    LinearLayout linearLayoutdailyvehana;

    protected Object doInBackground(Object[] params) {
        _companyId = Integer.valueOf(params[0].toString());
        _userid = Integer.valueOf(params[1].toString());
        classPosition = params[4].toString();
        dailyAnlysisTimeBaseActivity = (DailyAnlysisTimeBaseActivity) params[2];


        linearLayoutdailyvehana = (LinearLayout) params[3];

        Uri uri;
        if (_companyId != 0) {
            HttpHandler service = new HttpHandler();

            if (Login.server == 50) {

                uri = new Uri.Builder()
                        .scheme("http")
                        .authority("rtracksystem.co.in")
                        .path("android/frmvehiclelist.aspx")
                        .appendQueryParameter("CompanyID", String.valueOf(_companyId))
                        .appendQueryParameter("UserID", String.valueOf(_userid))
                        .build();
            } else {
                uri = new Uri.Builder()
                        .scheme("http")
                        .authority("rtracksystem.co.in")
                        .path("android/frmvehiclelist.aspx")
                        .appendQueryParameter("CompanyID", String.valueOf(_companyId))
                        .appendQueryParameter("UserID", String.valueOf(_userid))
                        .build();
            }

            String response = service.makeServiceCall(uri.toString());

            if (response != null) {
                try {
                    condition = 1;

                    JSONObject jsonObj = new JSONObject(response);
                    vehicles = jsonObj.getJSONArray("Vehicles");
                    JSONObject c;
                    String vehicleno = null, vehicleid = null, unitno = null;

                    vehicleIdlist = new ArrayList<String>();
                    vehiclenolist = new ArrayList<String>();
                    unitnolist = new ArrayList<String>();

                    searchArray = new String[vehicles.length()];
                    for (int i = 0; i < vehicles.length(); i++) {
                        c = vehicles.getJSONObject(i);
                        vehicleno = c.getString("Vehicle_No");
                        vehicleid = c.getString("Vehicle_ID");
                        unitno = c.getString("UnitNo");

                        searchArray[i] = vehicleno + "," + vehicleid + "," + unitno;

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (response == null) {
                condition = 2;
            } else {
                if (vehicles.length() == 0) {
                    condition = 3;
                }
            }
        }
        return null;
    }


    protected void onPostExecute(Object result) {
        //       pr.setVisibility(View.GONE);
        linearLayoutdailyvehana.setVisibility(View.VISIBLE);

        if (condition == 1) {
            dailyAnlysisTimeBaseActivity.bindglobalarray(searchArray);
        }
        if (condition == 2) {
            dailyAnlysisTimeBaseActivity.setNullError();
        }
        if (condition == 3) {
            dailyAnlysisTimeBaseActivity.setNoDataFoundError();
        }

    }
}


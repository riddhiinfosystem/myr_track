package services;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.riddhi.myr_track.VehicleNearByPlaceActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Vinayak on 6/18/2018.
 */

public class SearchPlaceAsync extends AsyncTask {

    Integer _companyId, _userid;
    String[] searchArray;

    VehicleNearByPlaceActivity vehicleNearByPlaceActivity;
    int condition = 0;
    String SearchFor = "";
    JSONArray vehicles = new JSONArray();

    String[] vehicleArray;
    ArrayList<String> vehiclenolist;
    ArrayList<String> vehicleIdlist;
    ArrayList<String> unitnolist;
    LinearLayout linearLayoutdailyvehana;

    protected Object doInBackground(Object[] params) {
        _companyId = Integer.valueOf(params[0].toString());
        SearchFor = params[1].toString();
        vehicleNearByPlaceActivity = (VehicleNearByPlaceActivity) params[2];
        linearLayoutdailyvehana = (LinearLayout) params[3];

        Uri uri;
        if (_companyId != 0) {
            HttpHandler service = new HttpHandler();

            uri = new Uri.Builder()
                    .scheme("http")
                    .authority("rtracksystem.co.in")
                    .path("android/FrmNearByVehiclePlaceReport.aspx")
                    .appendQueryParameter("CompanyID", String.valueOf(_companyId))
                    .appendQueryParameter("SearchFor", "" + SearchFor)
                    .build();

            String response = service.makeServiceCall(uri.toString());

            if (response != null) {
                try {
                    condition = 1;

                    JSONObject jsonObj = new JSONObject(response);
                    vehicles = jsonObj.getJSONArray("Vehicles");
                    JSONObject c;
                    String vehicleno = null, vehicleid = null, unitno = null;

                    vehicleIdlist = new ArrayList<String>();
                    vehiclenolist = new ArrayList<String>();
                    unitnolist = new ArrayList<String>();

                    searchArray = new String[vehicles.length()];
                    for (int i = 0; i < vehicles.length(); i++) {
                        c = vehicles.getJSONObject(i);
                        vehicleno = c.getString("Place_Name");
                        vehicleid = c.getString("latLong");

                        searchArray[i] = vehicleno + "?" + vehicleid;

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (response == null) {
                condition = 2;
            } else {
                if (vehicles.length() == 0) {
                    condition = 3;
                }
            }
        }
        return null;
    }


    protected void onPostExecute(Object result) {
        linearLayoutdailyvehana.setVisibility(View.VISIBLE);
        if (condition == 1) {
            Log.e("searchArray", "--->" + searchArray.length);
            vehicleNearByPlaceActivity.bindglobalarray(searchArray);
        }
        if (condition == 2) {
            Log.e("condition", "--->2 Null Record Found");
            //vehicleNearByPlaceActivity.setNullError();
        }
        if (condition == 3) {
            Log.e("condition", "--->3 Data Not Found");
            //vehicleNearByPlaceActivity.setNoDataFoundError();
        }
    }
}


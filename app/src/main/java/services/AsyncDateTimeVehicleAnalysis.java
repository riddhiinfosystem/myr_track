package services;

import android.net.Uri;
import android.os.AsyncTask;

import com.riddhi.myr_track.DailyAnalysisTimeBaseReportList;
import com.riddhi.myr_track.Login;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import model.Searchlist;

/**
 * Created by Vinayak on 8/13/2018.
 */

public class AsyncDateTimeVehicleAnalysis extends AsyncTask {
    String vehicleid;
    String unitid;
    String datefrom;
    String dateto;
    String[] searchArray;
    DailyAnalysisTimeBaseReportList searchReportList;
    List<Searchlist> _mysearchlist;
    ArrayList<String> vehiclenolist;
    ArrayList<String> vehicleIdlist;
    ArrayList<String> unitnolist;
    JSONArray vehicles = new JSONArray();
    int condition = 0;
    String fromTime, toTime;

    protected Object doInBackground(Object[] params) {
        vehicleid = String.valueOf(params[0].toString());
        unitid = String.valueOf(params[1].toString());
        datefrom = String.valueOf(params[2].toString());
        dateto = String.valueOf(params[3].toString());
        _mysearchlist = (List) params[4];
        searchReportList = (DailyAnalysisTimeBaseReportList) params[5];
        fromTime = String.valueOf(params[6]);
        toTime = String.valueOf(params[7]);

        searchReportList = (DailyAnalysisTimeBaseReportList) params[5];
        Uri uri;
        if (vehicleid != null) {
            HttpHandler service = new HttpHandler();

            if (Login.server == 50) {

                uri = new Uri.Builder()
                        .scheme("http")
                        .authority("rtracksystem.co.in")
                        .path("android/FrmVehicleAnalysisTimeBase.aspx")
                        .appendQueryParameter("VehicleID", String.valueOf(vehicleid))
                        .appendQueryParameter("FromDate", String.valueOf(datefrom))
                        .appendQueryParameter("ToDate", String.valueOf(dateto))
                        .appendQueryParameter("FromTime", fromTime)
                        .appendQueryParameter("ToTime", toTime)
                        .appendQueryParameter("CompanyID", String.valueOf(Login.companyid))
                        .build();
            } else {
                uri = new Uri.Builder()
                        .scheme("http")
                        .authority("rtracksystem.co.in")
                        .path("android/FrmVehicleAnalysisTimeBase.aspx")
                        .appendQueryParameter("VehicleID", String.valueOf(vehicleid))
                        .appendQueryParameter("FromDate", String.valueOf(datefrom))
                        .appendQueryParameter("ToDate", String.valueOf(dateto))
                        .appendQueryParameter("FromTime", fromTime)
                        .appendQueryParameter("ToTime", toTime)
                        .appendQueryParameter("CompanyID", String.valueOf(Login.companyid))
                        .build();
            }

            String response = service.makeServiceCall(uri.toString());
            response = android.text.Html.fromHtml(response).toString();
            if (response != null) {
                try {
                    condition = 1;
                    int srCount = 0;
                    JSONObject jsonObj = new JSONObject(response);
                    vehicles = jsonObj.getJSONArray("Vehicles");
                    JSONObject c;
                    String srno, vehicleno, StartLocation, EndLocation, totalrunningkms, totalrunningtime, totalstopagetime;

                    vehicleIdlist = new ArrayList<String>();
                    vehiclenolist = new ArrayList<String>();
                    unitnolist = new ArrayList<String>();

                    searchArray = new String[vehicles.length()];
                    for (int i = 0; i < vehicles.length(); i++) {
                        c = vehicles.getJSONObject(i);

                        srCount = srCount + 1;
                        srno = "" + srCount;
                        vehicleno = c.getString("Vehicle No");
                        StartLocation = c.getString("Start Location");
                        EndLocation = c.getString("End Location");
                        totalrunningkms = c.getString("Running KMS");
                        totalrunningtime = c.getString("Drive time");
                        totalstopagetime = c.getString("Total idle time");

                        _mysearchlist.add(new Searchlist(srno, vehicleno, totalrunningkms, totalrunningtime, totalstopagetime, StartLocation, EndLocation));

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if (response == null) {
                condition = 2;
            } else {
                if (vehicles.length() == 0) {
                    condition = 3;
                }
            }
        }
        return null;
    }

    protected void onPostExecute(Object result) {
        if (condition == 1) {
            searchReportList.populatevehiclelistview(_mysearchlist);
        }
        if (condition == 2) {
            searchReportList.setNullError();
        }
        if (condition == 3) {
            searchReportList.setNoDataFoundError();
        }
    }
}
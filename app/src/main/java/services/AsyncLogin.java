package services;

import android.net.Uri;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.riddhi.myr_track.Login;

import org.json.JSONObject;

/**
 * Created by abc on 1/17/2015.
 */
public class AsyncLogin extends AsyncTask {

    Integer _companyId, _userid = 0;
    int condition = 0;
    String _companyname;
    String _url;
    String asyncUserName;
    String asyncUserPass;
    ProgressBar sp;
    RelativeLayout _LinearLayout;
    Login login;

    protected Object doInBackground(Object[] params) {

        asyncUserName = String.valueOf(params[0]);
        asyncUserPass = String.valueOf(params[1]);
        sp = (ProgressBar) params[2];
        _LinearLayout = (RelativeLayout) params[3];
        login = (Login) params[4];

        Uri uri;
        ServiceHandler service = new ServiceHandler();

        if (Login.server == 50) {
            uri = new Uri.Builder()

                    .scheme("http")
                    .authority("rtracksystem.co.in")
                    .path("android/alogin.aspx")
                    .appendQueryParameter("uid", asyncUserName)
                    .appendQueryParameter("pass", asyncUserPass)
                    .build();
        } else {
            uri = new Uri.Builder()
                    .scheme("http")
                    .authority("rtracksystem.co.in")
                    .path("android/alogin.aspx")
                    .appendQueryParameter("uid", asyncUserName)
                    .appendQueryParameter("pass", asyncUserPass)

                    .build();
        }

        String response = service.makeServiceCall(uri.toString(),2);
        String loginresult;
        loginresult = "9";

        if (response != null) {
            try {

                condition = 1;
                JSONObject jsonObj = new JSONObject(response);
                loginresult = jsonObj.getString("out");
                _companyId = jsonObj.getInt("companyid");
                _companyname = jsonObj.getString("CompanyName");
                _url = jsonObj.getString("LogoPath");
                _userid = jsonObj.getInt("userid");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (response == null) {
            condition = 2;
        } else {
            if ((loginresult.equals("0"))) {

                condition = 3;
            }
        }
        return null;
    }

    protected void onPostExecute(Object result) {
        sp.setVisibility(View.GONE);
        _LinearLayout.setVisibility(View.VISIBLE);

        if (condition == 1) {
            login.otherWiseCondition(_companyId, _companyname, _url, _userid, asyncUserName, asyncUserPass);
        }
        if (condition == 2) {
            login.responseNull();
        }
        if (condition == 3) {
            if (Login.server == 72) {
                Login.server = 50;
                login.fluctuateActivity(asyncUserName, asyncUserPass);
            } else {
                Login.server = 72;
                login.reponseZero();
            }
        }
    }
}

package services;

import android.net.Uri;
import android.os.AsyncTask;
import android.view.View;
import android.widget.LinearLayout;

import com.riddhi.myr_track.Login;
import com.riddhi.myr_track.TrackAndTrace;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by abc on 1/23/2015.
 */
public class AsyncTrackandTraceLatLon extends AsyncTask {

    String stringAsyncTempPlotUnitNo, stringAsyncTempPlotDateFrom, stringAsyncTempPlotDateTo;
    Integer _companyId, _userid;
    String[] stringsLatitude;
    String[] stringsLongitude;
    String[] stringsLocalDateTime;
    String[] stringsReason;
    String[] stringsLocation;
    LinearLayout _LinearLayout;
    TrackAndTrace trackAndTrace;
    String fromTime, toTime;

    JSONArray vehicles = new JSONArray();
    int condition = 0;

    protected Object doInBackground(Object[] params) {

        stringAsyncTempPlotUnitNo = String.valueOf(params[0]);
        stringAsyncTempPlotDateFrom = String.valueOf(params[1]);
        stringAsyncTempPlotDateTo = String.valueOf(params[2]);
        _companyId = Integer.valueOf(params[3].toString());
        trackAndTrace = (TrackAndTrace) params[4];
        _LinearLayout = (LinearLayout) params[5];
        _userid = Integer.valueOf(params[6].toString());
        fromTime = String.valueOf(params[7]);
        toTime = String.valueOf(params[8]);

        Uri uri;
        if (_companyId != 0) {
            HttpHandler service = new HttpHandler();

            if (Login.server == 50) {

                uri = new Uri.Builder()
                        .scheme("http")
                        .authority("rtracksystem.co.in")
                        .path("android/FrmVehicleTraceReport.aspx")
                        .appendQueryParameter("UnitNo", String.valueOf(stringAsyncTempPlotUnitNo))
                        .appendQueryParameter("FromDateTime", String.valueOf(stringAsyncTempPlotDateFrom))
                        .appendQueryParameter("ToDateTime", String.valueOf(stringAsyncTempPlotDateTo))
                        .appendQueryParameter("FromTime", fromTime)
                        .appendQueryParameter("ToTime", toTime)
                        .build();
            } else {
                uri = new Uri.Builder()
                        .scheme("http")
                        .authority("rtracksystem.co.in")
                        .path("android/FrmVehicleTraceReport.aspx")
                        .appendQueryParameter("UnitNo", String.valueOf(stringAsyncTempPlotUnitNo))
                        .appendQueryParameter("FromDateTime", String.valueOf(stringAsyncTempPlotDateFrom))
                        .appendQueryParameter("ToDateTime", String.valueOf(stringAsyncTempPlotDateTo))
                        .appendQueryParameter("FromTime", fromTime)
                        .appendQueryParameter("ToTime", toTime)
                        .build();
            }

            String response = service.makeServiceCall(uri.toString());

            if (response != null) {
                try {
                    condition = 1;

                    JSONObject jsonObj = new JSONObject(response);
                    vehicles = jsonObj.getJSONArray("Vehicles");
                    JSONObject c;
                    String sLatitude, sLongitude, sLoacalDateandTime, sReason, sLocation;

                    stringsLatitude = new String[vehicles.length()];
                    stringsLongitude = new String[vehicles.length()];
                    stringsLocalDateTime = new String[vehicles.length()];
                    stringsReason = new String[vehicles.length()];
                    stringsLocation = new String[vehicles.length()];

                    for (int i = 0; i < vehicles.length(); i++) {
                        c = vehicles.getJSONObject(i);
                        sLatitude = c.getString("latitude");
                        sLongitude = c.getString("longitude");
                        sLoacalDateandTime = c.getString("utcToLocalDateTime");
                        sReason = c.getString("Reason");
                        sLocation = c.getString("Location");
                        stringsLatitude[i] = sLatitude;
                        stringsLongitude[i] = sLongitude;
                        stringsLocalDateTime[i] = sLoacalDateandTime;
                        stringsReason[i] = sReason;
                        stringsLocation[i] = sLocation;
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            if (response == null) {
                condition = 2;
            } else {
                if (vehicles.length() == 0) {
                    condition = 3;
                }
            }
        }
        return null;
    }

    protected void onPostExecute(Object result) {

        _LinearLayout.setVisibility(View.VISIBLE);

        if (condition == 1) {
            trackAndTrace.showdata(stringsLatitude, stringsLongitude, stringsLocalDateTime, stringsReason, stringsLocation);
        }
        if (condition == 2) {
            trackAndTrace.setNullError();
        }
        if (condition == 3) {
            trackAndTrace.setNoDataFoundError();
        }
    }
}

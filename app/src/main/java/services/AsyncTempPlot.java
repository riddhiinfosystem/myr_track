package services;

import android.net.Uri;
import android.os.AsyncTask;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.riddhi.myr_track.Login;
import com.riddhi.myr_track.TempraturePlotGraph;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by abc on 1/6/2015.
 */
public class AsyncTempPlot extends AsyncTask {

    String stringAsyncTempPlotUnitNo, stringAsyncTempPlotDateFrom, stringAsyncTempPlotDateTo;
    Integer _companyId, _userid;
    ProgressBar sp;
    String[] unitnoArray;
    String[] vehicleNoArray;
    String[] dateArray;
    String[] timeArray;
    String[] sensorO1Array;
    String[] sensorO2Array;
    String[] sensorO3Array;
    String[] sensorA1Array;
    String[] sensorA2Array;
    LinearLayout _LinearLayout;
    int condition = 0;
    TempraturePlotGraph tempraturePlotGraph;
    JSONArray vehicles = new JSONArray();
    String fromTime, toTime;

    protected Object doInBackground(Object[] params) {

        stringAsyncTempPlotUnitNo = String.valueOf(params[0]);
        stringAsyncTempPlotDateFrom = String.valueOf(params[1]);
        stringAsyncTempPlotDateTo = String.valueOf(params[2]);
        sp = (ProgressBar) params[3];
        _companyId = Integer.valueOf(params[4].toString());
        tempraturePlotGraph = (TempraturePlotGraph) params[5];
        _LinearLayout = (LinearLayout) params[6];
        _userid = Integer.valueOf(params[7].toString());
        fromTime = String.valueOf(params[8]);
        toTime = String.valueOf(params[9]);

        Uri uri;
        if (_companyId != 0) {
            HttpHandler service = new HttpHandler();

            if (Login.server == 50) {

                uri = new Uri.Builder()
                        .scheme("http")
                        .authority("rtracksystem.co.in")
//                    .authority("track.rtracksystem.com")
                        .path("android/FrmTemperatureSensorReport.aspx")
                        .appendQueryParameter("CompanyID", String.valueOf(_companyId))
                        .appendQueryParameter("UnitNo", String.valueOf(stringAsyncTempPlotUnitNo))
                        .appendQueryParameter("FromDate", String.valueOf(stringAsyncTempPlotDateFrom))
                        .appendQueryParameter("ToDate", String.valueOf(stringAsyncTempPlotDateTo))
                        .appendQueryParameter("FromTime", fromTime)
                        .appendQueryParameter("ToTime", toTime)
                        .build();
            } else {
                uri = new Uri.Builder()
                        .scheme("http")
                        .authority("rtracksystem.co.in")
                        .path("android/FrmTemperatureSensorReport.aspx")
                        .appendQueryParameter("CompanyID", String.valueOf(_companyId))
                        .appendQueryParameter("UnitNo", String.valueOf(stringAsyncTempPlotUnitNo))
                        .appendQueryParameter("FromDate", String.valueOf(stringAsyncTempPlotDateFrom))
                        .appendQueryParameter("ToDate", String.valueOf(stringAsyncTempPlotDateTo))
                        .appendQueryParameter("UserID", String.valueOf(_userid))
                        .appendQueryParameter("FromTime", fromTime)
                        .appendQueryParameter("ToTime", toTime)
                        .build();
            }

            String response = service.makeServiceCall(uri.toString());

            if (response != null) {
                try {

                    condition = 1;

                    JSONObject jsonObj = new JSONObject(response);
                    vehicles = jsonObj.getJSONArray("Vehicles");
                    JSONObject c;
//                    String vehicleno = null, vehicleid = null, unitno = null;
                    String unitNo, vehicleNo, date, time, sensorO1, sensorO2, sensorO3, sensorA1, sensorA2;

//                    JSONObject c = vehicles.getJSONObject(0);
//                    String vehicleNo = c.getString("Vehicle No");
//                    Log.w("txt", vehicleNo);
//                    searchArray = new String[vehicles.length()];

                    unitnoArray = new String[vehicles.length()];
                    vehicleNoArray = new String[vehicles.length()];
                    dateArray = new String[vehicles.length()];
                    timeArray = new String[vehicles.length()];
                    sensorO1Array = new String[vehicles.length()];
                    sensorO2Array = new String[vehicles.length()];
                    sensorO3Array = new String[vehicles.length()];
                    sensorA1Array = new String[vehicles.length()];
                    sensorA2Array = new String[vehicles.length()];
                    for (int i = 0; i < vehicles.length(); i++) {
                        c = vehicles.getJSONObject(i);
                        unitNo = c.getString("UnitNo");
                        vehicleNo = c.getString("Vehicle No");
                        date = c.getString("Date");
                        time = c.getString("Time");
                        sensorO1 = c.getString("SensorO1");
                        sensorO2 = c.getString("SensorO2");
                        sensorO3 = c.getString("SensorO3");
                        sensorA1 = c.getString("Sensor A 1");
                        sensorA2 = c.getString("Sensor A 2");
                        unitnoArray[i] = unitNo;
                        vehicleNoArray[i] = vehicleNo;
                        dateArray[i] = date;
                        timeArray[i] = time;
                        sensorO1Array[i] = sensorO1;
                        sensorO2Array[i] = sensorO2;
                        sensorO3Array[i] = sensorO3;
                        sensorA1Array[i] = sensorA1;
                        sensorA2Array[i] = sensorA2;

//                        searchArray[i] = vehicleno + "," + vehicleid + "," + unitno;
//                        _myreportlist.add(new reportlist(vehicleno,vehicleid,unitno));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (response == null) {
                condition = 2;
            } else {

                if (vehicles.length() == 0) {
                    condition = 3;
                }
            }
        }
        return null;
    }

    protected void onPostExecute(Object result) {
        //       pr.setVisibility(View.GONE);

        sp.setVisibility(View.GONE);
        _LinearLayout.setVisibility(View.VISIBLE);

        if (condition == 1) {
            tempraturePlotGraph.bindglobalarray(unitnoArray, vehicleNoArray, dateArray, timeArray, sensorO1Array, sensorO2Array, sensorO3Array, sensorA1Array, sensorA2Array);
        }
        if (condition == 2) {
            tempraturePlotGraph.setNullError();
        }
        if (condition == 3) {
            tempraturePlotGraph.setDataNotFindError();
        }
    }
}
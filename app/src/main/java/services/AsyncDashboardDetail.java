package services;

import android.net.Uri;
import android.os.AsyncTask;
import android.widget.RelativeLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import Fragment.HomeFragment;


public class AsyncDashboardDetail extends AsyncTask {

    RelativeLayout sp;
    Integer _companyId, _userid;
    String total_vehicle, ok_vehicle, in_garage, power_disconnect, device_problem;
    HomeFragment homeFragment;
    int condition = 0;
    JSONArray controllingBranchList = new JSONArray();

    @Override
    protected Object doInBackground(Object[] params) {
        _companyId = Integer.valueOf(params[0].toString());
        _userid = Integer.valueOf(params[1].toString());
        sp = (RelativeLayout) params[2];
        homeFragment = (HomeFragment) params[3];


        Uri uri;
        if (_companyId != 0) {
            HttpHandler service = new HttpHandler();
            uri = new Uri.Builder()
                    .scheme("http")
                    .authority("rtracksystem.co.in")
                    .path("android/FrmDashboardDetail.aspx")
                    .appendQueryParameter("CompanyID", String.valueOf(_companyId))
                    .appendQueryParameter("UserID", String.valueOf(_userid))
                    .build();

            String response = service.makeServiceCall(uri.toString());

            if (response != null) {
                try {
                    condition = 1;
                    JSONObject jsonObj = new JSONObject(response);

                    controllingBranchList = jsonObj.getJSONArray("JsonArray");

                    JSONObject c, d;
                    JSONArray j = new JSONArray();

                    for (int i = 0; i < controllingBranchList.length(); i++) {
                        c = controllingBranchList.getJSONObject(i);
                        j = c.getJSONArray("Vehicles");
                        for (int k = 0; k < j.length(); k++) {
                            d = j.getJSONObject(k);
                            if (i == 0) {
                                total_vehicle = d.getString("Total Vehicle");
                            }
                            if (i == 1) {
                                ok_vehicle = d.getString("OK Vehicle");
                            }
                            if (i == 2) {
                                in_garage = d.getString("In Garage");
                            }
                            if (i == 3) {
                                power_disconnect = d.getString("Power Disconnect");
                            }
                            if (i == 4) {
                                device_problem = d.getString("Device Problem");
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (response == null) {
                condition = 2;
            } else {
                if (controllingBranchList.length() == 0) {
                    condition = 3;
                }
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object result) {

        if (condition == 1) {
            homeFragment.setDashboardDataOnView(total_vehicle, ok_vehicle, in_garage, power_disconnect, device_problem);

        }


    }
}
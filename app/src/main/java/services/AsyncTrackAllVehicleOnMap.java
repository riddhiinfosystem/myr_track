package services;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.riddhi.myr_track.Login;
import com.riddhi.myr_track.TrackAllActivity;
import com.riddhi.myr_track.trackvehicle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import model.NearByVehicleList;

public class AsyncTrackAllVehicleOnMap extends AsyncTask {

    Integer _companyId, _userid;
    TrackAllActivity trackvehicle1;
    List<NearByVehicleList> nearByVehicle = new ArrayList<NearByVehicleList>();
    int condition = 0;
    JSONArray vehicles = new JSONArray();

    protected Object doInBackground(Object[] params) {
        _companyId = Integer.valueOf(params[0].toString());
        _userid = Integer.valueOf(params[1].toString());
        trackvehicle1 = (TrackAllActivity) params[2];

        Uri uri;
        if (_companyId != 0) {
            HttpHandler service = new HttpHandler();

            uri = new Uri.Builder()
                    .scheme("http")
                    .authority("rtracksystem.co.in")
                    .path("android/FrmTrackAllVehicleOnMap.aspx")
                    .appendQueryParameter("CompanyID", String.valueOf(_companyId))
                    .appendQueryParameter("UserID", String.valueOf(_userid))
                    .build();

            String response = service.makeServiceCall(uri.toString());

            if (response != null) {
                try {
                    condition = 1;
                    JSONObject jsonObj = new JSONObject(response);
                    vehicles = jsonObj.getJSONArray("Vehicles");
                    JSONObject c;
                    String Vehicle_No, Date_Time, latitude, longitude, Speed, Location,Driver1_Name;
                    int Vehicle_ID = 0,Direction = 0;
                    for (int i = 0; i < vehicles.length(); i++) {
                        c = vehicles.getJSONObject(i);
                        Vehicle_ID = c.getInt("Vehicle_ID");
                        Vehicle_No = ""+c.getString("Vehicle_No");
                        Date_Time = ""+c.getString("Date_Time");
                        latitude = ""+c.getString("latitude");
                        longitude = ""+c.getString("longitude");
                        Speed = ""+c.getString("Speed");
                        Location = ""+c.getString("Location");
                        Driver1_Name = ""+c.getString("Driver1_Name");
                        Direction = c.getInt("Direction");

                        nearByVehicle.add(new NearByVehicleList(Vehicle_ID,Vehicle_No, Date_Time, latitude, longitude, Speed, Location,Driver1_Name,Direction));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (response == null) {
                condition = 2;
            } else {
                if (vehicles.length() == 0) {
                    condition = 3;
                }
            }
        }
        return null;
    }


    protected void onPostExecute(Object result)
    {
        if (condition == 1) {
            Log.e("Data received", "successfully------>");
            trackvehicle1.setNearByVehicleData(nearByVehicle);
        }
        if (condition == 2) {
            trackvehicle1.setNullError();
        }
        if (condition == 3) {
            trackvehicle1.setNoDataFoundError();
        }
    }
}

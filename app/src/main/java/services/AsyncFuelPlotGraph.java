package services;

import android.net.Uri;
import android.os.AsyncTask;

import com.riddhi.myr_track.FuelPlotGraph;
import com.riddhi.myr_track.Login;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import model.FualConsumptionReport;

/**
 * Created by abc on 1/12/2015.
 */
public class AsyncFuelPlotGraph extends AsyncTask {

    String stringAsyncTempPlotUnitNo, stringAsyncTempPlotDateFrom, stringAsyncTempPlotDateTo;
    Integer _companyId, _userid;
    FuelPlotGraph fuelPlotGraph;
    String fromTime, toTime;
    List<FualConsumptionReport> _mysearchlist;
    int condition = 0;
    JSONArray vehicles = new JSONArray();

    protected Object doInBackground(Object[] params) {

        stringAsyncTempPlotUnitNo = String.valueOf(params[0]);
        stringAsyncTempPlotDateFrom = String.valueOf(params[1]);
        stringAsyncTempPlotDateTo = String.valueOf(params[2]);
        _companyId = Integer.valueOf(params[3].toString());
        fuelPlotGraph = (FuelPlotGraph) params[4];
        _userid = Integer.valueOf(params[5].toString());
        fromTime = String.valueOf(params[6]);
        toTime = String.valueOf(params[7]);
        _mysearchlist = (List) params[8];

        Uri uri;
        if (_companyId != 0) {
            HttpHandler service = new HttpHandler();

            if (Login.server == 50) {

                uri = new Uri.Builder()
                        .scheme("http")
                        .authority("rtracksystem.co.in")//rtracksystem.com
//                    .authority("track.rtracksystem.com")
                        .path("android/FrmFuelSensorReport.aspx")
                        .appendQueryParameter("CompanyID", String.valueOf(_companyId))
                        .appendQueryParameter("UnitNo", String.valueOf(stringAsyncTempPlotUnitNo))
                        .appendQueryParameter("FromDate", String.valueOf(stringAsyncTempPlotDateFrom))
                        .appendQueryParameter("ToDate", String.valueOf(stringAsyncTempPlotDateTo))
                        .appendQueryParameter("FromTime", fromTime)
                        .appendQueryParameter("toTime", toTime)
                        .build();
            } else {
                uri = new Uri.Builder()
                        .scheme("http")
                        .authority("rtracksystem.co.in")//track.rtracksystem.com
                        .path("android/FrmFuelSensorReport.aspx")
                        .appendQueryParameter("CompanyID", String.valueOf(_companyId))
                        .appendQueryParameter("UnitNo", String.valueOf(stringAsyncTempPlotUnitNo))
                        .appendQueryParameter("FromDate", String.valueOf(stringAsyncTempPlotDateFrom))
                        .appendQueryParameter("ToDate", String.valueOf(stringAsyncTempPlotDateTo))
                        .appendQueryParameter("UserID", String.valueOf(_userid))
                        .appendQueryParameter("FromTime", fromTime)
                        .appendQueryParameter("toTime", toTime)
                        .build();
            }


            String response = service.makeServiceCall(uri.toString());

            if (response != null) {
                try {
                    condition = 1;

                    JSONObject jsonObj = new JSONObject(response);
                    vehicles = jsonObj.getJSONArray("Vehicles");
                    JSONObject c;
                    String srno, vehicleno, activitytype, time, startlocation, endlocation, totalkms, totalfuelconsumption, totalavg;


                    for (int i = 0; i < vehicles.length(); i++) {
                        c = vehicles.getJSONObject(i);
                        srno = c.getString("UnitNo");
                        vehicleno = c.getString("Vehicle No");
                        activitytype = c.getString("speed");
                        time = c.getString("Date");
                        startlocation = c.getString("Time");
                        endlocation = c.getString("Fuel Level");
                        totalkms = c.getString("Location");

                        /*public String _UnitNo;
                        public String _Vehicle_No;
                        public String _speed;
                        public String _Date;
                        public String _Time;
                        public String _Fuel_Level;
                        public String _Location;*/

                       // _mysearchlist.add(new FualConsumptionReport(srno, vehicleno, activitytype, time, startlocation, endlocation, totalkms, totalfuelconsumption, totalavg));
                        _mysearchlist.add(new FualConsumptionReport(srno, vehicleno, activitytype, time, startlocation, endlocation, totalkms));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (response == null) {
                condition = 2;
            } else {
                if (vehicles.length() == 0) {
                    condition = 3;
                }
            }
        }
        return null;
    }


    protected void onPostExecute(Object result) {
        if (condition == 1) {
            fuelPlotGraph.populatevehiclelistview(_mysearchlist);

        }
        if (condition == 2) {
            fuelPlotGraph.setNullError();
        }
        if (condition == 3) {
            fuelPlotGraph.setNoDataFoundError();
        }


    }
}


package services;

import android.net.Uri;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import model.VehicleStatus;

/**
 * Created by Vinayak on 5/29/2018.
 */

public class AsyncVehicleStatus extends AsyncTask {

    Integer _companyId, _userid;
    List<VehicleStatus> _myvehiclesList;
    List<VehicleStatus> _myvehiclesControllingBranch;
    List<VehicleStatus> _myvehiclesStatus;
    com.riddhi.myr_track.VehicleStatusActivity _lstVehicleNew;
    int condition = 0;
    JSONArray controllingBranchList = new JSONArray();

    @Override
    protected Object doInBackground(Object[] params) {
        _companyId = Integer.valueOf(params[0].toString());
        _userid = Integer.valueOf(params[1].toString());
        _myvehiclesList = new ArrayList<>();
        _myvehiclesControllingBranch = new ArrayList<>();
        _myvehiclesStatus = new ArrayList<>();
        _lstVehicleNew = (com.riddhi.myr_track.VehicleStatusActivity) params[2];

        Uri uri;
        if (_companyId != 0) {
            HttpHandler service = new HttpHandler();

            uri = new Uri.Builder()
                    .scheme("http")
                    .authority("rtracksystem.co.in")
                    .path("android/FrmVehicleStatus.aspx")
                    .appendQueryParameter("CompanyID", String.valueOf(_companyId))
                    .appendQueryParameter("UserID", String.valueOf(_userid))
                    .build();

            String response = service.makeServiceCall(uri.toString());

            if (response != null) {
                try {
                    condition = 1;
                    JSONObject jsonObj = new JSONObject(response);

                    controllingBranchList = jsonObj.getJSONArray("JsonArray");

                    JSONObject c, d;
                    JSONArray j = new JSONArray();
                    String Controlling_Branch_ID, Vehicle_No, Status, Control_Branch_ID, Control_Branch;
                    Boolean selected;
                    for (int i = 0; i < controllingBranchList.length(); i++) {
                        c = controllingBranchList.getJSONObject(i);
                        j = c.getJSONArray("Vehicles");
                        for (int k = 0; k < j.length(); k++) {
                            d = j.getJSONObject(k);
                            if (i == 0) {
                                Controlling_Branch_ID = d.getString("Controlling_Branch_ID");
                                Vehicle_No = d.getString("Vehicle_No");
                                selected = false;
                                Status = "" + d.getString("Status");
                                if(Status.equalsIgnoreCase("none"))
                                {
                                    Status = "";
                                }
                                _myvehiclesList.add(new model.VehicleStatus(Controlling_Branch_ID, Vehicle_No, Status, selected));
                            } else if (i == 1) {
                                if (k == 0) {
                                    if (j.length() == 1) {
                                        Control_Branch_ID = "0";
                                        Control_Branch = "Select controlling Branch";
                                        _myvehiclesControllingBranch.add(new model.VehicleStatus(Control_Branch_ID, Control_Branch));
                                        Control_Branch_ID = "01";
                                        Control_Branch = "Select All";
                                        _myvehiclesControllingBranch.add(new model.VehicleStatus(Control_Branch_ID, Control_Branch));
                                        Control_Branch_ID = d.getString("Controlling_Branch_ID");
                                        Control_Branch = d.getString("Control_Branch");
                                    } else {
                                        Control_Branch_ID = "0";
                                        Control_Branch = "Select controlling Branch";
                                        _myvehiclesControllingBranch.add(new model.VehicleStatus(Control_Branch_ID, Control_Branch));
                                        Control_Branch_ID = "01";
                                        Control_Branch = "Select All";
                                    }

                                } else {

                                    Control_Branch_ID = d.getString("Controlling_Branch_ID");
                                    Control_Branch = d.getString("Control_Branch");
                                }

                                _myvehiclesControllingBranch.add(new model.VehicleStatus(Control_Branch_ID, Control_Branch));
                            } else {
                                if (k == 0) {
                                    Control_Branch_ID = "0";
                                    Control_Branch = "Select Vehicle Status";
                                    _myvehiclesStatus.add(new model.VehicleStatus(Control_Branch_ID, Control_Branch));
                                    Control_Branch_ID = d.getString("Status_ID");
                                    Control_Branch = d.getString("Status");

                                } else {
                                    Control_Branch_ID = d.getString("Status_ID");
                                    Control_Branch = d.getString("Status");
                                }

                                _myvehiclesStatus.add(new model.VehicleStatus(Control_Branch_ID, Control_Branch));
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (response == null) {
                condition = 2;
            } else {
                if (controllingBranchList.length() == 0) {
                    condition = 3;
                }
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object result) {
        //Log.e("onPostExecute","--->Success"+_myvehicles.size()+"--->"+searchArray.length);
        // _lstVehicleNew.
        // _lstVehicleNew.populatevehiclelistview(_myvehicles,);

        if (condition == 1) {
            _lstVehicleNew.bindglobalarray(_myvehiclesControllingBranch, _myvehiclesList, _myvehiclesStatus);

        }
        if (condition == 2) {
            //_lstVehicleNew.setNullError();
        }
        if (condition == 3) {
            _lstVehicleNew.setNoDataFoundError();
        }

    }
}
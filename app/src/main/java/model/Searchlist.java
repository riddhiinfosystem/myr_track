package model;

/**
 * Created by abc on 12/16/2014.
 */
public class Searchlist {

    private String srno;
    private String vehicleno;
    private String date;
    private String totalrunningkms;
    private String totalrunningtime;
    private String totalstopagetime;
    private String startlocation;
    private String endlocation;
    private String activitytype;
    private String fromdate;
    private String todate;

    public Searchlist(String srno, String vehicleno, String date, String totalrunningkms, String totalrunningtime, String totalstopagetime) {
        super();
        this.srno = srno;
        this.vehicleno = vehicleno;
        this.date = date;
        this.totalrunningkms = totalrunningkms;
        this.totalrunningtime = totalrunningtime;
        this.totalstopagetime = totalstopagetime;
    }

    public Searchlist(String srno, String vehicleno, String totalrunningkms, String totalrunningtime, String totalstopagetime, String startlocation, String endlocation) {
        this.srno = srno;
        this.vehicleno = vehicleno;
        this.totalrunningkms = totalrunningkms;
        this.totalrunningtime = totalrunningtime;
        this.totalstopagetime = totalstopagetime;
        this.startlocation = startlocation;
        this.endlocation = endlocation;
    }

    public Searchlist(String vehicleno, String activitytype, String fromdate, String todate, String date, String startlocation, String endlocation, String totalrunningkms) {
        this.vehicleno = vehicleno;
        this.activitytype = activitytype;
        this.fromdate = fromdate;
        this.todate = todate;
        this.date = date;
        this.startlocation = startlocation;
        this.endlocation = endlocation;
        this.totalrunningkms = totalrunningkms;
    }

    public String getActivitytype() {
        return activitytype;
    }

    public void setActivitytype(String activitytype) {
        this.activitytype = activitytype;
    }

    public String getFromdate() {
        return fromdate;
    }

    public void setFromdate(String fromdate) {
        this.fromdate = fromdate;
    }

    public String getTodate() {
        return todate;
    }

    public void setTodate(String todate) {
        this.todate = todate;
    }

    public String getSrno() {
        return srno;
    }

    public void setSrno(String srno) {
        srno = srno;
    }

    public String getvehicleno() {
        return vehicleno;
    }

    public void setVehicleno(String vehicleno) {
        vehicleno = vehicleno;
    }

    public String getdate() {
        return date;
    }

    public void setDate(String date) {
        date = date;
    }

    public String getTotalrunningkms() {
        return totalrunningkms;
    }

    public void setTotalrunningkms(String totalrunningkms) {
        totalrunningkms = totalrunningkms;
    }

    public String getTotalrunningtime() {
        return totalrunningtime;
    }

    public void setTotalrunningtime(String totalrunningtime) {
        totalrunningtime = totalrunningtime;
    }

    public String getTotalstopagetime() {
        return totalstopagetime;
    }

    public void setTotalstopagetime(String totalstopagetime) {
        totalstopagetime = totalstopagetime;
    }

    public String getStartlocation() {
        return startlocation;
    }

    public void setStartlocation(String startlocation) {
        this.startlocation = startlocation;
    }

    public String getEndlocation() {
        return endlocation;
    }

    public void setEndlocation(String endlocation) {
        this.endlocation = endlocation;
    }
}

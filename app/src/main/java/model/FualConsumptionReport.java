package model;

/**
 * Created by Vinayak on 8/14/2018.
 */

public class FualConsumptionReport {
    public String srno;
    public String vehicleno;
    public String activitytype;
    public String time;
    public String startlocation;
    public String endlocation;
    public String totalkms;
    public String totalfuelconsumption;
    public String totalavg;


    public String _UnitNo;
    public String _Vehicle_No;
    public String _speed;
    public String _Date;
    public String _Time;
    public String _Fuel_Level;
    public String _Location;

    public FualConsumptionReport(String srno, String vehicleno, String activitytype, String time, String startlocation, String endlocation, String totalkms, String totalfuelconsumption, String totalavg) {
        this.srno = srno;
        this.vehicleno = vehicleno;
        this.activitytype = activitytype;
        this.time = time;
        this.startlocation = startlocation;
        this.endlocation = endlocation;
        this.totalkms = totalkms;
        this.totalfuelconsumption = totalfuelconsumption;
        this.totalavg = totalavg;
    }

    public FualConsumptionReport(String _UnitNo, String _Vehicle_No, String _speed, String _Date, String _Time, String _Fuel_Level, String _Location) {
        this._UnitNo = _UnitNo;
        this._Vehicle_No = _Vehicle_No;
        this._speed = _speed;
        this._Date = _Date;
        this._Time = _Time;
        this._Fuel_Level = _Fuel_Level;
        this._Location = _Location;
    }
}

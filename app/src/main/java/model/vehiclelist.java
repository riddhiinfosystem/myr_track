package model;

/**
 * Created by Narendra on 21-Nov-14.
 */
public class vehiclelist {
    private String vehicleno, unitNo;
    private String vehiclegroup;
    private String location;
    private String lat;
    private String lon;
    private String speed;
    private String BatteryStatus;
    private String analog1, digital1, digital2,digital3, onewiredata1, onewiredata2, onewiredata3;
    private String SensorIDonAnalogOne, SensorIDonDigitalIO1, SensorIDonDigitalIO2, SensorIDonDigitalIO3;
    private String SensorIDonOneWireData1, SensorIDonOneWireData2, SensorIDonOneWireData3;
    private String lastreported;
    private String ignition;
    private String Status;
    private String Tracking_Date_Time;

    public vehiclelist(String vehicleno, String location, String lat, String lon, String speed, String lastreported, String analog1, String digital1, String digital2,String digital3, String onewiredata1, String onewiredata2, String onewiredata3, String SensorIDonAnalogOne, String SensorIDonDigitalIO1, String SensorIDonDigitalIO2, String SensorIDonDigitalIO3, String SensorIDonOneWireData1, String SensorIDonOneWireData2, String SensorIDonOneWireData3, String unitNo, String BatteryStatus,String ignition, String Status, String Tracking_Date_Time) {
        super();
        this.SensorIDonAnalogOne = SensorIDonAnalogOne;
        this.SensorIDonDigitalIO1 = SensorIDonDigitalIO1;
        this.SensorIDonDigitalIO2 = SensorIDonDigitalIO2;
        this.SensorIDonDigitalIO3 = SensorIDonDigitalIO3;
        this.SensorIDonOneWireData1 = SensorIDonOneWireData1;
        this.SensorIDonOneWireData2 = SensorIDonOneWireData2;
        this.SensorIDonOneWireData3 = SensorIDonOneWireData3;

        this.vehicleno = vehicleno;
        this.unitNo = unitNo;
        this.location = location;
        this.lat = lat;
        this.lon = lon;
        this.speed = speed;
        this.BatteryStatus = BatteryStatus;
        this.lastreported = lastreported;
        this.analog1 = analog1;
        this.digital1 = digital1;
        this.digital2 = digital2;
        this.digital3 = digital3;
        this.onewiredata1 = onewiredata1;
        this.onewiredata2 = onewiredata2;
        this.onewiredata3 = onewiredata3;
        this.ignition = ignition;
        this.Status = Status;
        this.Tracking_Date_Time = Tracking_Date_Time;
    }

    public vehiclelist( String lat, String lon, String speed, String lastreported,String location) {
        this.lat = lat;
        this.lon = lon;
        this.speed = speed;
        this.lastreported = lastreported;
        this.location = location;
    }

    public vehiclelist(String vehicleno, String vehiclegroup, String location, String speed, String batteryStatus, String lastreported) {
        this.vehicleno = vehicleno;
        this.vehiclegroup = vehiclegroup;
        this.location = location;
        this.speed = speed;
        this.BatteryStatus = batteryStatus;
        this.lastreported = lastreported;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getTracking_Date_Time() {
        return Tracking_Date_Time;
    }

    public void setTracking_Date_Time(String tracking_Date_Time) {
        Tracking_Date_Time = tracking_Date_Time;
    }

    public String getVehiclegroup() {
        return vehiclegroup;
    }

    public void setVehiclegroup(String vehiclegroup) {
        this.vehiclegroup = vehiclegroup;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getUnitNo() {
        return unitNo;
    }

    public void setUnitNo() {
        this.unitNo = unitNo;
    }

    public String getLastreported() {
        return lastreported;
    }

    public void setLastreported(String BatteryStatus) {
        this.BatteryStatus = BatteryStatus;
    }

    public String getBatteryStatus() {
        return BatteryStatus;
    }

    public void setBatteryStatus(String BatteryStatus) {
        this.BatteryStatus = BatteryStatus;
    }

    public String getSensorIDonAnalogOne() {
        return SensorIDonAnalogOne;
    }

    public void setSensorIDonAnalogOne(String sensorIDonAnalogOne) {
        SensorIDonAnalogOne = sensorIDonAnalogOne;
    }

    public String getSensorIDonDigitalIO1() {
        return SensorIDonDigitalIO1;
    }

    public void setSensorIDonDigitalIO1(String sensorIDonDigitalIO1) {
        SensorIDonDigitalIO1 = sensorIDonDigitalIO1;
    }

    public String getSensorIDonDigitalIO2() {
        return SensorIDonDigitalIO2;
    }

    public void setSensorIDonDigitalIO2(String sensorIDonDigitalIO2) {
        SensorIDonDigitalIO2 = sensorIDonDigitalIO2;
    }

    public String getSensorIDonDigitalIO3() {
        return SensorIDonDigitalIO3;
    }

    public void setSensorIDonDigitalIO3(String sensorIDonDigitalIO3) {
        SensorIDonDigitalIO3 = sensorIDonDigitalIO3;
    }

    public String getSensorIDonOneWireData1() {
        return SensorIDonOneWireData1;
    }

    public void setSensorIDonOneWireData1(String sensorIDonOneWireData1) {
        SensorIDonOneWireData1 = sensorIDonOneWireData1;
    }

    public String getSensorIDonOneWireData2() {
        return SensorIDonOneWireData2;
    }

    public void setSensorIDonOneWireData2(String sensorIDonOneWireData2) {
        SensorIDonOneWireData2 = sensorIDonOneWireData2;
    }

    public String getSensorIDonOneWireData3() {
        return SensorIDonOneWireData3;
    }

    public void setSensorIDonOneWireData3(String sensorIDonOneWireData3) {
        SensorIDonOneWireData3 = sensorIDonOneWireData3;
    }

    public String getAnalog1() {
        return analog1;
    }

    public void setAnalog1(String analog1) {
        this.analog1 = analog1;
    }

    public String getDigital1() {
        return digital1;
    }

    public void setDigital1(String digital1) {
        this.digital1 = digital1;
    }

    public String getOnewiredata1() {
        return onewiredata1;
    }

    public void setOnewiredata1(String onewiredata1) {
        this.onewiredata1 = onewiredata1;
    }

    public String getDigital2() {
        return digital2;
    }

    public void setDigital2(String digital2) {
        this.digital2 = digital2;
    }

    public String getOnewiredata2() {
        return onewiredata2;
    }

    public void setOnewiredata2(String onewiredata2) {
        this.onewiredata2 = onewiredata2;
    }

    public String getOnewiredata3() {
        return onewiredata3;
    }

    public void setOnewiredata3(String onewiredata3) {
        this.onewiredata3 = onewiredata3;
    }

    public String getVehicleno() {
        return vehicleno;
    }

    public void setVehicleno(String vehicleno) {
        this.vehicleno = vehicleno;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getDigital3() {
        return digital3;
    }

    public void setDigital3(String digital3) {
        this.digital3 = digital3;
    }

    public String getIgnition() {
        return ignition;
    }

    public void setIgnition(String ignition) {
        this.ignition = ignition;
    }
}

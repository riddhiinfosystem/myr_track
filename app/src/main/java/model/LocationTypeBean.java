package model;

public class LocationTypeBean {

    String locationType;
    int locationTypeId;

    public LocationTypeBean() {

    }

    public LocationTypeBean(String locationType, int locationTypeId) {
        this.locationType = locationType;
        this.locationTypeId = locationTypeId;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    public int getLocationTypeId() {
        return locationTypeId;
    }

    public void setLocationTypeId(int locationTypeId) {
        this.locationTypeId = locationTypeId;
    }

}

package model;

/**
 * Created by Vinayak on 5/29/2018.
 */

public class VehicleStatus {
    private String Controlling_Branch_ID;
    private String Vehicle_No;
    private String Status;
    private boolean selected = false;

    public VehicleStatus(String controlling_Branch_ID, String vehicle_No) {
        Controlling_Branch_ID = controlling_Branch_ID;
        Vehicle_No = vehicle_No;
    }

    public VehicleStatus(String controlling_Branch_ID, String vehicle_No, String status, boolean selected) {
        this.Controlling_Branch_ID = controlling_Branch_ID;
        this.Vehicle_No = vehicle_No;
        this.Status = status;
        this.selected = selected;
    }

    public String getControlling_Branch_ID() {
        return Controlling_Branch_ID;
    }

    public void setControlling_Branch_ID(String controlling_Branch_ID) {
        Controlling_Branch_ID = controlling_Branch_ID;
    }

    public String getVehicle_No() {
        return Vehicle_No;
    }

    public void setVehicle_No(String vehicle_No) {
        Vehicle_No = vehicle_No;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }
}

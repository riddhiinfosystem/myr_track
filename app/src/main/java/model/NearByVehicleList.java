package model;

/**
 * Created by Vinayak on 6/13/2018.
 */

public class NearByVehicleList {
    private int Vehicle_ID;
    private String vehicleno;
    private String date;
    private String latitude;
    private String longitude;
    private String Speed;
    private String location;
    private String Driver1_Name;
    private int Direction;
    private String kms;

    public NearByVehicleList(String vehicleno, String location, String date, String kms) {
        this.vehicleno = vehicleno;
        this.location = location;
        this.date = date;
        this.kms = kms;
    }

    public NearByVehicleList(int vehicle_ID, String vehicleno, String date, String latitude, String longitude, String speed, String location, String driver1_Name, int direction) {
        this.Vehicle_ID = vehicle_ID;
        this.vehicleno = vehicleno;
        this.date = date;
        this.latitude = latitude;
        this.longitude = longitude;
        this.Speed = speed;
        this.location = location;
        this.Driver1_Name = driver1_Name;
        this.Direction = direction;
    }

    public String getVehicleno() {
        return vehicleno;
    }

    public void setVehicleno(String vehicleno) {
        this.vehicleno = vehicleno;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getKms() {
        return kms;
    }

    public void setKms(String kms) {
        this.kms = kms;
    }

    public int getVehicle_ID() {
        return Vehicle_ID;
    }

    public void setVehicle_ID(int vehicle_ID) {
        Vehicle_ID = vehicle_ID;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getSpeed() {
        return Speed;
    }

    public void setSpeed(String speed) {
        Speed = speed;
    }

    public String getDriver1_Name() {
        return Driver1_Name;
    }

    public void setDriver1_Name(String driver1_Name) {
        Driver1_Name = driver1_Name;
    }

    public int getDirection() {
        return Direction;
    }

    public void setDirection(int direction) {
        Direction = direction;
    }
}

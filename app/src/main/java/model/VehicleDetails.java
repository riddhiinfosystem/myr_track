package model;

/**
 * Created by Vinayak on 4/28/2018.
 */

public class VehicleDetails {
    private String Vehicle_No;
    private String UnitNo;
    private String utcToLocalDateTime;
    private String Installation_Date;
    private String Power_Satatus;
    private String Remarks;
    private String LastLocation;
    private String Vehicle_Controlling_Branch;
    private String Vehicle_Group;
    private String Vehicle_Type;
    private String VehicleWorking;

    public VehicleDetails(String vehicle_No, String unitNo, String utcToLocalDateTime, String installation_Date, String power_Satatus, String remarks, String lastLocation, String vehicle_Controlling_Branch, String vehicle_Group, String vehicle_Type, String VehicleWorking) {
        this.Vehicle_No = vehicle_No;
        this.UnitNo = unitNo;
        this.utcToLocalDateTime = utcToLocalDateTime;
        this.Installation_Date = installation_Date;
        this.Power_Satatus = power_Satatus;
        this.Remarks = remarks;
        this.LastLocation = lastLocation;
        this.Vehicle_Controlling_Branch = vehicle_Controlling_Branch;
        this.Vehicle_Group = vehicle_Group;
        this.Vehicle_Type = vehicle_Type;
        this.VehicleWorking = VehicleWorking;
    }

    public String getVehicle_No() {
        return Vehicle_No;
    }

    public void setVehicle_No(String vehicle_No) {
        Vehicle_No = vehicle_No;
    }

    public String getUnitNo() {
        return UnitNo;
    }

    public void setUnitNo(String unitNo) {
        UnitNo = unitNo;
    }

    public String getUtcToLocalDateTime() {
        return utcToLocalDateTime;
    }

    public void setUtcToLocalDateTime(String utcToLocalDateTime) {
        this.utcToLocalDateTime = utcToLocalDateTime;
    }

    public String getInstallation_Date() {
        return Installation_Date;
    }

    public void setInstallation_Date(String installation_Date) {
        Installation_Date = installation_Date;
    }

    public String getPower_Satatus() {
        return Power_Satatus;
    }

    public void setPower_Satatus(String power_Satatus) {
        Power_Satatus = power_Satatus;
    }

    public String getRemarks() {
        return Remarks;
    }

    public void setRemarks(String remarks) {
        Remarks = remarks;
    }

    public String getLastLocation() {
        return LastLocation;
    }

    public void setLastLocation(String lastLocation) {
        LastLocation = lastLocation;
    }

    public String getVehicle_Controlling_Branch() {
        return Vehicle_Controlling_Branch;
    }

    public void setVehicle_Controlling_Branch(String vehicle_Controlling_Branch) {
        Vehicle_Controlling_Branch = vehicle_Controlling_Branch;
    }

    public String getVehicle_Group() {
        return Vehicle_Group;
    }

    public void setVehicle_Group(String vehicle_Group) {
        Vehicle_Group = vehicle_Group;
    }

    public String getVehicle_Type() {
        return Vehicle_Type;
    }

    public void setVehicle_Type(String vehicle_Type) {
        Vehicle_Type = vehicle_Type;
    }

    public String getVehicleWorking() {
        return VehicleWorking;
    }

    public void setVehicleWorking(String VehicleWorking) {
        VehicleWorking = VehicleWorking;
    }
}

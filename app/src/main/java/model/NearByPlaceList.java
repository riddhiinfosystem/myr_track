package model;

/**
 * Created by Vinayak on 6/18/2018.
 */

public class NearByPlaceList {
    private String PlaceName;
    private String LatLong;


    public NearByPlaceList(String placeName, String latLong) {
        this.PlaceName = placeName;
        this.LatLong = latLong;
    }

    public String getPlaceName() {
        return PlaceName;
    }

    public void setPlaceName(String placeName) {
        PlaceName = placeName;
    }

    public String getLatLong() {
        return LatLong;
    }

    public void setLatLong(String latLong) {
        LatLong = latLong;
    }
}

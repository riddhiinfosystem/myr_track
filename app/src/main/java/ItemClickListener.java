import android.view.View;

/**
 * Created by Vinayak on 8/18/2018.
 */

public interface ItemClickListener {
    void onClick(View view, int position, boolean isLongClick);
}

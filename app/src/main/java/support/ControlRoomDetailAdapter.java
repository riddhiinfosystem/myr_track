package support;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.riddhi.myr_track.R;

import java.util.ArrayList;
import java.util.List;

import model.NearByVehicleList;
import model.vehiclelist;

public class ControlRoomDetailAdapter extends BaseAdapter {
    ArrayList<vehiclelist> vehicleListDown;
    Context context;
    LayoutInflater inflater;

    public ControlRoomDetailAdapter(ArrayList<vehiclelist> vList, Context context) {
        this.context = context;
        this.vehicleListDown = vList;
        inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return vehicleListDown.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ControlRoomDetailAdapter.MyViewHolder holder = null;
        Log.v("ConvertView", String.valueOf(position));

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.control_room_detail_row, parent, false);
            holder = new ControlRoomDetailAdapter.MyViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ControlRoomDetailAdapter.MyViewHolder) convertView.getTag();
        }

        vehiclelist state = vehicleListDown.get(position);

        if(!state.getVehicleno().equalsIgnoreCase(""))
        {
            holder.tvvehiclenonew.setVisibility(View.VISIBLE);
            holder.vehicle_icon.setVisibility(View.VISIBLE);
            holder.tvvehiclenonew.setText("" + state.getVehicleno());
        }
        else
        {
            holder.vehicle_icon.setVisibility(View.GONE);
            holder.tvvehiclenonew.setVisibility(View.GONE);
        }
        //=====================================================
        if(!state.getLocation().equalsIgnoreCase(""))
        {
            holder.tvlocationnew.setVisibility(View.VISIBLE);
            holder.img_location_icon.setVisibility(View.VISIBLE);
            holder.tvlocationnew.setText("" + state.getLocation());
        }
        else
        {
            holder.img_location_icon.setVisibility(View.GONE);
            holder.tvlocationnew.setVisibility(View.GONE);
        }
        //=========================================================
        if(!state.getLastreported().equalsIgnoreCase(""))
        {
            holder.tvvehiclereporteddatetime.setVisibility(View.VISIBLE);
            holder.img_time_icon.setVisibility(View.VISIBLE);
            holder.tvvehiclereporteddatetime.setText("" + state.getLastreported());
        }
        else
        {
            holder.img_time_icon.setVisibility(View.GONE);
            holder.tvvehiclereporteddatetime.setVisibility(View.GONE);
        }
        //=============================================================
        if(!state.getVehiclegroup().equalsIgnoreCase(""))
        {
            holder.tvVehicleGroup.setVisibility(View.VISIBLE);
            holder.img_vehicle_group.setVisibility(View.VISIBLE);
            holder.tvVehicleGroup.setText("" + state.getVehiclegroup());
        }
        else
        {
            holder.tvVehicleGroup.setVisibility(View.GONE);
            holder.img_vehicle_group.setVisibility(View.GONE);
        }
        //=============================================================
        if(!state.getSpeed().equalsIgnoreCase(""))
        {
            holder.tvvehiclespeed.setVisibility(View.VISIBLE);
            holder.img_speed.setVisibility(View.VISIBLE);
            holder.tvvehiclespeed.setText("" + state.getSpeed());
            holder.img_speed.setImageResource(R.drawable.kms_icon);
        }
        else
        {
            if(!state.getBatteryStatus().equalsIgnoreCase(""))
            {
                holder.tvvehiclespeed.setVisibility(View.VISIBLE);
                holder.img_speed.setVisibility(View.VISIBLE);
                holder.tvvehiclespeed.setText("" + state.getBatteryStatus());
                holder.img_speed.setImageResource(R.drawable.external_battery);
            }
            else
            {
                holder.tvvehiclespeed.setVisibility(View.GONE);
                holder.img_speed.setVisibility(View.GONE);
            }

        }

        return convertView;
    }

    private class MyViewHolder {
        TextView tvvehiclenonew;
        TextView tvlocationnew;
        TextView tvvehiclereporteddatetime;
        TextView tvVehicleGroup;
        TextView tvvehiclespeed;
        ImageView img_speed;
        ImageView vehicle_icon;
        ImageView img_time_icon;
        ImageView img_vehicle_group;
        ImageView img_location_icon;

        public MyViewHolder(View item) {
            tvvehiclenonew = item.findViewById(R.id.tvvehiclenonew);
            img_speed = item.findViewById(R.id.img_speed);
            tvlocationnew = item.findViewById(R.id.tvlocationnew);
            tvvehiclereporteddatetime = item.findViewById(R.id.tvvehiclereporteddatetime);
            tvVehicleGroup = item.findViewById(R.id.tvVehicleGroup);
            tvvehiclespeed = item.findViewById(R.id.tvvehiclespeed);
            vehicle_icon = item.findViewById(R.id.vehicle_icon);
            img_time_icon = item.findViewById(R.id.img_time_icon);
            img_vehicle_group = item.findViewById(R.id.img_vehicle_group);
            img_location_icon = item.findViewById(R.id.img_location_icon);
        }
    }

}
package support;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.riddhi.myr_track.R;

import java.util.ArrayList;
import java.util.List;

import model.VehicleStatus;

/**
 * Created by Vinayak on 5/31/2018.
 */

public class MyCustomAdapter extends BaseAdapter {
    List<VehicleStatus> stateList = new ArrayList<VehicleStatus>();
    LayoutInflater inflater;
    Context context;

    public MyCustomAdapter(Context context, List<VehicleStatus> stateList) {
        this.stateList = stateList;
        this.context = context;
        inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return stateList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyViewHolder holder = null;
        Log.v("ConvertView", String.valueOf(position));

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.status_info, parent, false);
            holder = new MyCustomAdapter.MyViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (MyCustomAdapter.MyViewHolder) convertView.getTag();
        }

        VehicleStatus state = stateList.get(position);
        holder.code.setText(state.getVehicle_No());
        holder.name.setChecked(state.isSelected());
        holder.name.setTag(state);
        holder.txt_status.setText("" + state.getStatus());
        return convertView;
    }

    private class MyViewHolder {
        TextView code;
        TextView txt_status;
        CheckBox name;

        public MyViewHolder(View item) {
            code = item.findViewById(R.id.code);
            txt_status = item.findViewById(R.id.txt_status);
            name = item.findViewById(R.id.checkBox1);

            name.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    CheckBox cb = (CheckBox) v;
                    VehicleStatus _state = (VehicleStatus) cb.getTag();
                    _state.setSelected(cb.isChecked());
                }
            });
        }
    }

}
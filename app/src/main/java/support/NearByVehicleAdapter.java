package support;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.riddhi.myr_track.R;

import java.util.ArrayList;
import java.util.List;

import model.NearByVehicleList;

/**
 * Created by Vinayak on 6/13/2018.
 */

public class NearByVehicleAdapter extends BaseAdapter {
    List<NearByVehicleList> stateList = new ArrayList<NearByVehicleList>();
    LayoutInflater inflater;
    Context context;

    public NearByVehicleAdapter(Context context, List<NearByVehicleList> stateList) {
        this.stateList = stateList;
        this.context = context;
        inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return stateList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        NearByVehicleAdapter.MyViewHolder holder = null;
        Log.v("ConvertView", String.valueOf(position));

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.activity_near_by_vehicle_row, parent, false);
            holder = new NearByVehicleAdapter.MyViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (NearByVehicleAdapter.MyViewHolder) convertView.getTag();
        }

        NearByVehicleList state = stateList.get(position);


        holder.txt_vehicleNo.setText("" + state.getVehicleno());
        holder.txt_location.setText("" + state.getLocation());
        holder.txt_datetime.setText("" + state.getDate());
        holder.txt_kms.setText("" + state.getKms());

        return convertView;
    }

    private class MyViewHolder {
        TextView txt_vehicleNo;
        TextView txt_location;
        TextView txt_datetime;
        TextView txt_kms;

        public MyViewHolder(View item) {
            txt_vehicleNo = item.findViewById(R.id.txt_vehicleNo);
            txt_location = item.findViewById(R.id.txt_location);
            txt_datetime = item.findViewById(R.id.txt_datetime);
            txt_kms = item.findViewById(R.id.txt_kms);
        }
    }

}
package support;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;

import com.riddhi.myr_track.R;

import java.util.ArrayList;

import Fragment.VehicleFragment;
import model.vehiclelist;


/**
 * Created by Vinayak on 12/7/2016.
 */

public class CustomVehicleAdapter extends BaseAdapter {

    ArrayList<vehiclelist> vehicleListDown;
    Context context;
    LayoutInflater inflater;

    public CustomVehicleAdapter(ArrayList<vehiclelist> vList, Context context) {
        this.context = context;
        this.vehicleListDown = vList;
        inflater = LayoutInflater.from(this.context);
    }
    @Override
    public int getCount() {
        return vehicleListDown.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CustomVehicleAdapter.VehicleViewHolder holder = null;
        Log.v("ConvertView", String.valueOf(position));

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.card_view_layput, parent, false);
            holder = new CustomVehicleAdapter.VehicleViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (CustomVehicleAdapter.VehicleViewHolder) convertView.getTag();
        }


        TextView vehicleNoNew = holder.vehicleNoNew;
        TextView vehicleSpeed = holder.vehicleSpeed;
        TextView lastUpdateDateTime = holder.lastUpdateDateTime;
        TextView reportedUnitNo = holder.reportedUnitNo;
        TextView locationNew = holder.locationNew;
        TextView latitudeNew = holder.latitudeNew;
        TextView longitudeNew = holder.longitudeNew;
        TableLayout tableLayout = holder.tableLayout;
        ImageView imageTemp1 = holder.imageTemp1;
        ImageView imageTemp2 = holder.imageTemp2;
        ImageView imageTemp3 = holder.imageTemp3;
        ImageView img_speed = holder.img_speed;
        TextView sensor1Value = holder.sensor1Value;
        TextView sensor2Value = holder.sensor2Value;
        TextView sensor3Value = holder.sensor3Value;
        ImageView imageDoor1 = holder.imageDoor1;
        ImageView imageDoor2 = holder.imageDoor2;
        ImageView fuelNew = holder.fuelNew;
        TextView door1Value = holder.door1Value;
        TextView door2Value = holder.door2Value;
        TextView fuelValue = holder.fuelValue;
        ImageView ignition = holder.ignition;

        TextView tvStatus = holder.tvStatus;
        ImageView img_status = holder.img_status;
        TextView tvTracking_Date_Time = holder.tvTracking_Date_Time;
        ImageView img_tracking_date = holder.img_tracking_date;



        if (vehicleListDown.get(position).getStatus().equalsIgnoreCase("")) {
            tvStatus.setVisibility(View.GONE);
            img_status.setVisibility(View.GONE);
            tvTracking_Date_Time.setVisibility(View.GONE);
            img_tracking_date.setVisibility(View.GONE);
        } else {
            tvStatus.setText(""+vehicleListDown.get(position).getStatus());
            tvTracking_Date_Time.setText(""+vehicleListDown.get(position).getTracking_Date_Time());
            tvStatus.setVisibility(View.VISIBLE);
            img_status.setVisibility(View.VISIBLE);
            tvTracking_Date_Time.setVisibility(View.VISIBLE);
            img_tracking_date.setVisibility(View.VISIBLE);
        }

        //VerticalProgressBar batteryStatus = holder.batteryStatus;
        //TextView batteryStatusPercentage = holder.batteryStatusPercentage;

        imageDoor1.setVisibility(View.GONE);
        imageDoor2.setVisibility(View.GONE);
        door1Value.setVisibility(View.GONE);
        door2Value.setVisibility(View.GONE);

        String analog1value;
        String onewiresensor1 = "-";
        String onewiresensor2 = "-";
        String onewiresensor3 = "-";
        String door1, door2,ignitionval;

        String SensorIDonAnalogOne, SensorIDonDigitalIO1, SensorIDonDigitalIO2, SensorIDonDigitalIO3, unitNo;
        String SensorIDonOneWireData1, SensorIDonOneWireData2, SensorIDonOneWireData3;

        SensorIDonAnalogOne = vehicleListDown.get(position).getSensorIDonAnalogOne();
        SensorIDonDigitalIO1 = vehicleListDown.get(position).getSensorIDonDigitalIO1();
        SensorIDonDigitalIO2 = vehicleListDown.get(position).getSensorIDonDigitalIO2();
        SensorIDonDigitalIO3 = vehicleListDown.get(position).getSensorIDonDigitalIO3();
        SensorIDonOneWireData1 = vehicleListDown.get(position).getSensorIDonOneWireData1();
        SensorIDonOneWireData2 = vehicleListDown.get(position).getSensorIDonOneWireData2();
        SensorIDonOneWireData3 = vehicleListDown.get(position).getSensorIDonOneWireData3();

        reportedUnitNo.setText(vehicleListDown.get(position).getUnitNo());

        if (SensorIDonAnalogOne.equals("0") && SensorIDonDigitalIO1.equals("0")
                && SensorIDonDigitalIO2.equals("0") && SensorIDonDigitalIO3.equals("0") &&
                SensorIDonOneWireData1.equals("0") && SensorIDonOneWireData2.equals("0") &&
                SensorIDonOneWireData3.equals("0")) {
            tableLayout.setVisibility(View.GONE);

        } else {

            if (SensorIDonDigitalIO1.equals("6") || SensorIDonDigitalIO1.equals("10")) {
                if (!SensorIDonDigitalIO2.equals("2") || !SensorIDonOneWireData1.equals("1")) {
                    tableLayout.setVisibility(View.GONE);
                } else {
                    tableLayout.setVisibility(View.VISIBLE);
                }
            } else {
                tableLayout.setVisibility(View.VISIBLE);
            }
        }

        if (SensorIDonAnalogOne.equals("9") || SensorIDonAnalogOne.equals("10")) {
            analog1value = vehicleListDown.get(position).getAnalog1();
            if (analog1value.equals("-")) {
                fuelNew.setVisibility(View.GONE);
                fuelValue.setVisibility(View.GONE);
            } else {
                tableLayout.setVisibility(View.VISIBLE);
                fuelNew.setVisibility(View.VISIBLE);
                fuelValue.setVisibility(View.VISIBLE);
                fuelValue.setText(analog1value);
            }
        } else {
            fuelNew.setVisibility(View.INVISIBLE);
            fuelValue.setVisibility(View.INVISIBLE);
        }

        if (SensorIDonOneWireData1.equals("1")) { // applicable only if dallas temperature sensor connected

            //work for temp sensor1
            onewiresensor1 = vehicleListDown.get(position).getOnewiredata1();
            if (onewiresensor1.equals("-")) {
                sensor1Value.setVisibility(View.GONE);
                imageTemp1.setVisibility(View.GONE);
            } else {
                imageTemp1.setVisibility(View.VISIBLE);
                sensor1Value.setVisibility(View.VISIBLE);
                sensor1Value.setText(onewiresensor1);
            }
        } else {
            sensor1Value.setVisibility(View.GONE);
            imageTemp1.setVisibility(View.GONE);
        }

        //work for temp sensor2
        if (SensorIDonOneWireData2.equals("1")) { // applicable only if dallas temperature sensor connected

            onewiresensor2 = vehicleListDown.get(position).getOnewiredata2();
            if (onewiresensor2.equals("-")) {
                sensor2Value.setVisibility(View.GONE);
                imageTemp2.setVisibility(View.GONE);
            } else {
                imageTemp2.setVisibility(View.VISIBLE);
                sensor2Value.setVisibility(View.VISIBLE);
                sensor2Value.setText(onewiresensor2);
            }
        } else {
            sensor2Value.setVisibility(View.GONE);
            imageTemp2.setVisibility(View.GONE);
        }

        //work for temp sensor3
        if (SensorIDonOneWireData3.equals("1")) { // applicable only if dallas temperature sensor connected

            onewiresensor3 = vehicleListDown.get(position).getOnewiredata3();
            if (onewiresensor3.equals("-")) {
                sensor3Value.setVisibility(View.GONE);
                imageTemp3.setVisibility(View.GONE);
            } else {
                imageTemp3.setVisibility(View.VISIBLE);
                sensor3Value.setVisibility(View.VISIBLE);
                sensor3Value.setText(onewiresensor3);
            }
        } else {
            sensor3Value.setVisibility(View.GONE);
            imageTemp3.setVisibility(View.GONE);
        }

        // Work For door1 sensor
        if (SensorIDonDigitalIO1.equals("2")) { // applicable only for door sensor
            door1 = vehicleListDown.get(position).getDigital1();
            if (door1.equals("-")) {
                imageDoor1.setVisibility(View.GONE);
                door1Value.setVisibility(View.GONE);
            } else {
                imageDoor1.setVisibility(View.VISIBLE);
                door1Value.setVisibility(View.VISIBLE);
                if (door1.equals("Open")) {
                    imageDoor1.setImageResource(R.drawable.dooropennew);
                    door1Value.setText("Open");

                } else {
                    imageDoor1.setImageResource(R.drawable.doorclosenew);
                    door1Value.setText("Close");
                }
            }
        } else {
            imageDoor1.setVisibility(View.GONE);
            door1Value.setVisibility(View.GONE);
        }

        // Work For Door1 Sensor for 5300 devices

        if (SensorIDonDigitalIO1.equals("10")) { // applicable only for door sensor
            if (SensorIDonDigitalIO2.equals("2")) {
                door1 = vehicleListDown.get(position).getDigital2();
                if (door1.equals("-")) {
                    imageDoor1.setVisibility(View.GONE);
                    door1Value.setVisibility(View.GONE);
                } else {
                    imageDoor1.setVisibility(View.VISIBLE);
                    door1Value.setVisibility(View.VISIBLE);
                    if (door1.equals("Open"))
                    {
                        imageDoor1.setImageResource(R.drawable.dooropennew);
                        door1Value.setText("Open");
                    } else {
                        imageDoor1.setImageResource(R.drawable.doorclosenew);
                        door1Value.setText("Close");
                    }

                }
            }
        }

        // Work For door2 sensor
        if (SensorIDonDigitalIO2.equals("2")) { // applicable only for door sensor
            door2 = vehicleListDown.get(position).getDigital2();
            if (door2.equals("-")) {
                imageDoor2.setVisibility(View.GONE);
                door2Value.setVisibility(View.GONE);
            } else {
                imageDoor2.setVisibility(View.VISIBLE);
                door2Value.setVisibility(View.VISIBLE);
                if (door2.equals("Open")) {
                    imageDoor2.setImageResource(R.drawable.dooropennew);
                    door2Value.setText("Open");
                } else {
                    imageDoor2.setImageResource(R.drawable.doorclosenew);
                    door2Value.setText("Close");
                }
            }
        } else {
            imageDoor2.setVisibility(View.GONE);
            door2Value.setVisibility(View.GONE);
        }

        // work for door2 sensor in case of 5300

        if (SensorIDonDigitalIO1.equals("10")) { // applicable only for door sensor
            if (SensorIDonDigitalIO3.equals("2")) {
                door2 = vehicleListDown.get(position).getDigital2();
                if (door2.equals("-")) {
                    imageDoor2.setVisibility(View.GONE);
                    door2Value.setVisibility(View.GONE);
                } else {
                    imageDoor2.setVisibility(View.VISIBLE);
                    door2Value.setVisibility(View.VISIBLE);
                    if (door2.equals("Open")) {
                        imageDoor2.setImageResource(R.drawable.dooropennew);
                        door2Value.setText("Open");
                    } else {
                        imageDoor2.setImageResource(R.drawable.doorclosenew);
                        door2Value.setText("Close");
                    }
                }
            }
        }

        Integer speedint = 0;
        String dateToDisplay = "";

        try {
            speedint = Integer.parseInt(vehicleListDown.get(position).getSpeed());
            dateToDisplay = vehicleListDown.get(position).getLastreported().toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        vehicleNoNew.setText(" " + vehicleListDown.get(position).getVehicleno());
        locationNew.setText(vehicleListDown.get(position).getLocation());
        latitudeNew.setText(vehicleListDown.get(position).getLat());
        longitudeNew.setText(vehicleListDown.get(position).getLon());
        vehicleSpeed.setText(vehicleListDown.get(position).getSpeed());
        lastUpdateDateTime.setText(dateToDisplay);

        ignitionval = ""+vehicleListDown.get(position).getIgnition();
        if (ignitionval.equals("0")) {
            ignition.setImageResource(R.drawable.reddot_new);
        } else {
            ignition.setImageResource(R.drawable.greendot_new);
        }



        /*String tempbatteryStatus = "";
        tempbatteryStatus = "" + vehicleListDown.get(position).getBatteryStatus();
        float tempFloat = Float.valueOf(tempbatteryStatus);
        int tempInt = (int) tempFloat;
        batteryStatusPercentage.setText(tempInt + "%");
        final Percent randomPercent = new Percent(tempInt);
        batteryStatus.setCurrentValue(randomPercent);*/

        int myColormoving = context.getResources().getColor(R.color.green);
        int colornormalvehicle = context.getResources().getColor(R.color.black_color);

        if (speedint > 0) {
            vehicleNoNew.setTextColor(myColormoving);
            vehicleSpeed.setVisibility(View.VISIBLE);
            img_speed.setVisibility(View.VISIBLE);
        } else {
            vehicleNoNew.setTextColor(colornormalvehicle);
            vehicleSpeed.setVisibility(View.INVISIBLE);
            img_speed.setVisibility(View.INVISIBLE);
        }

        return convertView;
    }


    public static class VehicleViewHolder extends RecyclerView.ViewHolder {
        TextView vehicleNoNew;
        TextView vehicleSpeed;
        TextView lastUpdateDateTime;
        TextView reportedUnitNo;
        TextView locationNew;
        TextView latitudeNew;
        TextView longitudeNew;
        TableLayout tableLayout;
        ImageView imageTemp1;
        ImageView imageTemp2;
        ImageView imageTemp3;
        ImageView img_speed;
        TextView sensor1Value;
        TextView sensor2Value;
        TextView sensor3Value;
        ImageView imageDoor1;
        ImageView imageDoor2;
        ImageView fuelNew;
        TextView door1Value;
        TextView door2Value;
        TextView fuelValue;
        ImageView ignition;

        TextView tvStatus;
        ImageView img_status;
        TextView tvTracking_Date_Time;
        ImageView img_tracking_date;
        //VerticalProgressBar batteryStatus;
        //TextView batteryStatusPercentage;

        public VehicleViewHolder(View itemView) {
            super(itemView);

            vehicleNoNew = itemView.findViewById(R.id.tvvehiclenonew);
            vehicleSpeed = itemView.findViewById(R.id.tvvehiclespeed);
            lastUpdateDateTime = itemView.findViewById(R.id.tvvehiclereporteddatetime);
            reportedUnitNo = itemView.findViewById(R.id.tvvehiclereportedUnitNo);
            locationNew = itemView.findViewById(R.id.tvlocationnew);
            latitudeNew = itemView.findViewById(R.id.tvlatnew);
            longitudeNew = itemView.findViewById(R.id.tvlonnew);
            tableLayout = itemView.findViewById(R.id.tbllayout);
            imageTemp1 = itemView.findViewById(R.id.imgtemp);
            imageTemp2 = itemView.findViewById(R.id.imgtemp2);
            imageTemp3 = itemView.findViewById(R.id.imgtemp3);
            img_speed = itemView.findViewById(R.id.img_speed);
            sensor1Value = itemView.findViewById(R.id.tvsensor1value);
            sensor2Value = itemView.findViewById(R.id.tvsensor2value);
            sensor3Value = itemView.findViewById(R.id.tvsensor3value);
            imageDoor1 = itemView.findViewById(R.id.imgdoor1);
            imageDoor2 = itemView.findViewById(R.id.imgdoor2);
            fuelNew = itemView.findViewById(R.id.imgfuel);
            door1Value = itemView.findViewById(R.id.tvdoor1);
            door2Value = itemView.findViewById(R.id.tvdoor2);
            fuelValue = itemView.findViewById(R.id.tvfuelvalue);
            ignition = itemView.findViewById(R.id.ignition);
            tvStatus = itemView.findViewById(R.id.tvStatus);
            img_status = itemView.findViewById(R.id.img_status);
            tvTracking_Date_Time = itemView.findViewById(R.id.tvTracking_Date_Time);
            img_tracking_date = itemView.findViewById(R.id.img_tracking_date);
           // batteryStatus = itemView.findViewById(R.id.acd_id_proress_bar);
           // batteryStatusPercentage = itemView.findViewById(R.id.tvbatterystatus);
        }
    }


}


package support;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import model.LocationTypeBean;

public class DatabaseAdapter {

    static final String TABLE_NAME_1 = "location_types";
    static final String TABLE_CREATE_1 = "create table if not exists "
            + TABLE_NAME_1
            + "( id integer primary key autoincrement, "
            + " location_id integer,"
            + " location_type text); ";
    DBHelper dbHelper;
    SQLiteDatabase db;
    Context context;
    String DATABASE_NAME = "rTrack.db";
    int DATABASE_VERSION = 2;

    public DatabaseAdapter(Context context) {
        dbHelper = new DBHelper(context, DATABASE_NAME, null, DATABASE_VERSION);
        System.out.println("Database created");
    }

    public long insertLocationTypes(LocationTypeBean bean) {
        long i;
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        ContentValues initialValues = new ContentValues();

        initialValues.put("location_id", bean.getLocationTypeId());
        initialValues.put("location_type", bean.getLocationType());

        i = db.insert(TABLE_NAME_1, null, initialValues);
        return i;
    }

    public ArrayList<String> getLocationTypeList() {
        open();

        String location;
        ArrayList<String> locationTypeList = new ArrayList<String>();
        String query = "select location_type from " + TABLE_NAME_1;
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                location = cursor.getString(cursor.getColumnIndex("location_type"));
                locationTypeList.add(location);
            } while (cursor.moveToNext());
        }
        return locationTypeList;
    }

    public DatabaseAdapter open() throws SQLException {
        try {
            db = dbHelper.getWritableDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    public void close() {
        dbHelper.close();
    }

    public class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
            Log.i("DBHelper called", "");
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(TABLE_CREATE_1);
            Log.i("Location Type table ", "created successfully");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_1);
            Log.i("Location Type table ", "recreated");
            onCreate(db);
        }
    }

}

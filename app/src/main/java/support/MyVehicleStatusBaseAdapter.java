package support;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.riddhi.myr_track.R;

import java.util.ArrayList;
import java.util.List;

import model.VehicleStatus;

/**
 * Created by Vinayak on 5/30/2018.
 */

public class MyVehicleStatusBaseAdapter extends BaseAdapter {

    List<VehicleStatus> _myvehiclesList = new ArrayList<VehicleStatus>();
    LayoutInflater inflater;
    Context context;

    public MyVehicleStatusBaseAdapter(Context context, List<VehicleStatus> _myvehiclesList) {
        this._myvehiclesList = _myvehiclesList;
        this.context = context;
        inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return _myvehiclesList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }


    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyViewHolder mViewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.spinner_item, parent, false);
            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }

        VehicleStatus vehicleStatus = _myvehiclesList.get(position);

        mViewHolder.tvTitle.setText(vehicleStatus.getVehicle_No());

        return convertView;
    }

    private class MyViewHolder {
        TextView tvTitle;


        public MyViewHolder(View item) {
            tvTitle = item.findViewById(R.id.spinner1);

        }
    }
}
package support;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.riddhi.myr_track.R;

import java.math.BigDecimal;

@SuppressLint("AppCompatCustomView")
public class VerticalProgressBar extends ImageView {

    /**
     * @see <a href="http://developer.android.com/reference/android/graphics/drawable/ClipDrawable.html">ClipDrawable</a>
     */
    private static final BigDecimal MAX = BigDecimal.valueOf(10000);

    public VerticalProgressBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setImageResource(R.drawable.progress_bar);
    }

    public void setCurrentValue(Percent percent) {
        int cliDrawableImageLevel = percent.asBigDecimal().multiply(MAX).intValue();
        int temp = cliDrawableImageLevel - 1200;
        if (temp < 2000 && cliDrawableImageLevel != 0) {
            setImageLevel(cliDrawableImageLevel);
        } else {
            setImageLevel(cliDrawableImageLevel - 1200);
        }
    }
}
